<!DOCTYPE html>
<html>
	@include('Layout.header', ['type' => 'admin', 'title' => 'admin', 'icon' => asset('img/logophone.png') ])

<body class="font-base" onload="show_category();">
	@include('Layout.nav', ['type' => 'admin'])
	<div class="container-fluid mobile-margin">
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header">
					Subject
					<button class="btn btn-primary btn-sm float-right" onclick="add_category();"><i class="fa fa-plus"></i> Add Subject</button>
					</div>
					<div class="card-body">
						<table class="table table-bordered dt-responsive nowrap" id="tbl_category" style="width: 100%;"></table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
	 <div class="modal fade" role="dialog" id="add_category_modal">
	      <div class="modal-dialog">
	        <div class="modal-content">
	          <div class="modal-header">
	            <div class="modal-title">
	            Add Subject
	            </div>
	            <button class="close" data-dismiss="modal">&times;</button>
	          </div>
	          <div class="modal-body">
	         	<form class="needs-validation" id="add_category_form" action="{{ url('/admin/category/create') }}" novalidate>
					<div class="form-row">
						<div class="form-group col-sm-12">
							<label>Subject Name </label>
							<input type="text" id="category_name" name="category_name" placeholder="Category Name" class="form-control " required>
							<div class="invalid-feedback" id="err_category_name"></div>
						</div>

						<div class="col-sm-12 text-right">
							<button class="btn btn-dark btn-sm" type="submit">Save</button>
						</div>
					</div>
				</form>

	          </div>
	          <div class="modal-footer">

	          </div>
	        </div>
	      </div>
	    </div>

	@include('Layout.footer', ['type' => 'admin'])
</html>
<script type="text/javascript">
	$("#table_category").dataTable();

	function add_category(){
		$("#add_category_modal").modal('show');
	}
</script>

<script type="text/javascript">
		var tbl_category;
		function show_category(){
			if (tbl_category) {
				tbl_category.destroy();
			}
			var url = main_path + '/category/list';
			tbl_category = $('#tbl_category').DataTable({
			pageLength: 10,
			responsive: true,
			ajax: url,
			deferRender: true,
			language: {
			"emptyTable": "No data available"
		},
			columns: [{
			className: '',
			"data": "category_name",
			"title": "Subject Name",
		},{
			className: '',
			"data": "created_at",
			"title": "Date Created",
		},{
			className: 'width-option-1 text-center',
			"data": "category_id",
			"orderable": false,
			"title": "Options",
				"render": function(data, type, row, meta){
					var param_data = JSON.stringify(row);
					newdata = '';
					newdata += '<a class="btn btn-success btn-sm mt-1" href="'+url_path('/admin/category/edit/'+row.category_id)+'"><i class="fa fa-edit"></i> Edit</a> ';
					newdata += '<button class="btn btn-danger btn-sm font-base mt-1" data-id='+row.category_id+' onclick="delete_category(this)" type="button"><i class="fa fa-trash"></i> Delete</button>';
					return newdata;
				}
			}
		]
		});
		}
</script>

<!-- Javascript Function-->
<script>
	$("#add_category_form").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
				if(response.status == true){
					// console.log(response)
						show_category();
						swal({
							title: "Success",
							text: response.message,
							type: "success",
							showCancelButton: false,
							confirmButtonColor: "#007bff",
							confirmButtonText: "Okay",
						},
						function(isConfirm){
							if (isConfirm) {
								window.location = main_path + '/admin/category/edit/' + response.id;
							}
						});
					showValidator(response.error,'add_category_form');
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'add_category_form');
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	});

	function delete_category(_this){
	  var data = $(_this).attr('data-id');
	  var url = main_path + '/admin/category/delete/' + data;
	  swal({
	        title: "Are you sure?",
	        text: "Do you want to delete this subject ?",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "Yes",
	        closeOnConfirm: false
	      },
	        function(){
	         $.ajax({
	          type:"GET",
	          url:url,
	          data:{},
	          dataType:'json',
	          beforeSend:function(){
	          },
	          success:function(response){
	            // console.log(response);
	           if (response.status == true) {
	              swal("Success", response.message, "success");
	              // show_form_table();
	             show_category();
	           }else{
	            console.log(response);
	           }
	          },
	          error: function(error){
	            console.log(error);
	          }
	        });
	      });
	  }
</script>
