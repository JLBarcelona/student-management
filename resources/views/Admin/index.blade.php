<!DOCTYPE html>
<html>
	@include('Layout.header', ['type' => 'admin', 'title' => 'Admin', 'icon' => asset('img/logophone.png') ])
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/modules/data.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
	<script src="https://code.highcharts.com/modules/export-data.js"></script>
	<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<body class="font-base" onload="show_student()">
	@include('Layout.nav', ['type' => 'admin'])
	<div class="container-fluid mobile-margin">
	@if(Auth::guard('admin')->user()->user_type === 3)
			<div class="row">
				<div class="col-sm-6 mb-2">
					<div class="card border-info">
						<div class="card-body bg-info text-white">
							  <h5 class="card-title"><i class="fa fa-users"></i> Student  <span class="card-text float-right">{{ $student }}</span></h5>
						</div>
						<a href="{{ url('admin/student') }}" >
							<div class="card-footer">
								View More
							</div>
						</a>
					</div>
				</div>

				<div class="col-sm-6 mb-2">
					<div class="card border-success">
						<div class="card-body bg-success text-white">
							  <h5 class="card-title"><i class="fa fa-shopping-cart"></i> Product  <span class="card-text float-right">{{ $product }}</span></h5>
						</div>
						<a href="{{ url('admin/inventory') }}" >
							<div class="card-footer">
								View More
							</div>
						</a>
					</div>
				</div>

			</div>
	@elseif(Auth::guard('admin')->user()->user_type === 1)
		<div class="row">
			<div class="col-sm-4 mb-2">
				<div class="card border-primary">
					<div class="card-body bg-primary text-white">
						  <h5 class="card-title"><i class="fa fa-chalkboard-teacher"></i> Teacher  <span class="card-text float-right">{{ $teacher }}</span></h5>
					</div>
					<a href="{{ url('admin/teacher') }}" >
						<div class="card-footer">
							View More
						</div>
					</a>
				</div>
			</div>

			<div class="col-sm-4 mb-2">
				<div class="card border-info">
					<div class="card-body bg-info text-white">
						  <h5 class="card-title"><i class="fa fa-users"></i> Student  <span class="card-text float-right">{{ $student }}</span></h5>
					</div>
					<a href="{{ url('admin/student') }}" >
						<div class="card-footer">
							View More
						</div>
					</a>
				</div>
			</div>

			<div class="col-sm-4 mb-2">
				<div class="card border-success">
					<div class="card-body bg-success text-white">
						  <h5 class="card-title"><i class="fa fa-shopping-cart"></i> Product  <span class="card-text float-right">{{ $product }}</span></h5>
					</div>
					<a href="{{ url('admin/inventory') }}" >
						<div class="card-footer">
							View More
						</div>
					</a>
				</div>
			</div>

		</div>
	@endif

	<div class="row">
		<div class="col-sm-12">
		<div class="card mt-3">
			<div class="card-header">Student Progress</div>
			<div class="card-body">
				<table class="table table-bordered dt-responsive nowrap" id="tbl_student" style="width: 100%;"></table>
			</div>
			<div class="card-footer"></div>
		</div></div>
	</div>
</div>

<div class="modal fade" role="dialog" id="modal_progress">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title student_name">
        	Loading...
        </div>
        <button class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
           <div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header"><span class="student_name"></span> Progress</div>
					<div class="card-body">
						<div class="container" id="container"></div>
					</div>
					<div class="card-footer"></div>
				</div>


				<div class="card mt-2">
					<div class="card-header">Report Details</div>
					<div class="card-body">
						<table class="table table-bordered" id="user_table_detail" style="width: 100%;"></table>
					</div>
					<div class="card-footer"></div>
				</div>
			</div>
		</div>
      </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>


</body>
	@include('Layout.footer', ['type' => 'admin'])
</html>

<script type="text/javascript">
	var tbl_student;
	
	function show_student(){
			if (tbl_student) {
				tbl_student.destroy();
			}
	
			var url = main_path + '/student/list';
			tbl_student = $('#tbl_student').DataTable({
	        pageLength: 10,
	        responsive: true,
	        ajax: url,
	        deferRender: true,
				language: {
					 "emptyTable": "No data available"
				},
	        columns: [
	         {
	         	 className: '',
	            "data": "name",
	            "title": "Name",
	          },{
	         	 className: '',
	            "data": "username",
	            "title": "Username",
	          },{
	         	 className: '',
	            "data": "email",
	            "title": "Email",
	          },{
	            className: 'width-option-1 text-center',
	            "data": "user_id",
	            "orderable": false,
	            "title": "Options",
	            "render": function(data, type, row, meta){
	              var param_data = JSON.stringify(row);
	              // console.log();
	              // var data_param = JSON.parse(param_data);
	              	newdata = '<button class="btn btn-info btn-sm font-base mt-1" data-info=\''+param_data.trim()+'\' type="button" onclick="show_progress(this)"><i class="fa fa-eye"></i> Progress</button>';
	              
	              return newdata;
	            }
	          }
	        ]
	        });
		}
</script>

<script type="text/javascript">


	function show_progress(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		var id = data.user_id;

		get_graph(id);
		show_user_table_detail(id)		
		$("#modal_progress").modal('show');
	}

	function get_graph(id){

		var url = main_path + '/student/progress/' + id;

		$.ajax({
		    type:"GET",
		    url:url,
		    data:{},
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		      // console.log(response);
		     if (response.status == true) {
		        // swal("Success", response.message, "success");
		        show_charts(response.data);
		     }else{
		      console.log(response);
		     }
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });
	}

	function show_charts(datas){
	// Create the chart
	var data_info = JSON.parse(datas);
	var marker = (data_info.length > 0) ? data_info[0].pass_marker : 0;

	// console.log(datas);
	Highcharts.chart('container', {
	  chart: {
	    type: 'column'
	  },
	  title: {
	    text: 'Course Statistics'
	  },
	  subtitle: {
	    text: ''
	  },
	  accessibility: {
	    announceNewData: {
	      enabled: true
	    }
	  },
	  xAxis: {
	    type: 'category'
	  },
	  yAxis: {
	    title: {
	      text: 'Total Percentage'
	    }

	  },
	  legend: {
	    enabled: false
	  },
	  plotOptions: {
	    series: {
	      borderWidth: 0,
	      dataLabels: {
	        enabled: true,
	        format: '{point.y:.1f}%'
	      }
	    }
	  },

	  tooltip: {
	    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
	  },
	  series: [
	    {
	      name: "",
	      colorByPoint: false,
	      data: JSON.parse(datas)
	    }
	  ]
	},function(chart){

        $.each(chart.series[0].data,function(i,data){
		  var max = data.pass_marker;

			console.log(max);
			console.log(data.y);
            if(data.y >= max){
				data.update({
						color:'#1cc88a'
					});
			}else{
				data.update({
						color:'#e74a3b'
					});
			}
        });

    });
}
</script>

<script type="text/javascript">
	var user_table_detail;
	
	function show_user_table_detail(id){
		if (user_table_detail) {
			user_table_detail.destroy();
		}
		var url = main_path + '/student/progress/table/' + id;
		user_table_detail = $('#user_table_detail').DataTable({
		pageLength: 10,
		responsive: true,
		ajax: url,
		deferRender: true,
		language: {
		"emptyTable": "No data available"
	},
		columns: [{
		className: '',
		"data": "name",
		"title": "Name",
	},{
		className: '',
		"data": "score",
		"title": "Score",
	},{
		className: '',
		"data": "y",
		"title": "Percentage",
	},{
		className: '',
		"data": "passing_mark",
		"title": "Passing mark",
	},{
		className: '',
		"data": "pass_marker",
		"title": "Percentage",
	},{
		className: '',
		"data": "total_points",
		"title": "Total Question",
	},{
		className: 'width-option-1 text-center',
		"data": "purchased_id",
		"orderable": false,
		"title": "Options",
			"render": function(data, type, row, meta){
				// var param_data = JSON.stringify(row);
				newdata = '';
				newdata += '<a target="_blank" href="'+url_path('/student/exam/'+row.purchased_id )+'" class="btn btn-info btn-sm font-base mt-1" ><i class="fa fa-search"></i> View</a>';
				return newdata;
			}
		}
	]
	});
	}
</script>