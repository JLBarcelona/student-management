<!DOCTYPE html>
<html>
	@include('Layout.header', ['type' => 'admin', 'title' => 'admin', 'icon' => asset('img/logophone.png') ])
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
<body class="font-base" onload="show_form_table();">
	@include('Layout.nav', ['type' => 'admin'])
	<div class="container-fluid mobile-margin">
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header">
					Worksheet
					<button class="btn btn-primary btn-sm float-right" onclick="add_form();"><i class="fa fa-plus"></i> Add Worksheet</button>
					</div>
					<div class="card-body">
						<table class="table table-bordered dt-responsive nowrap" id="table_form" style="width: 100%;"></table>
					</div>
				</div>
			</div>
		</div>
	</div>

	 <div class="modal fade" role="dialog" id="add_form_modal">
	      <div class="modal-dialog">
	        <div class="modal-content">
	          <div class="modal-header">
	            <div class="modal-title">
	            Add Worksheet
	            </div>
	            <button class="close" data-dismiss="modal">&times;</button>
	          </div>
	          <div class="modal-body">
	           <form class="needs-validation" id="form_add_new" action="{{ url('/admin/form/create') }}" novalidate>
								<div class="form-row">
									<div class="form-group col-sm-12">
										<label>Name </label>
										<input type="text" id="name" name="name" placeholder="Form Name" class="form-control " required>
										<div class="invalid-feedback" id="err_name"></div>
									</div>
									<div class="form-group col-sm-12">
											<label>Passing Mark </label>
											<input type="text" id="passing_mark" name="passing_mark" placeholder="Passing Mark" class="form-control " required>
											<div class="invalid-feedback" id="err_passing_mark"></div>
									</div>
									<div class="form-group col-sm-12">
											<label>Question Choose Option</label>
											<select class="form-control" name="choose_option" id="choose_option">
												<option value="1">Let the system generate the questions</option>
												<option value="0">Let me choose the questions</option>
											</select>
											<div class="invalid-feedback" id="err_check_option"></div>
									</div>
									<div class="form-group col-sm-12">
											<label>Form Timer </label>
											<div class="input-group mb-3 date" id="datetimepicker1">
				                <input type="text" id="form_timer" name="form_timer" class="form-control" value="00:15:00">
				                <div class="input-group-append">
				                  <span class="input-group-text"><i class="fa fa-clock"></i></span>
				                </div>
				              </div>
									</div>

									<div class="col-sm-12 text-right">
										<button class="btn btn-dark btn-sm" type="submit">Save</button>
									</div>
								</div>
							</form>
	          </div>
	          <div class="modal-footer">

	          </div>
	        </div>
	      </div>
	    </div>
</body>
	@include('Layout.footer', ['type' => 'admin'])
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
</html>
<script type="text/javascript">

	function add_form(){
		$("#add_form_modal").modal('show');
	}
</script>

<script type="text/javascript">
	$(function(){
		$('#datetimepicker1').datetimepicker({
		    format: 'HH:mm:ss',
		    allowInputToggle: true,
		});

		});
</script>
<script>

	var table_form;

		function show_form_table(){
			if (table_form) {
				table_form.destroy();
			}

			var url = main_path + '/form/list';
			table_form = $('#table_form').DataTable({
	        pageLength: 10,
	        responsive: true,
	        ajax: url,
	        deferRender: true,
			language: {
				 "emptyTable": "No data available"
			},
	        columns: [
			{
				className: '',
				"data": "name",
				"title": "Name",
	          },{
				className: '',
				"data": "form_timer",
				"title": "Timer",
	          },{
				className: '',
				"data": "passing_mark",
				"title": "Passing Mark",
	          },{
				className: '',
				"data": "created_at",
				"title": "Date Created",
	          },{
	            className: 'width-option-1 text-center',
	            "data": "mainform_id",
	            "orderable": false,
	            "title": "Options",
	            "render": function(data, type, row, meta){
	              var param_data = JSON.stringify(row);
	              // console.log();
	              // var data_param = JSON.parse(param_data);
								if (row.is_auto_choose == 1) {
									newdata = '<a class="btn btn-success btn-sm mt-1" href="'+url_path('/admin/form/edit/'+row.mainform_id)+'"><i class="fa fa-edit"></i> Edit</a>\ <button class="btn btn-danger btn-sm font-base mt-1" data-info=\''+param_data.trim()+'\' type="button" onclick="delete_form(this)"><i class="fa fa-trash"></i> Delete</button>';
								}else{
									newdata = '<a class="btn btn-success btn-sm mt-1" href="'+url_path('/admin/form/manual/edit/'+row.mainform_id)+'"><i class="fa fa-edit"></i> Edit</a>\ <button class="btn btn-danger btn-sm font-base mt-1" data-info=\''+param_data.trim()+'\' type="button" onclick="delete_form(this)"><i class="fa fa-trash"></i> Delete</button>';
								}

	              return newdata;
	            }
	          }
	        ]
	        });
		}

	$("#form_add_new").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
				console.log(response);

				if(response.status == true){
					// console.log(response);
					showValidator(response.error,'form_add_new');
					swal({
							title: "Success",
							text: response.message,
							type: "success",
							showCancelButton: false,
							confirmButtonColor: "#007bff",
							confirmButtonText: "Okay",
						},
						function(isConfirm){
							if (isConfirm) {
								window.location = main_path + '/admin/form/edit/' + response.id;
							}
						});

				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'form_add_new');
				}
			},
			error:function(error){
				console.log(error);
			}
		});
	});


	function delete_form(_this){
	  var data = JSON.parse($(_this).attr('data-info'));
	  var url = main_path + '/form/delete/' + data.mainform_id;
	  swal({
	        title: "Are you sure?",
	        text: "Do you want to delete this worksheet ?",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "Yes",
	        closeOnConfirm: false
	      },
	        function(){
	         $.ajax({
	          type:"GET",
	          url:url,
	          data:{},
	          dataType:'json',
	          beforeSend:function(){
	          },
	          success:function(response){
	            // console.log(response);
	           if (response.status == true) {
	              swal("Success", response.message, "success");
	              show_form_table();
	           }else{
	            console.log(response);
	           }
	          },
	          error: function(error){
	            console.log(error);
	          }
	        });
	      });
	  }
</script>
