<!DOCTYPE html>
<!--
	Copyright (c) 2014-2020, CKSource - Frederico Knabben. All rights reserved.
	This file is licensed under the terms of the MIT License (see LICENSE.md).
-->
<!--
<style type="text/css">
	figure .table > table{
		border-collapse: collapse;
		width: 100%;
		margin-bottom: 1rem;
		color: #212529;
	}
	figure .table > table td, .table th {
	    padding: .75rem;
	    vertical-align: top;
	    border-top: 1px solid #dee2e6;
	}
	figure .table > table,  td, th{
		    border: 1px solid #dee2e6;
	}
</style> -->
<style type="text/css">
	iframe #FMathEd1_ads{
		display: none !important;
	}
	 .cke_button__fmatheditor_icon{
		background-image: url('{{ asset("img/MathEditor.png")  }}') !important;
	}
	textarea.cke_dialog_ui_input_textarea{
		outline: none !important;
	}
</style>
<html>
	@include('Layout.header', ['type' => 'admin', 'title' => 'admin', 'icon' => asset('img/logophone.png') ])
	<!-- <link rel="stylesheet" href="{{ asset('ckeditor/sample/styles.css') }}"> -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML"></script>
	<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<body class="font-base" onload="show_question();"  data-editor="ClassicEditor" data-collaboration="false">
	@include('Layout.nav', ['type' => 'admin'])
	<div class="container-fluid mobile-margin">
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header font-base-lg bold cat_name">{{ $category->category_name }}
						<button onclick="add_question();" class="btn btn-primary btn-sm float-right"><i class="fa fa-plus"></i> Add Question</button>
						<button onclick="edit_title();" class="btn btn-success btn-sm float-right mr-1"><i class="fa fa-cog"></i> Edit Title</button>
					</div>
					<div class="card-body">
						<div id="show_questions" data-id="{{ $category->category_id }}"></div>
					</div>
					<div class="card-footer"></div>
				</div>
			</div>
		</div>
	</div>
</body>


<div class="modal fade" role="dialog" id="modal_edit_category">
	<div class="modal-dialog">
	 <div class="modal-content">
		 <div class="modal-header">
			 <div class="modal-title">
			 Edit Title
			 </div>
			 <button class="close" data-dismiss="modal">&times;</button>
		 </div>
		 <div class="modal-body">
			 <form class="needs-validation" id="category_edit_form" action="{{ url('admin/category/create') }}" novalidate>
				 <div class="form-row">
					 <input type="hidden" id="category_id" value="{{ $category->category_id }}" name="category_id" placeholder="" class="form-control" required>
					 <div class="form-group col-sm-12">
						 <label>Subject Name </label>
						 <input type="text" id="category_name" name="category_name" value="{{ $category->category_name }}" placeholder="Subject name" class="form-control cat_name" required>
						 <div class="invalid-feedback" id="err_category_name"></div>
					 </div>

					 <div class="col-sm-12 text-right">
						 <button class="btn btn-dark btn-sm" type="submit">Save</button>
					 </div>
				 </div>
			 </form>
		 </div>
	 </div>
	</div>
</div>

	 <div class="modal fade" role="dialog" id="add_question_modal">
	      <div class="modal-dialog modal-xl">
	        <div class="modal-content">
	          <div class="modal-header">
	            <div class="modal-title">
	           	Add Question
	            </div>
	            <button class="close" data-dismiss="modal">&times;</button>
	          </div>
	          <div class="modal-body">
	          	<div id="accordion">
							  <div class="card rounded-0">
							    <div class="card-header rounded-0 bg-primary text-white" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							      <h5 class="mb-0 bold">
							         Question
							      </h5>
							    </div>

							    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
							      <div class="card-body">
								        <form class="needs-validation" id="add_question_form" action="{{ url('/question/add') }}" novalidate>
													<div class="form-row">
														<div class="form-group col-sm-12">
															<label for="checking_option">Question Type</label>
															<select class="form-control" name="checking_option" id="checking_option" oninput="if_open_ended(this);">
																<option value="1">Standard Questions with Answers</option>
																<option value="0">Open Ended Questions</option>
															</select>
															<div class="invalid-feedback" id="err_checking_option"></div>
														</div>

														<div class="form-group col-sm-12">
															<input type="hidden" name="category_id" id="category_id" value="{{ $category->category_id }}">
															<input type="hidden" id="question_id" name="question_id" placeholder="" class="form-control " required>
															<label>Question </label>
															<textarea id="question" name="question" placeholder="" class="form-control question" required></textarea>
															<div class="invalid-feedback" id="err_question"></div>
														</div>

														<div class="form-grouop col-sm-12 mt-2">
															<label>Explanation </label>
															<textarea name="explanation" id="explanation" class="form-control explanation"></textarea>
															<div class="invalid-feedback" id="err_explanation"></div>
														</div>

														<div class="col-sm-12 text-right mt-2">
															<button class="btn btn-dark btn-sm" type="submit">Save</button>
														</div>
													</div>
												</form>
							      </div>
							    </div>
							  </div>
							  <div class="card rounded-0" id="choices_tab">
							    <div class="card-header rounded-0 bg-primary text-white collapsed" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
							      <h5 class="mb-0 bold">
							          Choices
							      </h5>
							    </div>
							    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
							      <div class="card-body">
							       <form class="needs-validation" id="add_choices_form" action="{{ url('/choices/add') }}" novalidate>
										<div class="form-row">
										<div class="form-group col-sm-12">
												<input type="hidden" id="choice_id" name="choice_id" placeholder="" class="form-control ">
												<input type="hidden" id="question_id_cat" name="question_id_cat" placeholder="" class="form-control ">
												<label>Choice Answer </label>
												<textarea id="choice_answer" name="choice_answer" placeholder="" class="form-control " required></textarea>
												<div class="invalid-feedback" id="err_choice_answer"></div>
											</div>
											<div class="form-group col-sm-12">
												<label>Points </label>
												<input type="number" id="points" name="points" value="0" placeholder="Choices Points" class="form-control " required>
												<div class="invalid-feedback" id="err_points"></div>
											</div>

											<div class="col-sm-12 text-right">
												<button class="btn btn-dark btn-sm" type="submit">Save</button>
											</div>
										</div>
									</form>
						    	   </div>
						    	   <div class="card-body border">
						    	   		<table class="table table-bordered dt-responsive nowrap" style="width: 100%;" id="show_choices"></table>
						    	   </div>
							    </div>
							  </div>
							</div>

	          </div>
	          <div class="modal-footer">

	          </div>
	        </div>
	      </div>
	    </div>

	@include('Layout.footer', ['type' => 'admin'])
	<script type="text/javascript">
		CKEDITOR.replace('question');
		CKEDITOR.replace('choice_answer');
		CKEDITOR.replace('explanation');
	</script>
	<script src="{{ asset('ckeditor/adapters/jquery.js') }}"></script>
	<script src="{{ asset('js/Category/category.js') }}"></script>
	<script type="text/javascript">

	function if_open_ended(_this){
		// choices_tab
		console.log('a');
		if ($(_this).val() == 1) {
			$('#choices_tab').slideDown();
		}else{
			$('#choices_tab').slideUp();
		}
	}

	$("#category_edit_form").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		var new_name = $("#category_name").val();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					console.log(response);
					$(".cat_name").text(new_name);
					$(".cat_name").val(new_name);

					swal("Success", response.message, "success");
					showValidator(response.error,'category_edit_form');
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'category_edit_form');
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	});

	function edit_title(){
		$("#modal_edit_category").modal('show');
	}
	</script>

</html>
