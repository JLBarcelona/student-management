<!DOCTYPE html>
<html>
	@include('Layout.header', ['type' => 'admin', 'title' => 'Promo', 'icon' => asset('img/logophone.png') ])

<body class="font-base" onload="show_promo();">
	@include('Layout.nav', ['type' => 'admin'])
	<div class="container-fluid mobile-margin">
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header">Promo Code 
					 <button class="btn btn-primary btn-sm float-right" onclick="add_promo();"><i class="fa fa-plus"></i> Add Promo Code</button>
					</div>
					<div class="card-body">
						<table class="table table-bordered dt-responsive nowrap" id="tbl_promo" style="width: 100%;"></table>
					</div>
					<div class="card-footer"></div>
				</div>
			</div>
		</div>
	</div>
</body>
	@include('Layout.footer', ['type' => 'admin'])
</html>

 <div class="modal fade" role="dialog" id="modal_add_promo">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title">
            Add Promo
          
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <form class="needs-validation" id="promo_add_form" action="{{ url('promo/add') }}" novalidate>
				<div class="form-row">
					<input type="hidden" id="promo_id" name="promo_id" placeholder="" class="form-control" required>
					<div class="form-group col-sm-12">
						<label>Promo Code </label>
						<input type="text" id="promo_code" name="promo_code" placeholder="Promo Code" class="form-control " required>
						<div class="invalid-feedback" id="err_promo_code"></div>
					</div>
					<div class="form-group col-sm-12">
						<label>Promo Name </label>
						<input type="text" id="promo_name" name="promo_name" placeholder="Promo Name" class="form-control " required>
						<div class="invalid-feedback" id="err_promo_name"></div>
					</div>
					<div class="form-group col-sm-12">
						<label>Promo Description </label>
						<textarea type="text" id="promo_description" name="promo_description" placeholder="Promo Description" class="form-control " required></textarea>
						<div class="invalid-feedback" id="err_promo_description"></div>
					</div>
					<div class="form-group col-sm-12">
						<label>Price Min </label>
						<input type="number" id="price_min" name="price_min" placeholder="Minimmum Price" class="form-control " required>
						<div class="invalid-feedback" id="err_price_min"></div>
					</div>
					<div class="form-group col-sm-12">
						<label>Price Discount </label>
						<input type="number" id="price_discount" name="price_discount" placeholder="Discount" class="form-control " required>
						<div class="invalid-feedback" id="err_price_discount"></div>
					</div>

					<div class="col-sm-12 text-right">
						<button class="btn btn-dark btn-sm" type="submit">Submit</button>
					</div>
				</div>
			</form>
          </div>
          <div class="modal-footer">
            
          </div>
        </div>
      </div>
    </div>


    <!-- Javascript Function-->
<script>
	function add_promo(){
		clear_promo_form();
		$("#modal_add_promo").modal('show');
	}

	var tbl_promo;
	function show_promo(){
		if (tbl_promo) {
			tbl_promo.destroy();
		}
		var url = main_path + '/promo/list';
		tbl_promo = $('#tbl_promo').DataTable({
		pageLength: 10,
		responsive: true,
		ajax: url,
		deferRender: true,
		language: {
		"emptyTable": "No data available"
	},
		columns: [{
		className: '',
		"data": "promo_name",
		"title": "Promo_name",
	},{
		className: '',
		"data": "promo_code",
		"title": "Promo_code",
	},{
		className: '',
		"data": "promo_description",
		"title": "Promo_description",
	},{
		className: '',
		"data": "price_min",
		"title": "Price_min",
	},{
		className: '',
		"data": "price_discount",
		"title": "Price_discount",
	},{
		className: 'width-option-1 text-center',
		"data": "promo_id",
		"orderable": false,
		"title": "Options",
			"render": function(data, type, row, meta){
				var param_data = JSON.stringify(row);
				newdata = '';
				newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-info=\''+param_data.trim()+'\' onclick="edit_promo(this)" type="button"><i class="fa fa-edit"></i> Edit</button> ';
				newdata += '<button class="btn btn-danger btn-sm font-base mt-1" data-info=\''+param_data.trim()+'\' onclick="delete_promo(this)" type="button"><i class="fa fa-edit"></i> delete</button>';
				return newdata;
			}
		}
	]
	});
	}

	$("#promo_add_form").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
				if(response.status == true){
					console.log(response)
					clear_promo_form();
					$("#modal_add_promo").modal('hide');
					show_promo();
					swal("Success", response.message, "success");
					showValidator(response.error,'promo_add_form');
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'promo_add_form');
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	});

	function delete_promo(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		var url =  main_path + '/promo/delete/' + data.promo_id;
			swal({
				title: "Are you sure?",
				text: "Do you want to delete this promo?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				closeOnConfirm: false
			},
			function(){
				$.ajax({
				type:"GET",
				url:url,
				data:{},
				dataType:'json',
				beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					swal("Success", response.message, "success");
					show_promo();
				}else{
					console.log(response);
				}
			},
			error: function(error){
				console.log(error);
			}
			});
		});
	}

	function clear_promo_form(){
		$('#promo_id').val('');
		$('#promo_name').val('');
		$('#promo_code').val('');
		$('#promo_description').val('');
		$('#price_min').val('');
		$('#price_discount').val('');
	}

	function edit_promo(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		$('#promo_id').val(data.promo_id);
		$('#promo_name').val(data.promo_name);
		$('#promo_code').val(data.promo_code);
		$('#promo_description').val(data.promo_description);
		$('#price_min').val(data.price_min);
		$('#price_discount').val(data.price_discount);
		$("#modal_add_promo").modal('show');
	}
</script>