<!DOCTYPE html>
<html>
	@include('Layout.header', ['type' => 'admin', 'title' => 'Bundle', 'icon' => asset('img/logophone.png') ])
<style type="text/css">
	#tbl_product_list_wrapper .col-sm-12{
		padding:10px !important;
	}
</style>
<body class="font-base" onload="show_bundle();">
	@include('Layout.nav', ['type' => 'admin'])
	<div class="container-fluid mobile-margin">
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header">Bundle Product
						<button class="btn btn-primary btn-sm float-right" onclick="add_bundle();"><i class="fa fa-plus"></i> Add Bundle</button>
					</div>
					<div class="card-body">
						<table class="table table-bordered dt-responsive nowrap" id="tbl_bundle" style="width: 100%;"></table>
					</div>
					<div class="card-footer"></div>
				</div>
			</div>
		</div>
	</div>
</body>
	@include('Layout.footer', ['type' => 'admin'])
</html>

 <div class="modal fade" role="dialog" id="modal_add_bundle">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title">
            Add Product Bundle
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <form class="needs-validation" id="bundle_add_form" action="{{ url('bundle/add') }}" novalidate>
				<div class="form-row">
					<input type="hidden" id="bundle_id" name="bundle_id" placeholder="" class="form-control" required>
					<div class="form-group col-sm-12">
						<label>Bundle Name </label>
						<input type="text" id="bundle_name" name="bundle_name" placeholder="Bundle Name" class="form-control " required>
						<div class="invalid-feedback" id="err_bundle_name"></div>
					</div>
					<div class="form-group col-sm-12">
						<label>Bundle Price </label>
						<input type="number" id="bundle_price" name="bundle_price" placeholder="Bundle Price" class="form-control " required>
						<div class="invalid-feedback" id="err_bundle_price"></div>
					</div>

					<div class="col-sm-12">
						<hr>
						<label>Select Product</label>
						<table class="table table-bordered dt-responsive nowrap" id="tbl_product_list" style="width: 100% !important;">
							<thead>
								<tr>
									<th><input type="checkbox" name="select_all" id="select_all" class="check_all"></th>
									<th>Product Name</th>
									<th>Price</th>
								</tr>
							</thead>

							<tbody>
								@foreach($product as $row)
								<tr>
									<td><input type="checkbox" name="product_item[{{ $row->product_id }}]" value="{{ $row->product_id }}" id="select_item_{{ $row->product_id }}" class="check_item"></td>
									<td>{{ $row->mainform->name }}</td>
									<td>{{ $row->price }}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>

					<div class="col-sm-12 text-right">
						<button class="btn btn-dark btn-sm" type="submit">Save</button>
					</div>
				</div>
			</form>
          </div>
          <div class="modal-footer">
            
          </div>
        </div>
      </div>
    </div>


    <!-- Javascript Function-->
<script>

	$(document).ready(function(){
	    $(".check_all").click(function(){
	    	if ($(this).prop('checked')) {
	    		$(".check_item").prop("checked", true);
	    	}else{
    			$(".check_item").prop("checked", false);
	    	}
	    });
	
		$(".check_item").on('input', function(){
			var selector = $("#select_all");

		   if ($('.check_item:checked').length == $('.check_item').length) {
				selector.prop("checked", true);
	    	}else{
				selector.prop("checked", false);
	    	}
		});

	});

	function check_checkbox(){
		var selector = $("#select_all");

	   if ($('.check_item:checked').length == $('.check_item').length) {
			selector.prop("checked", true);
    	}else{
			selector.prop("checked", false);
    	}
	}


	$("#tbl_product_list").dataTable();

	function add_bundle(){
		clear_bundle_form();
		$(".check_item").prop("checked", false);
		$(".check_all").prop("checked", false);
		$("#modal_add_bundle").modal('show');
	}

	var tbl_bundle;
	function show_bundle(){
		if (tbl_bundle) {
			tbl_bundle.destroy();
		}
		var url = main_path + '/bundle/list';
		tbl_bundle = $('#tbl_bundle').DataTable({
		pageLength: 10,
		responsive: true,
		ajax: url,
		deferRender: true,
		language: {
		"emptyTable": "No data available"
	},
		columns: [{
		className: '',
		"data": "bundle_name",
		"title": "Bundle Name",
	},{
		className: '',
		"data": "bundle_price",
		"title": "Bundle Price",
	},{
		className: 'width-option-1 text-center',
		"data": "bundle_id",
		"orderable": false,
		"title": "Options",
			"render": function(data, type, row, meta){
				var param_data = JSON.stringify(row);
				newdata = '';
				newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-info=\''+param_data.trim()+'\' onclick="edit_bundle(this)" type="button"><i class="fa fa-edit"></i> Edit</button> ';
				newdata += '<button class="btn btn-danger btn-sm font-base mt-1" data-info=\''+param_data.trim()+'\' onclick="delete_bundle(this)" type="button"><i class="fa fa-edit"></i> delete</button>';
				return newdata;
			}
		}
	]
	});
	}

	$("#bundle_add_form").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		// alert(mydata);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
				if(response.status == true){
					// console.log(response)
					clear_bundle_form();
					$("#modal_add_bundle").modal('hide');
					show_bundle();
					swal("Success", response.message, "success");
					showValidator(response.error,'bundle_add_form');
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'bundle_add_form');
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	});

	function delete_bundle(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		var url =  main_path + '/bundle/delete/' + data.bundle_id;
			swal({
				title: "Are you sure?",
				text: "Do you want to delete this bundle?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				closeOnConfirm: false
			},
			function(){
				$.ajax({
				type:"GET",
				url:url,
				data:{},
				dataType:'json',
				beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					show_bundle();
					swal("Success", response.message, "success");
				}else{
					console.log(response);
				}
			},
			error: function(error){
				console.log(error);
			}
			});
		});
	}

	function clear_bundle_form(){
		$('#bundle_id').val('');
		$('#bundle_name').val('');
		$('#bundle_price').val('');
	}

	function edit_bundle(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		// console.log(data.items[0].product_id);

		for (var i = 0; i < data.items.length; i++) {
			// console.log(data.items[i]);
			$("#select_item_" + data.items[i].product_id).prop('checked', true);
		}
		check_checkbox();

		$('#bundle_id').val(data.bundle_id);
		$('#bundle_name').val(data.bundle_name);
		$('#bundle_price').val(data.bundle_price);
		$("#modal_add_bundle").modal('show');
	}
</script>