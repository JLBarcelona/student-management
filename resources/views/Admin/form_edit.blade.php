<!DOCTYPE html>
<html>
	@include('Layout.header', ['type' => 'admin', 'title' => 'admin', 'icon' => asset('img/logophone.png') ])

<body class="font-base" onload="show_category_question();">
	@include('Layout.nav', ['type' => 'admin'])
	<div class="container-fluid mobile-margin">
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header bg-info text-white font-base-xl bold">{{ $form->name }}
						<button onclick="add_category();" class="btn btn-primary btn-sm float-right border"><i class="fa fa-plus"></i> Add Subject</button>
						<button class="btn btn-success btn-sm float-right border mr-1" data-toggle="modal" data-target="#modal_edit_form"><i class="fa fa-edit"></i> Edit Form</button>

					</div>
					<div class="card-body">
						<div id="form_category_question" data-id="{{ $form->mainform_id }}"></div>
					</div>
					<div class="card-footer"></div>
				</div>
			</div>
		</div>
	</div>
</body>
	@include('Layout.footer', ['type' => 'admin'])
</html>
 <div class="modal fade" role="dialog" id="add_category_modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title">
            Add Subject
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
	         <form class="needs-validation" id="add_category_form" action="{{ url('/form/add/category') }}" novalidate>
				<div class="form-row">
					<input type="hidden" id="form_detail_id" name="form_detail_id" placeholder="" class="form-control " required>
					<input type="hidden" id="mainform_id" name="mainform_id" placeholder="" class="form-control "  value="{{ $form->mainform_id }}">

					<div class="form-group col-sm-12">
						<label>Subject</label>
						<select id="category" name="category" class="form-control">
							<option selected="" value="">Select Category</option>
							@foreach($category as $row)
							<option value="{{ $row->category_id }}">{{ $row->category_name }}</option>
							@endforeach
						</select>
						<div class="invalid-feedback" id="err_category"></div>
					</div>
					<div class="form-group col-sm-12">
						<label>Question Number </label>
						<input type="number" id="question_load" name="question_load" placeholder="Number Of Question" class="form-control " required>
						<div class="invalid-feedback" id="err_question_load"></div>
					</div>

					<div class="col-sm-12 text-right">
						<button class="btn btn-dark btn-sm" type="submit">Save</button>
					</div>
				</div>
			</form>
          </div>
          <div class="modal-footer">

          </div>
        </div>
      </div>
    </div>

     <div class="modal fade" role="dialog" id="modal_edit_form">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <div class="modal-title">
                	Edit Worksheet
                </div>
                <button class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                <form class="needs-validation" id="form_add_new" action="{{ url('/admin/form/create') }}" novalidate>
									<div class="form-row">
									<div class="form-group col-sm-12">
											<input type="hidden" name="choose_option" id="choose_option" value="{{ $form->is_auto_choose }}">
											<input type="hidden" id="mainform_id_edit" name="mainform_id" placeholder="" value="{{ $form->mainform_id }}" class="form-control">
											<label>Name </label>
											<input type="text" id="name" name="name" placeholder="Form Name" value="{{ $form->name }}" class="form-control " required>
											<div class="invalid-feedback" id="err_name"></div>
										</div>
										<div class="form-group col-sm-12">
											<label>Passing Mark </label>
											<input type="number" id="passing_mark" name="passing_mark" placeholder="Passing Mark" value="{{ $form->passing_mark }}" class="form-control " required>
											<div class="invalid-feedback" id="err_passing_mark"></div>
										</div>
									<div class="form-group col-sm-12">
											<label>Form Timer </label>
											<input type="text" id="form_timer" name="form_timer" placeholder="" value="{{ $form->form_timer }}" class="form-control " required>
											<div class="invalid-feedback" id="err_form_timer"></div>
										</div>


										<div class="col-sm-12 text-right">
											<button class="btn btn-dark btn-sm" type="submit">Save</button>
										</div>
									</div>
								</form>
              </div>
              <div class="modal-footer">

              </div>
            </div>
          </div>
        </div>

    <script type="text/javascript">
    	function add_category(){
    		$("#form_detail_id").val('');
    		$("#category").val('');
    		$("#question_load").val('');
    		$("#add_category_modal").modal('show');
    	}
    </script>


    <script>


    $("#form_add_new").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
				console.log(response);

				if(response.status == true){
					// console.log(response);
					showValidator(response.error,'form_add_new');
					swal("Success","Form updated successfully!","success");

				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'form_add_new');
				}
			},
			error:function(error){
				console.log(error);
			}
		});
	});



	$("#add_category_form").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
				if(response.status == true){
					console.log(response)
					showValidator(response.error,'add_category_form');
					show_category_question();
					swal("Success",response.message,"success");
					$("#form_detail_id").val('');
		    		$("#category").val('');
		    		$("#question_load").val('');
		    		$("#add_category_modal").modal('hide');
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'add_category_form');
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	});


	function show_category_question(){
		var selector = $("#form_category_question");
		var id = selector.attr('data-id');

		var url = main_path + '/form/category/question/' + id;

		$.ajax({
		    type:"GET",
		    url:url,
		    data:{},
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		      // console.log(response);
		     if (response.status == true) {
		        selector.html(response.output);
		     }else{
		      console.log(response);
		     }
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });
	}


function delete_category(_this){
	var data = JSON.parse($(_this).attr('data-info'));
	var url = main_path + '/form/category/delete/' + data.form_detail_id;
  swal({
        title: "Are you sure?",
        text: "Do you want to delete this category?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false
      },
        function(){
         $.ajax({
          type:"GET",
          url:url,
          data:{},
          dataType:'json',
          beforeSend:function(){
          },
          success:function(response){
            // console.log(response);
           if (response.status == true) {
              swal("Success", response.message, "success");
              show_category_question();
           }else{
            console.log(response);
           }
          },
          error: function(error){
            console.log(error);
          }
        });
      });
}

// function publish(id){
// 	var url = main_path + '/generate_form_order/' + id;
// 	 swal({
//         title: "Are you sure?",
//         text: "Do you want to publish this form ?",
//         type: "info",
//         showCancelButton: true,
//         confirmButtonColor: "#DD6B55",
//         confirmButtonText: "Yes",
//         closeOnConfirm: false
//       },
//         function(){
//      	$.ajax({
// 		    type:"POST",
// 		    url:url,
// 		    data:{},
// 		    dataType:'json',
// 		    beforeSend:function(){
// 		    },
// 		    success:function(response){
// 		      console.log(response);
// 		     if (response.status == true) {
// 		        swal("Success", response.message, "success");
// 		     }else{
// 		        swal("Error", response.message, "error");

// 		      console.log(response);
// 		     }
// 		    },
// 		    error: function(error){
// 		      console.log(error);
// 		    }
// 		  });
//       });
// }



function edit_category(_this){
	var data = JSON.parse($(_this).attr('data-info'));
	// console.log(data.category_id);
	$("#form_detail_id").val(data.form_detail_id);
	$("#category").val(data.category_id);
	$("#question_load").val(data.question_load);
	$("#add_category_modal").modal('show');
}
</script>
