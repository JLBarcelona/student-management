<!DOCTYPE html>
<html>
	@include('Layout.header', ['type' => 'admin', 'title' => 'Form Checking', 'icon' => asset('img/logophone.png') ])

<body class="font-base" onload="show_checking_form();">
	@include('Layout.nav', ['type' => 'admin'])
	<div class="container-fluid mobile-margin">
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header">
					Form Checking
					</div>
					<div class="card-body">
						<table class="table table-bordered dt-responsive nowrap" id="tbl_checking_form" style="width: 100%;"></table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
	@include('Layout.footer', ['type' => 'admin'])
</html>

<div class="modal fade" role="dialog" id="modal_show_question">
	<div class="modal-dialog modal-lg">
	  <div class="modal-content">
	    <div class="modal-header">
	      <div class="modal-title check_modal_title">
	      Title here
	      </div>
	      <button class="close" data-dismiss="modal">&times;</button>
	    </div>
	    <div class="modal-body">
				<form class="needs-validation" id="form_check_mark" action="{{ url('/teacher/score/question') }}" novalidate>
					<input type="hidden" id="answer_id" name="answer_id" placeholder="" class="form-control" required>
					<div class="form-row">
						<div class="col-sm-12">

								<div id="content_question">

								</div>
						</div>

						<div class="col-sm-12 form-group">
							<hr>
							<label for="">Score</label>
							<input type="number" name="score" id="score" placeholder="Enter Score" class="form-control">
							<div class="invalid-feedback" id="err_score"></div>
						</div>

						<div class="col-sm-12 form-group text-right">
							<button type="submit" name="button" class="btn btn-dark btn-sm">Save</button>
						</div>

					</div>
				</form>
	    </div>
	    <div class="modal-footer">

	    </div>
	  </div>
	</div>
</div>

<script type="text/javascript">
var tbl_checking_form;


function show_checking_form(){
	if (tbl_checking_form) {
		tbl_checking_form.destroy();
	}
	var url = main_path + '/teacher/check_list';
	tbl_checking_form = $('#tbl_checking_form').DataTable({
	pageLength: 10,
	responsive: true,
	ajax: url,
	deferRender: true,
	language: {
	"emptyTable": "No data available"
},
	columns: [{
	className: '',
	"data": "name",
	"title": "Worksheet",
	},{
	className: '',
	"data": "student",
	"title": "Student",
	},{
	className: '',
	"data": "answered_at",
	"title": "Answered Date",
	},{
	className: 'width-option-1 text-center',
	"data": "is_checked",
	"title": "Checked Date",
		"render": function(data, type, row, meta){
			newdata = (row.is_checked == '')? '<span class="badge badge-danger p-2">Pending</span>': '<span class="badge badge-success p-2">'+row.is_checked+'</span>';
			return newdata;
		}
	},{
	className: 'width-option-1 text-center',
	"data": "mainform_id",
	"orderable": false,
	"title": "Options",
		"render": function(data, type, row, meta){
			var param_data = JSON.stringify(row);
			newdata = '';
			newdata += ' <button class="btn btn-info btn-sm font-base mt-1" data-student=\''+row.student+'('+row.name+')'+'\'  data-id=\''+row.answer_id+'\' onclick="show_question(this)" type="button"><i class="fa fa-eye"></i> Take a look</button>';
			return newdata;
		}
	}
]
});
}

$("#form_check_mark").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					console.log(response)
					swal("Success", response.message, "success");
					show_checking_form();
					$("#modal_show_question").modal('hide');
					showValidator(response.error,'sample_form_id');
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'sample_form_id');
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	});


function show_question(_this){
	let id = $(_this).attr('data-id');
	let student = $(_this).attr('data-student');
	let url = main_path+'/teacher/check/' + id;
	$.ajax({
	    type:"GET",
	    url:url,
	    data:{},
	    dataType:'json',
	    beforeSend:function(){
				$(_this).text('Loading');
				$(_this).attr('disabled', true);
	    },
	    success:function(response){
	      console.log(response);
				$(_this).html('<i class="fa fa-eye"></i> Take a look');
				$(_this).attr('disabled', false);
	     if (response.status == true) {
	        // swal("Success", response.message, "success");
					let output = '';

					output += `<div class="card mb-1">
											<div class="card-header">Question</div>
											<div class="card-body">
											<center>`+response.data.questionaire+`</center>
											</div>
										</div>`;

					output += `<div class="card">
											<div class="card-header">Answer</div>
											<div class="card-body">
											<center>`+response.data.custom_answer+`</center>
											</div>
										</div>`;

					$("#content_question").html(output);
					$("#score").val(response.data.score);
					$("#answer_id").val(response.data.answer_id);
					$(".check_modal_title").text(student);
					$("#modal_show_question").modal({'backdrop' : 'static'});
	     }else{
	      console.log(response);
	     }
	    },
	    error: function(error){
	      console.log(error);
	    }
	  });
}

</script>
