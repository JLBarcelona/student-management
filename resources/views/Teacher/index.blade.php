<!DOCTYPE html>
<html>
	@include('Layout.header', ['type' => 'teacher', 'title' => 'login', 'icon' => asset('img/logophone.png') ])

<body class="font-base">
	@include('Layout.nav', ['type' => 'teacher'])
	<div class="container-fluid mobile-margin">
		<div class="row">
			<div class="col-sm-12"></div>
		</div>
	</div>
</body>
	@include('Layout.footer', ['type' => 'teacher'])
</html>