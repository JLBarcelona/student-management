<!DOCTYPE html>
<html>
	@include('Layout.header', ['type' => 'admin', 'title' => 'My Group', 'icon' => asset('img/logophone.png') ])

<body class="font-base">
	@include('Layout.nav', ['type' => 'admin'])
	<div class="container-fluid mobile-margin">
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header">
					{{ $group->group_name }}
					</div>
					<div class="card-body">
						<table class="table table-bordered dt-responsive nowrap" id="tbl_group_members" style="width: 100%;"></table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
	@include('Layout.footer', ['type' => 'admin'])
</html>

<div class="modal fade" role="dialog" id="show_member_forms">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title modal_member_title">
        Title here
        </div>
        <button class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
				<ul class="nav nav-tabs" id="myTab" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="home-tab" data-toggle="tab" href="#subject_tab" role="tab" aria-controls="home" aria-selected="true">Subject</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="profile-tab"  data-toggle="tab" href="#worksheet_tab" role="tab" aria-controls="profile" aria-selected="false">Worksheet</a>
					</li>
				</ul>
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade show active p-1 mt-2" id="subject_tab" role="tabpanel" aria-labelledby="home-tab">
							<table class="table table-bordered dt-responsive nowrap" id="subject_table" style="width: 100%;"></table>
					</div>
					<div class="tab-pane fade p-1 mt-2" id="worksheet_tab" role="tabpanel" aria-labelledby="profile-tab">
							<table class="table table-bordered" id="worksheet_table" style="width: 100%;"></table>
					</div>
				</div>
      </div>
      <div class="modal-footer">

      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
var tbl_group_members;

function show_member_forms(_this){
	let id = $(_this).attr('data-id');
	let name = $(_this).attr('data-name');

	$(".modal_member_title").text(name);
	$("#show_member_forms").modal({'backdrop' : 'static'});
}


show_table('{{ $group->group_category_id }}');

function show_table(id){
	var url = main_path + '/group/list_group_member/find/'+id;
	$.ajax({
    type:"GET",
    url:url,
    data:{},
    dataType:'json',
    beforeSend:function(){
    },
    success:function(response){
      // console.log(response);
			show_group_members(response.worksheet);
    },
    error: function(error){
      console.log(error);
    }
  });
}


function show_group_members(data){
	if (tbl_group_members) {
		tbl_group_members.destroy();
	}
	tbl_group_members = $('#tbl_group_members').DataTable({
	pageLength: 10,
	responsive: true,
	data: data,
	deferRender: true,
	language: {
	"emptyTable": "No data available"
},
	columns: [{
	className: '',
	"data": "name",
	"title": "Craeted By",
},{
className: '',
"data": "form_name",
"title": "Worksheet",
},{
className: '',
"data": "form_timer",
"title": "Timer",
},{
className: '',
"data": "passing_mark",
"title": "Passing Mark",
},{
	className: 'width-option-1 text-center',
	"data": "form_id",
	"orderable": false,
	"title": "Options",
		"render": function(data, type, row, meta){
			var param_data = JSON.stringify(row);
			newdata = '';
			newdata = '<a class="btn btn-success btn-sm mt-1" href="'+url_path('/admin/form/edit/'+row.form_id)+'"><i class="fa fa-edit"></i> Edit</a>';
			return newdata;
		}
	}
]
});
}
</script>
