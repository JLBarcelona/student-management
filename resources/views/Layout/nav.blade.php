<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

@if($type == 'home')

<nav class="navbar navbar-expand-lg navbar-dark nav-base-color nav-padding p-0">
  <a class="navbar-brand nav-item-base bold" href="#"><img src="{{ asset('img/logophone.png') }}" class="img-fluid img-thumbnail" width="94"></a>
</nav>

@elseif($type == 'admin')
<nav class="navbar navbar-expand-lg navbar-dark bg-dark nav-base-color nav-padding fixed-top">
      <a class="navbar-brand nav-item-base" href="{{ url('/admin') }}"><img src="{{ asset('img/logophone.png') }}" class="img-fluid img-thumbnail" width="94"></a>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav text-white bold">

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-white nav-item-base" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Worksheet
        </a>
        <ul class="dropdown-menu nav-base-color animated fadeIn" aria-labelledby="navbarDropdownMenuLink">
          <li><a class="dropdown-item nav-item-base text-white bold" href="{{ url('/admin/category') }}">Subject</a></li>
          <li><a class="dropdown-item nav-item-base text-white bold" href="{{ url('/admin/form') }}">Worksheet</a></li>
          <!-- <li><a class="dropdown-item nav-item-base text-white bold" href="{{ url('/admin/report') }}">Report</a></li> -->
        </ul>
      </li>

      @if(Auth::guard('admin')->user()->user_type == 1 )
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-white nav-item-base" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Products
        </a>
        <ul class="dropdown-menu nav-base-color animated fadeIn" aria-labelledby="navbarDropdownMenuLink">
            <li><a class="dropdown-item nav-item-base text-white bold" href="{{ url('/admin/inventory') }}">Product</a></li>
            <li><a class="dropdown-item nav-item-base text-white bold" href="{{ url('/admin/bundle') }}">Product Bundle</a></li>
            <li><a class="dropdown-item nav-item-base text-white bold" href="{{ url('/admin/promo') }}">Promo Code</a></li>
        </ul>
      </li>
      @endif

      @if(Auth::guard('admin')->user()->user_type == 3 )
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-white nav-item-base" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          My Group
        </a>
        <ul class="dropdown-menu nav-base-color animated fadeIn" id="my_group" aria-labelledby="navbarDropdownMenuLink">

        </ul>
      </li>
      @endif


      <li class="nav-item dropdown">
        <a class="nav-link text-white nav-item-base" href="{{ url('/admin/student') }}">Student</a>
      </li>


      @if(Auth::guard('admin')->user()->user_type == 3 )
      <li class="nav-item dropdown">
        <a class="nav-link text-white nav-item-base" href="{{ url('/admin/test/check') }}">Form Checking <span class="badge badge-danger hide counter_checking">0</span></a>
      </li>
      @endif

      @if(Auth::guard('admin')->user()->user_type == 1 )

      <li class="nav-item dropdown">
        <a class="nav-link text-white nav-item-base" href="{{ url('/admin/teacher') }}">Teacher</a>
      </li>
      @endif

      @if(Auth::guard('admin')->user()->user_type == 1 )
      <li class="nav-item dropdown">
        <a class="nav-link text-white nav-item-base" href="{{ url('/admin/group/category') }}">Groups</a>
      </li>
      @endif



      <li class="nav-item dropdown">
        <a class="nav-link text-white nav-item-base" href="#guide" onclick="user_guide();">User Guide</a>
      </li>
    </ul>

    <ul class="navbar-nav ml-auto">

         <li class="nav-item dropdown no-arrow mx-1 show">


         <a class="nav-link dropdown-toggle nav-item-base" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
             @if(!empty(Auth::guard('admin')->user()->profile_picture) || Auth::guard('admin')->user()->profile_picture !== null)
               <img src="{{ asset('/profile_picture/'.Auth::guard('admin')->user()->profile_picture) }}" class="img-fluid profile_pict" width="25">
             @else
            <i class="fas fa-user-circle fa-fw"></i>
           @endif
         </a>

         <!-- Dropdown - Messages -->
         <div class="dropdown-list dropdown-menu dropdown-menu-right shadow-lg animated--grow-in font-base" aria-labelledby="messagesDropdown">
           <div class="dropdown-item">
           @if(!empty(Auth::guard('admin')->user()->profile_picture) || Auth::guard('admin')->user()->profile_picture !== null)
               <img src="{{ guard('admin')->asset('/profile_picture/'. Auth::guard('admin')->user()->profile_picture) }}" class="img-fluid profile_pict" width="20">
                {{ Auth::user()->name }}
           @else

            <i class="fa fa-user-circle"></i> {{ Auth::guard('admin')->user()->name }}
           @endif

         </div>
         <div class="dropdown-divider"></div>

         <a class="dropdown-item  text-gray" href="{{ url('/Admin/config') }}"><i class="fa fa-cog"></i> Account Settings</a>
         <a class="dropdown-item  text-gray" href="{{ url('/logout/admin') }}"><i class="fa fa-sign-out-alt"></i> Logout</a>
         </div>
       </li>


     </ul>

  </div>
</nav>

@elseif($type == 'teacher')
<nav class="navbar navbar-expand-lg navbar-dark bg-dark nav-base-color nav-padding fixed-top">
    <a class="navbar-brand nav-item-base" href="{{ url('/admin') }}"><img src="{{ asset('img/logophone.png') }}" class="img-fluid img-thumbnail" width="94"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav text-white bold">

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-white nav-item-base" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Form
        </a>
        <ul class="dropdown-menu nav-base-color animated fadeIn" aria-labelledby="navbarDropdownMenuLink">
          <li><a class="dropdown-item nav-item-base text-white bold" href="{{ url('/admin/category') }}">Form Category</a></li>
          <li><a class="dropdown-item nav-item-base text-white bold" href="{{ url('/admin/form') }}">Form</a></li>
          <!-- <li><a class="dropdown-item nav-item-base text-white bold" href="{{ url('/admin/report') }}">Report</a></li> -->
        </ul>
      </li>


      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-white nav-item-base" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Products
        </a>
        <ul class="dropdown-menu nav-base-color animated fadeIn" aria-labelledby="navbarDropdownMenuLink">
            <li><a class="dropdown-item nav-item-base text-white bold" href="{{ url('/admin/inventory') }}">Product</a></li>
            <li><a class="dropdown-item nav-item-base text-white bold" href="{{ url('/admin/bundle') }}">Product Bundle</a></li>
            <li><a class="dropdown-item nav-item-base text-white bold" href="{{ url('/admin/promo') }}">Promo Code</a></li>
        </ul>
      </li>


      <li class="nav-item dropdown">
        <a class="nav-link text-white nav-item-base" href="{{ url('/admin/student') }}">User</a>
      </li>
    </ul>

    <ul class="navbar-nav ml-auto">

         <li class="nav-item dropdown no-arrow mx-1 show">


         <a class="nav-link dropdown-toggle nav-item-base" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
             @if(!empty(Auth::guard('admin')->user()->profile_picture) || Auth::guard('admin')->user()->profile_picture !== null)
               <img src="{{ asset('/profile_picture/'.Auth::guard('admin')->user()->profile_picture) }}" class="img-fluid profile_pict" width="25">
             @else
            <i class="fas fa-user-circle fa-fw"></i>
           @endif
         </a>

         <!-- Dropdown - Messages -->
         <div class="dropdown-list dropdown-menu dropdown-menu-right shadow-lg animated--grow-in font-base" aria-labelledby="messagesDropdown">
           <div class="dropdown-item">
           @if(!empty(Auth::guard('admin')->user()->profile_picture) || Auth::guard('admin')->user()->profile_picture !== null)
               <img src="{{ guard('admin')->asset('/profile_picture/'. Auth::guard('admin')->user()->profile_picture) }}" class="img-fluid profile_pict" width="20">
                {{ Auth::user()->name }}
           @else

            <i class="fa fa-user-circle"></i> {{ Auth::guard('admin')->user()->name }}
           @endif

         </div>
         <div class="dropdown-divider"></div>

         <a class="dropdown-item  text-gray" href="{{ url('/Admin/config') }}"><i class="fa fa-cog"></i> Account Settings</a>
         <a class="dropdown-item  text-gray" href="{{ url('/logout/admin') }}"><i class="fa fa-sign-out-alt"></i> Logout</a>
         </div>
       </li>


     </ul>

  </div>
</nav>


@elseif($type == 'student')
<nav class="navbar navbar-expand-lg navbar-dark bg-dark nav-base-color nav-padding fixed-top">
    <a class="navbar-brand nav-item-base" href="{{ url('/student') }}"><img src="{{ asset('img/logophone.png') }}" class="img-fluid img-thumbnail" width="94"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav text-white bold">
      <li class="nav-item dropdown">
        <a class="nav-link text-white nav-item-base" href="{{ url('/student/shop') }}">Shop now</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link text-white nav-item-base" href="{{ url('/student/form') }}">My Forms</a>
      </li>
    </ul>

    <ul class="navbar-nav ml-auto">

         <li class="nav-item dropdown no-arrow mx-1 show">


         <a class="nav-link dropdown-toggle nav-item-base" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
             @if(!empty(Auth::user()->profile_picture) || Auth::user()->profile_picture !== null)
               <img src="{{ asset('/profile_picture/'.Auth::user()->profile_picture) }}" class="img-fluid profile_pict" width="25">
             @else
            <i class="fas fa-user-circle fa-fw"></i>
           @endif
         </a>

         <!-- Dropdown - Messages -->
         <div class="dropdown-list dropdown-menu dropdown-menu-right shadow-lg animated--grow-in font-base" aria-labelledby="messagesDropdown">
           <div class="dropdown-item">
           @if(!empty(Auth::user()->profile_picture) || Auth::user()->profile_picture !== null)
               <img src="{{ asset('/profile_picture/'. Auth::user()->profile_picture) }}" class="img-fluid profile_pict" width="20">
                {{ Auth::user()->name }}
           @else

            <i class="fa fa-user-circle"></i> {{ Auth::user()->name }}
           @endif

         </div>
         <div class="dropdown-divider"></div>

         <a class="dropdown-item  text-gray" href="{{ url('/student/config') }}"><i class="fa fa-cog"></i> Account Settings</a>
         <a class="dropdown-item  text-gray" href="{{ url('/logout/user') }}"><i class="fa fa-sign-out-alt"></i> Logout</a>
         </div>
       </li>


     </ul>

  </div>
</nav>
@endif


<!-- Nav function -->

<script type="text/javascript">
$('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
    if (!$(this).next().hasClass('show')) {
        $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
    }
    var $subMenu = $(this).next(".dropdown-menu");
    $subMenu.toggleClass('show');
    $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
        $('.dropdown-submenu .show').removeClass("show");
    });
    return false;
});
</script>
