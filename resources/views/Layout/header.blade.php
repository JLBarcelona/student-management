<head>
<title>{{ $title }}</title>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<!-- $icon -->
<link rel="icon" type="image/png" href="{{ asset('img/logo_small.png') }}">
<meta name="csrf-token" content="{{ csrf_token() }}">

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('css/animate.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/sweetalert.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/themes/twitter/twitter.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">


@if($type == 'home')
<link rel="stylesheet" href="{{ asset('css/b3.css') }}">
<script src="https://www.google.com/recaptcha/api.js" async defer></script>

@elseif($type == 'admin')
	<!-- admin -->
<link rel="stylesheet" type="text/css" href="{{ asset('datatables/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('datatables/rowdata.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('datatables/rowresponsive.css') }}">
<link rel="stylesheet" href="{{ asset('css/croppie.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('dropzone/dropzone.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/simple-sidebar.css') }}">
@include('Layout.guide')
@elseif($type == 'teacher')
	<!-- teacher -->
<link rel="stylesheet" type="text/css" href="{{ asset('datatables/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('datatables/rowdata.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('datatables/rowresponsive.css') }}">
<link rel="stylesheet" href="{{ asset('css/croppie.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('dropzone/dropzone.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/simple-sidebar.css') }}">
@include('Layout.guide')

@elseif($type == 'student')
	<!-- student -->
<link rel="stylesheet" type="text/css" href="{{ asset('datatables/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('datatables/rowdata.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('datatables/rowresponsive.css') }}">
<link rel="stylesheet" href="{{ asset('css/croppie.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('dropzone/dropzone.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/simple-sidebar.css') }}">
@endif

</head>
