<!-- Common -->
<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.4.4/umd/popper.min.js"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/ajaxsetup.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/sweetalert.min.js') }}"></script>
<!-- <script type="text/javascript" src="{{ asset('js/index/index.js') }}"></script> -->
@if($type == 'home')

@elseif($type == 'admin')
<script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('datatables/rowdata.js') }}"></script>
<script type="text/javascript" src="{{ asset('datatables/rowresponsive.js') }}"></script>
<script type="text/javascript" src="{{ asset('datatables/dataTables.bootstrap4.min.js') }}"></script>

<script type="text/javascript">
  show_my_group();

  function show_my_group(){
    $.ajax({
    type:"GET",
    url:main_path+'/group/list_user_group/find',
    data:{},
    dataType:'json',
    beforeSend:function(){
    },
    success:function(response){
      // console.log(response);
      let data = response.data;
      let html_output = data.map(grp => {
        let output ='';
        output +='<li><a class="dropdown-item nav-item-base text-white bold" href="{{ url('/mygroup/show/') }}/'+grp.group_category_id+'">'+grp.group_name+'</a></li>';

        return output;
      });

      // console.log(html_output);

      $("#my_group").html(html_output);
    },
    error: function(error){
      console.log(error);
    }
  });
  }


@if(Auth::guard('admin')->user()->user_type == 3)
setInterval(function(){
  // var is_counter = 0;
  show_checking_counter();
}, 1000);


function show_checking_counter(){
  var url = main_path + '/teacher/check_list';
  $.ajax({
    type:"GET",
    url:url,
    data:{},
    dataType:'json',
    beforeSend:function(){
    },
    success:function(response){
      // console.log(response.counter+' checking');

      if (response.counter == 0) {
        $(".counter_checking").removeClass('animated bounceIn');
        $(".counter_checking").addClass('animated bounceOut', () =>{
          $(".counter_checking").addClass('hide');
        });
        $(".counter_checking").text(response.counter);
      }else{
        $(".counter_checking").removeClass('animated bounceOut hide');
        $(".counter_checking").addClass('animated bounceIn');
        $(".counter_checking").text(response.counter);
      }
    },
    error: function(error){
      console.log(error);
    }
  });
}
@endif



</script>
@elseif($type == 'teacher')
<script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('datatables/rowdata.js') }}"></script>
<script type="text/javascript" src="{{ asset('datatables/rowresponsive.js') }}"></script>
<script type="text/javascript" src="{{ asset('datatables/dataTables.bootstrap4.min.js') }}"></script>


@elseif($type == 'student')
<script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('datatables/rowdata.js') }}"></script>
<script type="text/javascript" src="{{ asset('datatables/rowresponsive.js') }}"></script>
<script type="text/javascript" src="{{ asset('datatables/dataTables.bootstrap4.min.js') }}"></script>
@endif
