<!DOCTYPE html>
<html>
	@include('Layout.header', ['type' => 'admin', 'title' => 'admin', 'icon' => asset('img/logophone.png') ])
	<link rel="stylesheet" href="{{ asset('select2/select2.css')  }}">
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

	<style media="screen">
		.select2-selection{
			display: block !important;
			width: 100% !important;
			height: calc(1.5em + 0.75rem + 2px) !important;
			/* padding: 0.375rem 0.75rem !important;R */
			font-size: 1rem !important;
			font-weight: 400 !important;
			line-height: 1.5 !important;
			color: #495057 !important;
			background-color: #fff !important;
			background-clip: padding-box !important;
			border: 1px solid #ced4da !important;
			border-radius: 0.25rem !important;
			transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
		}
		.select2-search__field{
			padding-left: 0.375rem !important;R
			font-size: 1rem !important;
			font-weight: 400 !important;
		}
	</style>
	<body class="font-base" onload="show_group();">
		@include('Layout.nav', ['type' => 'admin'])
		<div class="container-fluid mobile-margin">
			<div class="row">
				<div class="col-sm-12">
					<div class="card">
						<div class="card-header">Group Category <button class="btn btn-primary btn-sm float-right" onclick="add_group()"><i class="fa fa-plus"></i> Add Group</button></div>
						<div class="card-body">
              <table class="table table-bordered dt-responsive nowrap" id="tbl_group" style="width: 100%;"></table>
						</div>
						<div class="card-footer"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
	@include('Layout.footer', ['type' => 'admin'])
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
</html>

<div class="modal fade" role="dialog" id="modal_add_group">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title add_group_modal_title">
        <!-- Title here -->
        </div>
        <button class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <form class="needs-validation" id="group_form_add" action="{{ url('group/add_group') }}" novalidate>
        	<div class="form-row">
        		<input type="hidden" id="group_category_id" name="group_category_id" placeholder="" class="form-control" required>
        		<div class="form-group col-sm-12">
        			<label>Group Name </label>
        			<input type="text" id="group_name" name="group_name" placeholder="Group Name" class="form-control"  required>
        			<div class="invalid-feedback" id="err_group_name"></div>
        		</div>

							<div class="form-group col-sm-12">
							<label>Members</label>
							<select id="members" name="members[]" class="form-control" multiple style="width:100%;">

							</select>
							<div class="invalid-feedback" id="err_members"></div>
						</div>

        		<div class="col-sm-12 text-right">
        			<button class="btn btn-dark btn-sm" type="submit">Save</button>
        		</div>
        	</div>
        </form>

        <!-- Content Here -->
      </div>
      <div class="modal-footer">

      </div>
    </div>
  </div>
</div>

<div class="modal fade" role="dialog" id="modal_members_detail">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title member_title_modal">
        Member title
        </div>
        <button class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body p-2">
				<table class="table table-bordered dt-responsive nowrap" id="tbl_g_member" style="width: 100%;"></table>
      </div>
      <div class="modal-footer">

      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
var tbl_g_member;

function add_group(){
  $(".add_group_modal_title").text('Add Group');
  $("#modal_add_group").modal({'backdrop' : 'static'});
  $("#group_category_id").val('');
  $("#group_name").val('');
}

	function show_g_member(data){
		if (tbl_g_member) {
			tbl_g_member.destroy();
		}
		var url = main_path + '/g_member/list_g_member';
		tbl_g_member = $('#tbl_g_member').DataTable({
		pageLength: 10,
		responsive: true,
		data: data,
		deferRender: true,
		language: {
		"emptyTable": "No data available"
	},
		columns: [{
		className: '',
		"data": "name",
		"title": "Name",
	},{
		className: 'width-option-1 text-center',
		"data": "group_id",
		"orderable": false,
		"title": "Options",
			"render": function(data, type, row, meta){
				var param_data = JSON.stringify(row);
				newdata = '';
				newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" data-id=\' '+row.group_id+'\' data-catid=\' '+row.group_category_id+'\' onclick="delete_g_member(this)" type="button"><i class="fa fa-times"></i> Remove</button>';
				return newdata;
			}
		}
	]
	});
	}

</script>
<script type="text/javascript">
	get_teacher_selections();
	function get_teacher_selections(){
		let url = main_path + '/teacher/list';
      $.ajax({
      type:"GET",
      url:url,
      data:{},
      dataType:'json',
      beforeSend:function(){
      },
      success:function(response){
        // console.log(response);
				let data = response.data;
				let output  = data.map(opt => {
					return '<option value="'+opt.id+'">'+opt.name+'</option>';
				});
				$("#members").html(output);
      },
      error: function(error){
        console.log(error);
      }
    });
  }

	$("#members").select2({
		placeholder: "Select member"
	});
</script>
<!-- Javascript Function-->
<script>
	var tbl_group;
	function show_group(){
		if (tbl_group) {
			tbl_group.destroy();
		}
		var url = main_path + '/group/list_group';
		tbl_group = $('#tbl_group').DataTable({
		pageLength: 10,
		responsive: true,
		ajax: url,
		deferRender: true,
		language: {
		"emptyTable": "No data available"
	},
		columns: [{
		className: '',
		"data": "group_name",
		"title": "Group name",
	},{
		className: 'width-option-1 text-center',
		"data": "group_category_id",
		"orderable": false,
		"title": "Options",
			"render": function(data, type, row, meta){
				var param_data = JSON.stringify(row);
				newdata = '';
				newdata += '<button class="btn btn-info btn-sm font-base mt-1" data-catid=\' '+row.group_category_id+'\' onclick="view_group(this)" type="button"><i class="fa fa-eye"></i> View</button>';
				newdata += ' <button class="btn btn-success btn-sm font-base mt-1" data-id=\' '+row.group_category_id+'\' onclick="edit_group(this)" type="button"><i class="fa fa-edit"></i> Edit</button>';
				newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="delete_group(this)" type="button"><i class="fa fa-trash"></i> Delete</button>';
				return newdata;
			}
		}
	]
	});
	}

	$("#group_form_add").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
					console.log(response);
				if(response.status == true){
					console.log(response)
					swal("Success", response.message, "success");
					showValidator(response.error,'group_form_add');
          $("#modal_add_group").modal('hide');
          show_group();
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'group_form_add');
				}
			},
			error:function(error){
				console.log(error);
			}
		});
	});

	function delete_group(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		var url =  main_path + '/group/delete_group/' + data.group_category_id;
			swal({
				title: "Are you sure?",
				text: "Do you want to delete this group?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				closeOnConfirm: false
			},
			function(){
				$.ajax({
				type:"GET",
				url:url,
				data:{},
				dataType:'json',
				beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					swal("Success", response.message, "success");
          show_group();
				}else{
					console.log(response);
				}
			},
			error: function(error){
				console.log(error);
			}
			});
		});
	}

	function edit_group(_this){
		$(".add_group_modal_title").text('Edit Group');
		var id = JSON.parse($(_this).attr('data-id'));
		let url = main_path + '/group/list_group/find/'+id;
		$.ajax({
	    type:"GET",
	    url:url,
	    data:{},
	    dataType:'json',
	    beforeSend:function(){
	    },
	    success:function(response){
	      // console.log(JSON.parse(response.members));
				$('#group_category_id').val(response.group_category.group_category_id);
				$('#group_name').val(response.group_category.group_name);
				$('#members').val(response.members_id);
				$('#members').trigger('change');
				$("#modal_add_group").modal({'backdrop' : 'static'});
	    },
	    error: function(error){
	      console.log(error);
	    }
	  });

	}

	function view_group(_this){
		var id = JSON.parse($(_this).attr('data-catid'));
		let url = main_path + '/group/list_group/find/'+id;
		$.ajax({
	    type:"GET",
	    url:url,
	    data:{},
	    dataType:'json',
	    beforeSend:function(){
	    },
	    success:function(response){
				// console.log(response.members);
				show_g_member(response.members);
				$("#modal_members_detail").modal('show');
				$(".member_title_modal").text(response.group_category.group_name);
	    },
	    error: function(error){
	      console.log(error);
	    }
	  });

	}



	function delete_g_member(_this){
		var data = JSON.parse($(_this).attr('data-id'));
		var url =  main_path + '/g_member/delete_g_member/' + data;
			swal({
				title: "Are you sure?",
				text: "Do you want to delete this member?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				closeOnConfirm: false
			},
			function(){
				$.ajax({
				type:"GET",
				url:url,
				data:{},
				dataType:'json',
				beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					swal("Success", response.message, "success");
					view_group(_this);
				}else{
					console.log(response);
				}
			},
			error: function(error){
				console.log(error);
			}
			});
		});
	}
</script>
