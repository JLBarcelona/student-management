<!DOCTYPE html>
<html>
	@include('Layout.header', ['type' => 'home', 'title' => 'login', 'icon' => asset('img/logophone.png') ])

<body class="font-base">
	@include('Layout.nav', ['type' => 'home'])
	<div class="container-fluid mobile-margin">
		<div class="row">
			<div class="col-sm-4  col-lg-4 col-md-3"></div>
			<div class="col-sm-4 col-lg-4 col-md-6 p-0">
				<div class="card">
					<div class="card-header font-base-xl">Login</div>
					<div class="card-body">
						<form class="needs-validation" novalidate="" action="{{ url('/check_user') }}" id="loginForm">
								<div class="form-row">
									<div class="form-group col-sm-12">
										<label class="font-base-md bold label">Username</label>
										<input type="text" id="username" name="username" placeholder="Username" class="form-control">
										<div class="invalid-feedback" id="err_username"></div>
									</div>
									<div class="form-group col-sm-12">
										<label class="font-base-md bold label">Password</label>
										<input type="password" id="password" name="password" placeholder="Password" class="form-control">
										<div class="invalid-feedback" id="err_password"></div>
									</div>
									<div class="form-group col-sm-12">
										<label class="font-base-md bold label">Captcha</label>
									</div>
									<div class="form-group col-sm-12">
										<div class="row">
											<div class="col-sm-10 g-cap col-md-12 col-lg-10">
													<p class="alert alert-danger alert-captcha hide" style="width: 90%;"></p>
													<div class="g-recaptcha float-left" data-sitekey="{{ env('CAPTCHA_KEY') }}"></div>
													<div class="invalid-feedback" id="err_g-recaptcha-response"></div
														>
											</div>
											<div class="col-sm-2 col-md-12 col-lg-2">
												<span class="form-group btn-login-mobile">
													<button type="submit" class="btn btn-primary btn-sm">Log in</button>
													<a href="https://bona.com.sg/" target="_blank" class="btn btn-dark btn-sm" style="margin: 8px 0;">Support</a>
												</span>
											</div>
										</div>
									</div>
									<div class="col-sm-12 p-0">
										<a href="{{ url('/forgot_password') }}">Forgot Passsword ?</a>
										<a href="{{ url('/home/register') }}" class="float-right">Sign up now!</a>
									</div>
								</div>
							</form>
					</div>
				</div>
				<div class="text-center mt-4">
					<div class="row">
						<div class="col-sm-12">
							<a href="https://bona.com.sg/" target="_blank" class="btn-sm bold font-base-lg text-dark">Bona ERP by Bona Technologies</a>
						</div>
						<div class="col-sm-6">
							<img src="{{ asset('img/sgd.jpg') }}" alt="" class="img-fluid">
						</div>
						<div class="col-sm-6">
							<img src="{{ asset('img/infocom.jpg') }}" alt="" class="img-fluid">
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-md-3 col-lg-4"></div>
		</div>
	</div>
</body>
	@include('Layout.footer', ['type' => 'home'])
</html>
<script type="text/javascript">

$("#loginForm").on('submit', function(e){
 var form = $("#loginForm").serialize();
 // alert(result);
 var url = $(this).attr('action');
 e.stopPropagation();
 e.preventDefault();

 // alert(form);
	 $.ajax({
			 type:"POST",
			 url:url,
			 data:form,
			 dataType:"json",
			 beforeSend:function(){
					loading();
			 },
			 success:function(response){
				 console.log(response);
				 if (response.status == true) {
					 if (response.user_type == 1) {
						 showValidator(response.error,'loginForm');
						 window.location = main_path + '/admin';
					 }else if (response.user_type == 2) {
						 showValidator(response.error,'loginForm');
						 window.location = main_path + '/student';
					 }
					 else if (response.user_type == 3) {
						 showValidator(response.error,'loginForm');
						 window.location = main_path + '/admin';
					 }
				 }else{
					 check_captcha(response.error['g-recaptcha-response']);
					 showValidator(response.error,'loginForm');
				 }

			 },error:function(error){
				 console.log(error);
			 }
		 });

 });
</script>

<script>
$(".g-recaptcha-response").on('input', function(){
	$('.alert-captcha').hide('fast');
	return true;
});


function check_captcha(check_res){
	if (typeof check_res == 'undefined') {
		$('.alert-captcha').hide('fast');
		return true;
	}else{
		if(check_res.length > 0){
			$('.alert-captcha').show('fast');
			$('.alert-captcha').text('Please input captcha field');
			setTimeout(function(){
				$('.alert-captcha').hide('slow');
			},3000);
			return false;
		}

	}
}
</script>
