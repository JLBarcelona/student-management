<!DOCTYPE html>
<html>
	@include('Layout.header', ['type' => 'home', 'title' => 'Verify Account', 'icon' => asset('img/logophone.png') ])

<body class="font-base">
	@include('Layout.nav', ['type' => 'home'])
	<div class="container-fluid mobile-margin">
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header"><i class="fa fa-check text-success"></i> Account Verified!</div>
					<div class="card-body">
						<p>Hi {{ $verify->name }},</p>
						<p>Your account is verified!</p>
						<a href="{{ url('/') }}" class="btn btn-dark">Login</a>
						<p>Thanks,</p>
						<p>Student Management</p>
					</div>
					<div class="card-footer"></div>
				</div>
			</div>
		</div>
	</div>
</body>
	@include('Layout.footer', ['type' => 'home'])
</html>