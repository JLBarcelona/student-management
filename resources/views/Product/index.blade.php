<!DOCTYPE html>
<html>
	@include('Layout.header', ['type' => 'admin', 'title' => 'Admin', 'icon' => asset('img/logophone.png') ])

<body class="font-base" onload="show_product();">
	@include('Layout.nav', ['type' => 'admin'])
	<div class="container-fluid mobile-margin">
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header">Product
						<button class="btn btn-primary btn-sm float-right" onclick="add_product();"><i class="fa fa-plus"></i> Add Product</button>
					</div>
					<div class="card-body">
						<table class="table table-bordered dt-responsive nowrap" id="table_product" style="width: 100%;"></table>
					</div>
					<div class="card-footer"></div>
				</div>
			</div>
		</div>
	</div>
</body>
	@include('Layout.footer', ['type' => 'admin'])
</html>

 <div class="modal fade" role="dialog" id="modal_add_product">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title">
        Add Product
        </div>
        <button class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <form class="needs-validation" id="form_add_product" action="{{ url('/product/add') }}" novalidate>
			<div class="form-row">
				<div class="form-group col-sm-12">
					<input type="hidden" id="product_id" name="product_id" placeholder="" class="form-control " required>
					<label>Form</label>
					<select id="form" name="form" class="form-control">
					</select>
					<div class="invalid-feedback" id="err_form"></div>

				</div>
				<div class="form-group col-sm-12">
					<label>Price </label>
					<input type="number" id="price" name="price" placeholder="Enter Price" class="form-control " required>
					<div class="invalid-feedback" id="err_price"></div>
				</div>

				<div class="col-sm-12 text-right">
					<button class="btn btn-dark btn-sm" type="submit">Save</button>
				</div>
			</div>
		</form>
      </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	var table_product;
	
	function show_product(){
			if (table_product) {
				table_product.destroy();
			}
	
			var url = main_path + '/inventory/list';
			table_product = $('#table_product').DataTable({
	        pageLength: 10,
	        responsive: true,
	        ajax: url,
	        deferRender: true,
			language: {
				 "emptyTable": "No data available"
			},
	        columns: [
	         {
	         	className:'',
	            "data": "form.name",
	            "title": "Name",
	          },{
	         	 className:'width-option-1',
	            "data": "price",
	            "title": "Price",
	          },{
	            className: 'width-option-1 text-center',
	            "data": "product_id",
	            "orderable": false,
	            "title": "Options",
	            "render": function(data, type, row, meta){
	              var param_data = JSON.stringify(row);
	              // console.log();
	              // var data_param = JSON.parse(param_data);
	              	newdata = '<button class="btn btn-success btn-sm font-base mt-1" data-info=\''+param_data.trim()+'\' onclick="edit_product(this)" type="button"><i class="fa fa-edit"></i> Edit</button>\
							   <button class="btn btn-danger btn-sm font-base mt-1" data-info=\''+param_data.trim()+'\' type="button" onclick="delete_product(this)"><i class="fa fa-trash"></i> Delete</button>';
	              
	              return newdata;
	            }
	          }
	        ]
	        });
		}
</script>

<script type="text/javascript">
	function add_product(){
		product_form_clear();
		$("#modal_add_product").modal('show');
		show_form_dropdown();
	}
</script>

<script>
	$("#form_add_product").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
				if(response.status == true){
					// console.log(response)
					swal("Success",response.message,"success");
					product_form_clear();
					$("#modal_add_product").modal('hide');

					show_product();
					showValidator(response.error,'form_add_product');
					show_form_dropdown();
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'form_add_product');
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	});

function product_form_clear(){
	$("#product_id").val('');
	$("#form").val('');
	$("#price").val('');

	$(".is_product_yes").hide('fast');
	$(".is_product_no").show('fast');
}

function edit_product(_this){
	var data = JSON.parse($(_this).attr('data-info'));
	
	$("#product_id").val(data.product_id);
	$("#form").val(data.formable_id);
	$("#price").val(data.price);
	$(".is_product_yes #is_product_"+data.formable_id).show('fast');
	$(".is_product_no").hide('fast');
	$("#modal_add_product").modal('show');
}

function delete_product(_this){
	var data = JSON.parse($(_this).attr('data-info'));
    var url =  main_path + '/product/delete/' + data.product_id;
	  swal({
	        title: "Are you sure?",
	        text: "Do you want to delete ?",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "Yes",
	        closeOnConfirm: false
	      },
	        function(){
	         $.ajax({
	          type:"GET",
	          url:url,
	          data:{},
	          dataType:'json',
	          beforeSend:function(){
	          },
	          success:function(response){
	            // console.log(response);
	           if (response.status == true) {
	              swal("Success", response.message, "success");
	              show_product();
	           }else{
	            console.log(response);
	           }
	          },
	          error: function(error){
	            console.log(error);
	          }
	        });
	      });
}

function show_form_dropdown(){
	var url = main_path + '/form/dropdown';
	$.ajax({
	    type:"GET",
	    url:url,
	    data:{},
	    dataType:'json',
	    beforeSend:function(){
	    },
	    success:function(response){
	      // console.log(response);
	     if (response.status == true) {
	        // swal("Success", response.message, "success");
	        $("#form").html(response.output);
	     }else{
	      console.log(response);
	     }
	    },
	    error: function(error){
	      console.log(error);
	    }
	  });
}

</script>
