<!DOCTYPE html>
<html>
	@include('Layout.header', ['type' => 'student', 'title' => 'Review', 'icon' => asset('img/logophone.png') ])

<body class="font-base" onload="exam_form('{{ $purchased->purchased_id }}');">
	@include('Layout.nav', ['type' => 'student'])
	<div class="container-fluid mobile-margin">
		<div class="row">
			<div class="col-sm-12">
				<div class="card mb-3">
				<div class="card-header">{{ $purchased->product->form->name }} (review)</div>
				<div class="card-body">
					<div id="exam_form"></div>
				</div>
				<div class="card-footer">
				</div>
			</div>
			</div>
		</div>
	</div>
</body>
	@include('Layout.footer', ['type' => 'student'])
</html>

<script type="text/javascript">
		function exam_form(purchased_id){
		var url = main_path + '/get/exam/review/'+ purchased_id;
		$.ajax({
		    type:"GET",
		    url:url,
		    data:{},
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		      // console.log(response);
		     if (response.status == true) {
		     	$("#exam_form").html(response.data);
		        // swal("Success", response.message, "success");
		     }else{
		      console.log(response);
		     }
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });
	}

</script>
