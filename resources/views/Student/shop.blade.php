<!DOCTYPE html>
<html>
@include('Layout.header', ['type' => 'student', 'title' => 'Shop', 'icon' => asset('img/logophone.png') ])

<body class="font-base" onload="show_product(); show_bundle();">
	@include('Layout.nav', ['type' => 'student'])
	<div class="container-fluid mobile-margin">
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header">Shop</div>
					<div class="card-body">
						<ul class="nav nav-tabs" id="myTab" role="tablist">
						  <li class="nav-item">
						    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#product" onclick="show_product();" role="tab" aria-controls="home" aria-selected="true">Product</a>
						  </li>
						  <li class="nav-item">
						    <a class="nav-link" id="profile-tab" onclick="show_bundle();" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Bundle Product</a>
						  </li>
						</ul>
						<div class="tab-content" id="myTabContent">
						  <div class="tab-pane fade show active p-1 mt-2" id="product" role="tabpanel" aria-labelledby="home-tab">
						  		<table class="table table-bordered dt-responsive nowrap" id="tbl_product" style="width: 100%;"></table>
						  </div>
						  <div class="tab-pane fade p-1 mt-2" id="profile" role="tabpanel" aria-labelledby="profile-tab">
							  	<table class="table table-bordered" id="tbl_bundle" style="width: 100%;"></table>
						  </div>
						</div>
					</div>
					<div class="card-footer"></div>
				</div>
			</div>
		</div>
	</div>
</body>


	@include('Layout.footer', ['type' => 'student'])

<form action="" id="form_buy_bundle" data-purchased="">
	<div class="modal fade" role="dialog" id="modal_data_bundle">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <div class="modal-title">
	        	<span class="name_bundle"></span> Details
	        </div>
	        <button class="close" data-dismiss="modal">&times;</button>
	      </div>
	      <div class="modal-body">
	       <input type="hidden" id="bundle_id" name="bundle_id" placeholder="" class="form-control" required>
			<div class="row">
				<div class="col-sm-12">
					<label class="bold font-base-lg">Bundle Name: <span class="name_bundle"></span></label>
				</div>
				<div class="col-sm-12">
					<label class="bold font-base-lg">Bundle Price: {{ env('APP_CURRENCY') }} <span class="price_bundle"></span> </label>
				</div>
				<div class="col-sm-12">
					<hr>
					<table class="table table-bordered">
			      		<thead>
			      			<tr>
			      				<th>Included Forms</th>
			      			</tr>
			      		</thead>

			      		<tbody id="content_bundle"></tbody>
			      	</table>
				</div>
			</div>
	      </div>
	      <div class="modal-footer">
	        	<button class="btn btn-dark btn-sm">Buy ($ <span class="price_bundle"></span>)</button>
	      </div>
	    </div>
	  </div>
	</div>
</form>


</html>

<script type="text/javascript">
	var tbl_product;
	function show_product(){
		if (tbl_product) {
			tbl_product.destroy();
		}
		var url = main_path + '/product/list';


		tbl_product = $('#tbl_product').DataTable({
		pageLength: 10,
		responsive: true,
		ajax: url,
		deferRender: true,
		language: {
		"emptyTable": "No data available"
	},
		columns: [{
		className: '',
		"data": "form.name",
		"title": "Name",
	},{
		className: 'width-option-1 text-center',
		"data": "product_id",
		"orderable": false,
		"title": "Options",
			"render": function(data, type, row, meta){
				var param_data = JSON.stringify(row);
				newdata = '';
				newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-info=\''+param_data.trim()+'\' onclick="make_buy(this)" type="button"><i class="fa fa-dollar-sign"></i> '+row.price+'</button>';
				return newdata;
				}
		}
	]
	});
	}

</script>

<script type="text/javascript">

	function make_buy(_this){
	  var data = JSON.parse($(_this).attr('data-info'));
	  // console.log(data);
	  var url = main_path + '/student/buy/form';
	  swal({
	        title: "Are you sure?",
	        text: "Do you want to buy this product ?",
	        type: "info",
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "Yes",
	        closeOnConfirm: false,
	        showLoaderOnConfirm: true
	      },
	        function(){
	         $.ajax({
	          type:"POST",
	          url:url,
	          data:{product_id : data.product_id},
	          dataType:'json',
	          beforeSend:function(){
	          },
	          success:function(response){
	            console.log(response);
	           if (response.status == true) {
	              swal("Success", response.message, "success");
	           }else{
	            console.log(response);
	           }
	          },
	          error: function(error){
	            console.log(error);
	          }
	        });
	      });
	  }



	var tbl_bundle;
	function show_bundle(){
		if (tbl_bundle) {
			tbl_bundle.destroy();
		}
		var url = main_path + '/product/bundle/list';
		tbl_bundle = $('#tbl_bundle').DataTable({
		pageLength: 10,
		responsive: true,
		ajax: url,
		deferRender: true,
		language: {
		"emptyTable": "No data available"
	},
		columns: [{
		className: '',
		"data": "bundle_name",
		"title": "Bundle_name",
	},{
		className: 'width-option-1 text-center',
		"data": "bundle_id",
		"orderable": false,
		"title": "Options",
			"render": function(data, type, row, meta){
				var param_data = JSON.stringify(row);
				newdata = '';
				newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-info=\''+param_data.trim()+'\' onclick="purchased_bundle(this)" type="button"><i class="fa fa-dollar-sign"></i> '+row.bundle_price+'</button>';
				return newdata;
			}
		}
	]
	});
	}


	function purchased_bundle(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		// console.log(data.items);
		var output = '';

		var url =	 main_path + '/buy/bundle';

		$("#form_buy_bundle").attr('action', url_path(url));

		$(".name_bundle").text(data.bundle_name);
		$(".price_bundle").text(data.bundle_price);
		$("#form_buy_bundle").attr("data-purchased", data.bundle_id);

		var each = data.items;

	  	each.forEach(function(item) {
	  		output += '<tr>';
	  		output += '<td>';
	  		output += item.products.form.name+'<span class="float-right">$'+item.products.price+'</span>';
	  		output += '</td>';
	  		output += '</tr>';

		    // console.log(entry.items.mainform);
		});

	  	$("#content_bundle").html(output);
		$("#modal_data_bundle").modal('show');
	}


	$("#form_buy_bundle").on('submit', function(e){
		e.preventDefault();
		  var bundle_id = $("#form_buy_bundle").attr("data-purchased");
		  var data = 'bundle_id=' + bundle_id;

	 	  var url = $(this).attr('action');
		  swal({
	        title: "Are you sure?",
	        text: "Do you want to buy this product ?",
	        type: "info",
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "Yes",
	        closeOnConfirm: false,
	        showLoaderOnConfirm: true
	      },
	        function(){
	         $.ajax({
	          type:"POST",
	          url:url,
	          data:data,
	          dataType:'json',
	          beforeSend:function(){
	          },
	          success:function(response){
	        	// console.log(response);
	        	if (response.status == true) {
	        		swal("Success",response.message,"success");
	        	}
	          },
	          error: function(error){
	            console.log(error);
	          }
	        });
	      });
	});

</script>
