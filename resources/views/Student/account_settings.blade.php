<!DOCTYPE html>
<html>
	@include('Layout.header', ['type' => 'student', 'title' => 'student', 'icon' => asset('img/logophone.png') ])

<body class="font-base">
	@include('Layout.nav', ['type' => 'student'])
	<div class="container-fluid mobile-margin">
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header">Account Settings</div>
					<div class="card-body">
						<div class="row">
							<div class="col-sm-6 mb-2">
								<div class="card">
									<div class="card-header">Profile</div>
									<div class="card-body">
										<ul class="list-group">
											<li class="list-group-item "><span><b>Name</b></span> <span class="float-right text-tiny">{{ Auth::user()->name }}</span></li>
											<li class="list-group-item"><span><b>Username</b></span> <span class="float-right text-tiny">{{ Auth::user()->username }}</span></li>
											<li class="list-group-item"><span><b>Date Registered</b></span> <span class="float-right text-tiny">{{ Auth::user()->created_at }}</span></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-sm-6 mb-2">
								<div class="card">
									<div class="card-header">Account Settings</div>
									<div class="card-body">
										<form class="needs-validation" id="account_change_form" action="{{ url('student/account/add') }}" novalidate>
										<div class="form-row">
											<input type="hidden" id="id" name="id" value="{{ Auth::user()->id }}" placeholder="" class="form-control" required>
											<div class="form-group col-sm-12">
												<label>Name </label>
												<input type="text" id="name" name="name" value="{{ Auth::user()->name }}" placeholder="Name" class="form-control " required>
												<div class="invalid-feedback" id="err_name"></div>
											</div>
											<div class="form-group col-sm-12">
												<label>Username </label>
												<input type="text" id="username" name="username" value="{{ Auth::user()->username }}" placeholder="Username" class="form-control " required>
												<div class="invalid-feedback" id="err_username"></div>
											</div>
											<div class="form-group col-sm-12">
												<label>Email </label>
												<input type="email" id="email" name="email" value="{{ Auth::user()->email }}" placeholder="Email" class="form-control " required>
												<div class="invalid-feedback" id="err_email"></div>
											</div>

											<div class="col-sm-12 text-right">
												<button class="btn btn-dark btn-sm" type="submit">Save</button>
												<button class="btn btn-light btn-sm border-dark" onclick="change_password();" type="button">Change Password</button>
											</div>
										</div>
									</form>

									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card-footer"></div>
				</div>
			</div>
		</div>
	</div>
</body>
	@include('Layout.footer', ['type' => 'student'])
</html>

 <div class="modal fade" role="dialog" id="modal_change_password">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title">
            Change Password
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <form class="needs-validation" id="account_change_password" action="{{ url('student/password/change_password') }}" novalidate>
				<div class="form-row">
					<div class="form-group col-sm-12">
						<label>Old Password </label>
						<input type="password" id="old_password" name="old_password" placeholder="Old Password" class="form-control " required>
						<div class="invalid-feedback" id="err_old_password"></div>
					</div>
					<div class="form-group col-sm-12">
						<label>New Password </label>
						<input type="password" id="new_password" name="new_password" placeholder="New Password" class="form-control " required>
						<div class="invalid-feedback" id="err_new_password"></div>
					</div>
					<div class="form-group col-sm-12">
						<label>Confirm Password </label>
						<input type="password" id="confirm_password" name="confirm_password" placeholder="Confirm Password" class="form-control " required>
						<div class="invalid-feedback" id="err_confirm_password"></div>
					</div>

					<div class="col-sm-12 text-right">
						<button class="btn btn-dark btn-sm" type="submit">Save Changes</button>
					</div>
				</div>
			</form>

          </div>
          <div class="modal-footer">
            
          </div>
        </div>
      </div>
    </div>


<script type="text/javascript">
	function change_password(){
		clear_password_form();
		$("#modal_change_password").modal('show');
	}

	$("#account_change_form").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
				if(response.status == true){
					// console.log(response)
					swal("Success",response.message,"success");
					showValidator(response.error,'account_change_form');
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'account_change_form');
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	});
</script>

<script type="text/javascript">
$("#account_change_password").on('submit', function(e){
	var url = $(this).attr('action');
	var mydata = $(this).serialize();
	e.stopPropagation();
	e.preventDefault(e);

	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){
				//<!-- your before success function -->
		},
		success:function(response){
			if(response.status == true){
				console.log(response)
				swal("Success",response.message,"success");
				showValidator(response.error,'account_change_password');
				$("#modal_change_password").modal('hide');
				clear_password_form();
			}else{
				//<!-- your error message or action here! -->
				showValidator(response.error,'account_change_password');
			}
		},
		error:function(error){
			console.log(error)
		}
	});
});

function clear_password_form(){
	$("#old_password").val('');
	$("#new_password").val('');
	$("#confirm_password").val('');
}
</script>