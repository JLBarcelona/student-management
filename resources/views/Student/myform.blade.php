<!DOCTYPE html>
<html>
	@include('Layout.header', ['type' => 'student', 'title' => 'My Form', 'icon' => asset('img/logophone.png') ])

<body class="font-base" onload="show_purchased();">
	@include('Layout.nav', ['type' => 'student'])
	<div class="container-fluid mobile-margin">
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header">My Form</div>
					<div class="card-body">
						<table class="table table-bordered dt-responsive nowrap" id="tbl_purchased" style="width: 100%;"></table>
					</div>
					<div class="card-footer"></div>
				</div>
			</div>
		</div>
	</div>
</body>
	@include('Layout.footer', ['type' => 'student'])
</html>

<script>
	var tbl_purchased;
	function show_purchased(){
		if (tbl_purchased) {
			tbl_purchased.destroy();
		}
		var url = main_path + '/purchased/list';
		tbl_purchased = $('#tbl_purchased').DataTable({
		pageLength: 10,
		responsive: true,
		ajax: url,
		deferRender: true,
		language: {
		"emptyTable": "No data available"
		},
		columns: [{
			className: '',
			"data": "products.form.name",
			"title": "Form",
		},{
			className: 'width-option-1',
			"data": "timer",
			"title": "Timer",
		},{
			className: 'width-option-1',
			"data": "total_score",
			"title": "Total Question",
		},{
			className: 'width-option-1',
			"data": "products.passing_mark",
			"title": "Passing Mark",
		},{
			className: 'width-option-2',
			"data": "created_at",
			"title": "Date Purchased",
		},{
			className: 'width-option-1 text-center',
			"data": "purchased_id",
			"orderable": false,
			"title": "Options",
				"render": function(data, type, row, meta){
					var param_data = JSON.stringify(row);
					newdata = '';
					// console.log(row.finish_at);

					if (row.finish_at === null || row.finish_at === '') {
						newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-info=\''+param_data.trim()+'\' onclick="start_exam(this)" type="button"><i class="fa fa-play"></i> Start Exam</button>';
					}else{
						newdata += '<button class="btn btn-info btn-sm font-base mt-1" data-info=\''+param_data.trim()+'\' onclick="review(this)" type="button"><i class="fas fa-search"></i> Review</button>';
					}

					return newdata;
				}
			}
		]
		});
	}
</script>


<script type="text/javascript">
	function start_exam(_this){
	  var data = JSON.parse($(_this).attr('data-info'));
	  var url = main_path + '/student/exam/' + data.purchased_id;
	  swal({
	        title: "Are you sure?",
	        text: "Do you want to start the exam?",
	        type: "info",
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "Yes",
	        closeOnConfirm: false
	      },function(){
	      	window.location = url;
	      });
	  }

	  function review(_this){
	  	 var data = JSON.parse($(_this).attr('data-info'));
		 var url = main_path + '/student/exam/' + data.purchased_id;
		 window.location = url;
	  }
</script>
