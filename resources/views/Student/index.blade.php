<!DOCTYPE html>
<html>
	@include('Layout.header', ['type' => 'student', 'title' => 'Student', 'icon' => asset('img/logophone.png') ])
	
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/modules/data.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
	<script src="https://code.highcharts.com/modules/export-data.js"></script>
	<script src="https://code.highcharts.com/modules/accessibility.js"></script>

	<body class="font-base" onload="get_graph('{{ Auth::user()->user_id }}'); show_user_table_detail('{{ Auth::user()->user_id }}')">
		@include('Layout.nav', ['type' => 'student'])
		<div class="container-fluid mobile-margin">
			<div class="row">
				<div class="col-sm-12">
					<div class="card">
						<div class="card-header">My Progress</div>
						<div class="card-body">
							<div class="container" id="container"></div>
						</div>
						<div class="card-footer"></div>
					</div>


					<div class="card mt-2">
						<div class="card-header">Report Details</div>
						<div class="card-body">
							<table class="table table-bordered" id="user_table_detail" style="width: 100%;"></table>
						</div>
						<div class="card-footer"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
	@include('Layout.footer', ['type' => 'student'])
</html>

<script type="text/javascript">

	function get_graph(id){

		var url = main_path + '/student/progress/' + id;

		$.ajax({
		    type:"GET",
		    url:url,
		    data:{},
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		      // console.log(response);
		     if (response.status == true) {
		        // swal("Success", response.message, "success");
		        show_charts(response.data);
		     }else{
		      console.log(response);
		     }
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });
	}

	function show_charts(datas){
	// Create the chart
	var data_info = JSON.parse(datas);
	var marker = (data_info.length > 0) ? data_info[0].pass_marker : 0;

	// console.log(datas);
	Highcharts.chart('container', {
	  chart: {
	    type: 'column'
	  },
	  title: {
	    text: 'Course Statistics'
	  },
	  subtitle: {
	    text: ''
	  },
	  accessibility: {
	    announceNewData: {
	      enabled: true
	    }
	  },
	  xAxis: {
	    type: 'category'
	  },
	  yAxis: {
	    title: {
	      text: 'Total Percentage'
	    }

	  },
	  legend: {
	    enabled: false
	  },
	  plotOptions: {
	    series: {
	      borderWidth: 0,
	      dataLabels: {
	        enabled: true,
	        format: '{point.y:.1f}%'
	      }
	    }
	  },

	  tooltip: {
	    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
	  },
	  series: [
	    {
	      name: "",
	      colorByPoint: false,
	      data: JSON.parse(datas)
	    }
	  ]
	},function(chart){

        $.each(chart.series[0].data,function(i,data){
		  var max = data.pass_marker;

			console.log(max);
			console.log(data.y);
            if(data.y >= max){
				data.update({
						color:'#1cc88a'
					});
			}else{
				data.update({
						color:'#e74a3b'
					});
			}
        });

    });
}
</script>

<script type="text/javascript">
	var user_table_detail;
	
	function show_user_table_detail(id){
		if (user_table_detail) {
			user_table_detail.destroy();
		}
		var url = main_path + '/student/progress/table/' + id;
		user_table_detail = $('#user_table_detail').DataTable({
		pageLength: 10,
		responsive: true,
		ajax: url,
		deferRender: true,
		language: {
		"emptyTable": "No data available"
	},
		columns: [{
		className: '',
		"data": "name",
		"title": "Name",
	},{
		className: '',
		"data": "score",
		"title": "Score",
	},{
		className: '',
		"data": "y",
		"title": "Percentage",
	},{
		className: '',
		"data": "passing_mark",
		"title": "Passing mark",
	},{
		className: '',
		"data": "pass_marker",
		"title": "Percentage",
	},{
		className: '',
		"data": "total_points",
		"title": "Total Question",
	},{
		className: 'width-option-1 text-center',
		"data": "purchased_id",
		"orderable": false,
		"title": "Options",
			"render": function(data, type, row, meta){
				// var param_data = JSON.stringify(row);
				newdata = '';
				newdata += '<a target="_blank" href="'+url_path('/student/exam/'+row.purchased_id )+'" class="btn btn-info btn-sm font-base mt-1" ><i class="fa fa-search"></i> View</a>';
				return newdata;
			}
		}
	]
	});
	}
</script>