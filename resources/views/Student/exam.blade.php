<!DOCTYPE html>
<html>
<style type="text/css">
	iframe #FMathEd1_ads{
		display: none !important;
	}
	 .cke_button__fmatheditor_icon{
		background-image: url('{{ asset("img/MathEditor.png")  }}') !important;
	}
	textarea.cke_dialog_ui_input_textarea{
		outline: none !important;
	}
</style>
	@include('Layout.header', ['type' => 'student', 'title' => 'Exam', 'icon' => asset('img/logophone.png') ])
<style type="text/css">
.image-question-display{
	background-position: center;
	background-repeat: no-repeat;
	background-size: 100%;
	cursor: pointer;
	transition: transform 0.1s;
}
.image-question-display:hover{
 	transform: scale(0.9);
}
.timer{
	bottom: 10px;
	right: 50px;
	position: fixed;
	background: rgba(0,0,0,0.8);
	color: #fff;
	padding: 10px;
	border-radius: 10px;
}
</style>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML"></script>
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<body class="font-base" onload="exam_form('{{ $purchased->purchased_id }}'); timer_exam('{{ $purchased->purchased_id }}');">
	@include('Layout.nav', ['type' => 'student'])
	<div class="container-fluid mobile-margin">
		<div class="row">
			<div class="col-sm-12">
			<div class="card mb-3">
				<div class="card-header">{{ $purchased->product->form->name }}</div>
				<div class="card-body">
					<div id="exam_form"></div>
				</div>
				<div class="card-footer">
					<button class="btn btn-dark btn-sm" onclick="submit_exam('{{ $purchased->purchased_id }}')">Submit</button>
				</div>
			</div>

			 <div class="timer h3">
          <div id="counter">
              <div class="values"><img src="{{ asset('img/loading.gif') }}" class="img-fluid" width="20"></div>
          </div>
        </div>

		</div>
	</div>
</body>
	@include('Layout.footer', ['type' => 'student'])
	<script type="text/javascript" src="{{ asset('js/easytimer.min.js') }}"></script>
</html>

<script type="text/javascript">
	var timer = new easytimer.Timer();

  function update_time(time, purchased_id){
    var url = main_path + '/exam/timer/update/' + purchased_id;
    $.ajax({
      type:"POST",
      url:url,
      data:{time : time},
      dataType:'json',
      success:function(response){
        if (response.status == true) {
          if (time == '00:00:00') {
             submit_form('{{ $purchased->purchased_id }}');
          }else if (time == '00:00:20') {
            $(".timer").addClass('animated pulse infinite text-danger');
          }
        }else{
          console.log(response);
        }
      }
    });

  }


  function timer_exam(id){
    var url = main_path + '/exam/user/timer/' + id;
    $.ajax({
      type:"GET",
      url:url,
      data:{},
      cache:false,
      dataType:'json',
      success:function(response){
        data = response.time;
        // console.log(data);
        var hh = data[0]+data[1];
        var mm = data[3]+data[4];
        var ss = data[6]+data[7];

        // console.log(data);

        if (data == "00:00:00") {
         submit_form('{{ $purchased->purchased_id }}');
         // console.log(data);

          setInterval(function(){
              update_time(data.trim(), id);
          },1000);
        }else{
           setTimeout(function(){
            timer.start({countdown: true, startValues: {hours: Number(hh),minutes: Number(mm),seconds: Number(ss)}});
              $('#counter .values').html(timer.getTimeValues().toString());
              timer.addEventListener('secondsUpdated', function (e) {
                  $('#counter .values').html(timer.getTimeValues().toString());
                  var val = timer.getTimeValues().toString();
                  update_time(val, id);
              });


         },1000);
        }


      }
    });
}

// function timer_pause(){
//    timer.pause();
// }

// function timer_play(){
//       timer.start();
// }
</script>
<!-- Key down interval -->
<script type="text/javascript">
	// var timer = null;
	// $('#text').keydown(function(){
	//        clearTimeout(timer); timer = setTimeout(doStuff, 1000)
	// });

	function submitCustomAnswer(form,ck) {
		let Formdata =  $("#"+form).serialize();
		let custom_answer = ck.editor.getData();
		// console.log(Formdata+'&custom_answer='+custom_answer);
		let url = main_path + '/save/custom_answer';
			$.ajax({
		    type:"POST",
		    url:url,
		    data:Formdata+'&custom_answer='+custom_answer,
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		      // console.log(response);
		     // if (response.status == true) {
		     //    swal("Success", response.message, "success");
		     // }else{
		     //  console.log(response);
		     // }
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });
	}
</script>

<script type="text/javascript">
	function exam_form(purchased_id){
		var url = main_path + '/get/exam/form/'+ purchased_id;
		$.ajax({
		    type:"GET",
		    url:url,
		    data:{},
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		      // console.log(response);
		     if (response.status == true) {
		     	$("#exam_form").html(response.data);
		        // swal("Success", response.message, "success");
		     }else{
		      console.log(response);
		     }
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });
	}



	function make_answer(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		var purchased_id = data.purchased;
		var question_id = data.question;
		var user_id = data.user.user_id
		var choice_id = data.choice;

		var url = main_path + '/save/answer'

		$.ajax({
		    type:"POST",
		    url:url,
		    data:{purchased_id: purchased_id, question_id: question_id, user_id: user_id, choice_id: choice_id},
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		      // console.log(response);
		     if (response.status == true) {
		        // swal("Success", response.message, "success");
		     }else{
		      console.log(response);
		     }
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });
	}


	function submit_exam(id){
	  var url =  main_path + '/result/add';
	  swal({
	        title: "Are you sure?",
	        text: "Do you want to submit the form ?",
	        type: "info",
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "Yes",
	        closeOnConfirm: false
	      },
	        function(){
	         $.ajax({
	          type:"post",
	          url:url,
	          data:{purchased_id : id},
	          dataType:'json',
	          beforeSend:function(){
	          },
	          success:function(response){
	            // console.log(response);
	           if (response.status == true) {
	              swal("Success", response.message, "success");
	              location.reload();
	           }else{
	            console.log(response);
	           }
	          },
	          error: function(error){
	            console.log(error);
	          }
	        });
	      });
	  }

		function submit_form(id){
			var url =  main_path + '/result/add';

			$.ajax({
			type:"post",
			url:url,
			data:{purchased_id : id},
			dataType:'json',
			beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
			 if (response.status == true) {
					swal("Success", response.message, "success");
					location.reload();
			 }else{
				console.log(response);
			 }
			},
			error: function(error){
				console.log(error);
			}
		});
		}


</script>
