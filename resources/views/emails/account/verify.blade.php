@component('mail::message', ['data' => $data])
# Hi {{ $data['name'] }},

You are now registered in Student Management.
Click the button below to confirm your account.

@component('mail::button', ['url' => $data['url_link']])
 Confirm
@endcomponent


Thanks,<br>
Student Management
@endcomponent
