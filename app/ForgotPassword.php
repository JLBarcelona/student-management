<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class ForgotPassword extends Authenticatable
{

  protected $primaryKey = 'forgot_id';

   public $timestamps = false;

   protected $table = "sm_forgot_password";

   protected $fillable = [
        'forgotable_id','forgotable_type','token','date_validity','is_used'
    ];

    public function forgotable(){
      return $this->morphTo();
    }

}
