<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BundleItem extends Model
{
    public $timestamps = false;

	protected $primaryKey = 'bundle_item_id';

    protected $table = 'sm_bundle_detail';

    protected $fillable = [
       'product_id'
    ];

    protected $hidden = [
        'owner_type', 'owner_id',
    ];
    protected $appends = ['products'];

    public function owner()
    {
        return $this->morphTo();
    }

    public function getProductsAttribute(){
        return $this->product()->first();
    }

     public function product()
    {
       return $this->belongsTo('App\Product', 'product_id');
    }
}
