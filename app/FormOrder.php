<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormOrder extends Model
{
    public $timestamps = false;

    protected $primaryKey = 'form_order_id';


    protected $table = 'sm_form_order';


    protected $fillable = [
       'question_id', 'sub_category_id','product_id', 'mainform_id'
    ];

    protected $hidden = [
        'owner_type', 'owner_id',
    ];


    public function owner()
    {
        return $this->morphTo();
    }


     public function question()
    {
         return $this->belongsTo('App\Question', 'question_id');
    }

    //  public function product()
    // {
    //      return $this->belongsTo('App\Product', 'question_id');
    // }
}
