<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
     protected $primaryKey = 'category_id';


    protected $table = 'sm_category';


    protected $fillable = [
       'category_name','deleted_at'
    ];

    protected $appends = ['detail'];

    protected $hidden = [
        'owner_type', 'owner_id',
    ];

    public function owner()
    {
        return $this->morphTo();
    }

    public function category()
    {
        return $this->morphTo();
    }

      public function question()
    {
        return $this->morphMany('App\Question', 'owner');
    }

    public function getDetailAttribute(){
        return $this->question();
    }

     public function getCreatedAtAttribute($value)
    {
        // return ucfirst($value);
        // return  \Carbon\Carbon::parse($value)->format('m-d-y h:i:s A');
        return  \Carbon\Carbon::parse($value)->format('jS F Y');
    }
}
