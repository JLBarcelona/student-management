<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bundle extends Model
{
     protected $primaryKey = 'bundle_id';


    protected $table = 'sm_bundle';

    protected $fillable = [
       'bundle_name', 'bundle_price', 'deleted_at'
    ];

    protected $appends = ['items'];
    protected $hidden = [
        'owner_type', 'owner_id',
    ];


    public function owner()
    {
        return $this->morphTo();
    }

    public function getItemsAttribute(){
        return $this->bundle_item()->get();
    }

    public function bundle_item()
    {
       return $this->morphMany('App\BundleItem', 'owner');
    }
}
