<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
   protected $primaryKey = 'group_id';


    protected $table = 'sm_group';


    protected $fillable = [
        'group_category_id', 'id', 'created_at','updated_at','deleted_at'
    ];

    protected $hidden = [
        'groupable_type', 'groupable_id',
    ];


    public function groupable()
    {
        return $this->morphTo();
    }

    public function groupdetail(){
      return  $this->hasOne('App\GroupCategory', 'group_category_id', 'group_category_id');
    }

}
