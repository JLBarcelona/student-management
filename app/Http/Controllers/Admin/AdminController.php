<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use App\Admin;

use Auth;

class AdminController extends Controller
{
   function index(){
		$user = Auth::guard('admin')->user();

		$product = $user->product->count();
		$teacher = Admin::whereNull('date_deleted')->where('user_type', 3)->count();
		$student = User::whereNull('date_deleted')->whereNotNull('verified_at')->count();

    	return view('Admin.index', compact('product', 'teacher', 'student'));
	}
}
