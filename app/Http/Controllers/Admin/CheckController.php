<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Purchased;
use App\AnswerDetail;
use App\Choice;
use App\Result;

use Str;
use Auth;
use Validator;


class CheckController extends Controller
{
  function checking_form_list(){
    $data = array();
    $user = Auth::guard('admin')->user();
    $answer_detail = AnswerDetail::where('check_by', $user->id)->orderBy('check_at', 'asc');


    if ($answer_detail->count() > 0) {
      $purchase = $answer_detail->get();

      // $i = 0;
      foreach ($purchase as $row) {
        $student_ = $row->purchased()->whereNotNull('finish_at')->first();

        $student = $student_->owner;

        $form = $row->purchased->product->form;
        $form->answer_id = $row->answer_id;
        $form->student = $student->name;
        $form->question_id = $row->question_id;
        $form->answered_at = $row->created_at;
        $form->purchased_id = $row->purchased_id;
        $form->is_checked = $row->check_at;
        $form->score = $row->points;
        // $row->form = $form;
        $data[] = $form;

        // $i++;
      }
      return response()->json(['status' => true, 'data' => $data, 'counter' => $answer_detail->whereNull('check_at')->count()]);
    }

  }

  function add_score(Request $request){
    $answer_id = $request->get('answer_id');
    $score = $request->get('score');

    $validator = Validator::make($request->all(), [
  		'score' => 'required',
  	]);

  	if ($validator->fails()) {
  		return response()->json(['status' => false, 'error' => $validator->errors()]);
  	}else{
      $answer = AnswerDetail::where('answer_id', $answer_id);

      if ($answer->count() > 0) {
        $ans = $answer->first();
        $ans->points = $score;
        $ans->check_at = now();
        if ($ans->save()) {
          $this->save_purchase($ans->purchased_id);
          return response()->json(['status' => true, 'message' => 'Score added successfully!']);
        }
      }
    }
  }

  function save_purchase($purchased_id){
    $user =  Auth::user();
    $get_score = AnswerDetail::where('purchased_id', $purchased_id)->sum('points');
    $purchased = Purchased::find($purchased_id);
    // $my_score = $user->answerdetail->where('purchased_id', $purchased_id)->where('points', 1)->count();
		// $get_score = $my_score;
		$base_score = $purchased->product->form->order->count();
		$passing_mark = $purchased->passing_mark;

    // return response()->json(['status' => true, 'myscore' => $get_score, 'base_score' => $base_score, 'passing_mark' => $passing_mark]);
		$result = Result::where('purchased_id',$purchased_id)->first();
		$result->get_score = $get_score;
		$result->base_score = $base_score;
		$result->passing_mark = $passing_mark;
    if ($result->save()) {
      return true;
    }
  }

  function get_question_from_answer($id){
    $user = Auth::guard('admin')->user();
    $answer = [];
    $data = [];
    $status = false;
    $answer_detail = AnswerDetail::where('answer_id', $id)->where('check_by', $user->id);
    if ($answer_detail->count() > 0) {
      $ans = $answer_detail->first();
      $answer = $answer_detail->first();
      $qs = $ans->question->question;

      $answer->questionaire = $qs;
      $answer->score = $ans->points;
      $data = $answer;
      // $data->questionaire = $qs;

      $status = true;
    }

    return response()->json(['status' => $status, 'data' => $data ]);
  }

  // function checking_form_list(){
  //   $data = array();
  //   $user = Auth::guard('admin')->user();
  //   $answer_detail = AnswerDetail::where('check_by', $user->id);
  //
  //   if ($answer_detail->count() > 0) {
  //     $purchase = $answer_detail->groupBy('purchased_id')->get();
  //
  //     $i = 0;
  //     foreach ($purchase as $row) {
  //       $student = $row->purchased->owner;
  //       $form = $row->purchased->product->form;
  //
  //       $form->student = $student->name;
  //       $form->purchased_id = $row->purchased_id;
  //       // $row->form = $form;
  //       $data[] = $form;
  //
  //       $i++;
  //     }
  //     return response()->json(['status' => true, 'data' => $data, 'counter' => $i]);
  //   }
  //
  // }
}
