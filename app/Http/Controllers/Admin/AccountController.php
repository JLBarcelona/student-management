<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Admin;

use Validator;
use Auth;
use Str;
use Hash;

class AccountController extends Controller
{
    
	function list(){
		$account = Admin::whereNull('date_deleted')->get();
		return response()->json(['status' => true, 'data' => $account]);
	}

	  function is_unique($param,$tbl){
    	$result = ($param > 0) ? '|unique:'.$tbl : '';

    	return $result;
    }

	function add(Request $request){
		$id = $request->get('id');
		$name = $request->get('name');
		$username = $request->get('username');
		$email = $request->get('email');

		$username_check = Admin::where('username', $username)->whereNull('date_deleted')->where('id', '!=', $id)->count();
		$email_check = Admin::where('email', $username)->whereNull('date_deleted')->where('id', '!=', $id)->count();

		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'username' => 'required'.$this->is_unique($username_check, 'sm_admin'),
			'email' => 'required|email'.$this->is_unique($email_check, 'sm_admin'),
		]);

		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
			if (!empty($id)) {
				$account = Admin::find($id);
				$account->name = $name;
				$account->username = $username;
				$account->email = $email;
				if($account->save()){
					return response()->json(['status' => true, 'message' => 'Account updated successfully!']);
				}
			}else{
				$account = new Admin;
				$account->name = $name;
				$account->username = $username;
				$account->email = $email;
				if($account->save()){
					return response()->json(['status' => true, 'message' => 'Account saved successfully!']);
				}
			}
		}
	}


	function delete_account($id){
		$account = Admin::find($id);
		if($account->delete()){
			return response()->json(['status' => true, 'message' => 'Account deleted successfully!']);
		}
	}


	function change_password(Request $request){
	$user = Auth::guard('admin'); 
	$id = $user->user()->id;
	$user_pwd_enc = $user->user()->password;

	$old_password = $request->get('old_password');
	$new_password = $request->get('new_password');
	$confirm_password = $request->get('confirm_password');

	$validator = Validator::make($request->all(), [
		'old_password' => 'required',
		'new_password' => 'required|max:20',
		'confirm_password' => 'required|same:new_password|max:20',
	]);

	
	if (Hash::check($old_password, $user_pwd_enc)) {
	    $status = true;
	  }else if (!empty($old_password)) {
		  return response()->json(['status' => false, 'error' => $validator->errors()->add('old_password', 'Old password not match!')]);
	  }


	  if ($validator->fails()) {
	     return response()->json(['status' => false, 'error' => $validator->errors()]);
	  }else{
	    if ($status == true) {
	        $user = Admin::find($id);
	        $user->password = $confirm_password;
	        if ($user->save()) {
	          return response()->json(['status' => true, 'message' => 'Account password updated successfully!']);
	        }
	    }
	  }
}

}
