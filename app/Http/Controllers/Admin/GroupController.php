<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Group;

use Validator;
use Auth;
use Str;

class GroupController extends Controller
{

  function delete_g_member($group_id){
  	$g_member = Group::find($group_id);
  	if($g_member->delete()){
  		return response()->json(['status' => true, 'message' => 'G_member deleted successfully!']);
  	}
  }

}
