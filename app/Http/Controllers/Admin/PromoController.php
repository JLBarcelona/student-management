<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Promo;
use Validator;


class PromoController extends Controller
{
	function index(){
		return view('Admin.promo');
	}

    function list(){
		$promo = Promo::whereNull('date_deleted')->get();
		return response()->json(['status' => true, 'data' => $promo]);
	}


	 function is_unique($param,$tbl){
    	$result = ($param > 0) ? '|unique:'.$tbl : '';

    	return $result;
    }

	function add(Request $request){
		$promo_id= $request->get('promo_id');
		$promo_name = $request->get('promo_name');
		$promo_code = $request->get('promo_code');
		$promo_description = $request->get('promo_description');
		$price_min = $request->get('price_min');
		$price_discount = $request->get('price_discount');

		$check_code = Promo::where('promo_code', $promo_code)->where('promo_id', '!=', $promo_id)->count();

		$validator = Validator::make($request->all(), [
			'promo_name' => 'required',
			'promo_code' => 'required'.$this->is_unique($check_code, 'sm_promo'),
			'promo_description' => 'required',
			'price_min' => 'required',
			'price_discount' => 'required',
		]);

		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
			if (!empty($promo_id)) {
				$promo = Promo::find($promo_id);
				$promo->promo_name = $promo_name;
				$promo->promo_code = $promo_code;
				$promo->promo_description = $promo_description;
				$promo->price_min = $price_min;
				$promo->price_discount = $price_discount;
				if($promo->save()){
					return response()->json(['status' => true, 'message' => 'Promo updated successfully!']);
				}
			}else{
				$promo = new Promo;
				$promo->promo_name = $promo_name;
				$promo->promo_code = $promo_code;
				$promo->promo_description = $promo_description;
				$promo->price_min = $price_min;
				$promo->price_discount = $price_discount;
				if($promo->save()){
					return response()->json(['status' => true, 'message' => 'Promo saved successfully!']);
				}
			}
		}
	}


	function delete_promo($promo_id){
		$promo = Promo::find($promo_id);
		$promo->date_deleted = now();
		if($promo->save()){
			return response()->json(['status' => true, 'message' => 'Promo deleted successfully!']);
		}
	}
}
