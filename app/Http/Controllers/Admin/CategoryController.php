<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Category;

use App\Choice;
use App\Question;
use Validator;
use Auth;
use Str;

class CategoryController extends Controller
{

	function choice_order(){
		return 'abcdefghijklmnopqrstuvwxyz';
	}


	function list(){
		$user = Auth::guard('admin')->user();

		if ($user->user_type == 1) {
			$category = Category::whereNull('deleted_at')->get();
			return response()->json(['status' => true, 'data' => $category]);
		}else{
			$category = $user->category;
			return response()->json(['status' => true, 'data' => $category]);
		}
	}

	function find_question($id){
		$category = Question::where('question_id', $id)->whereNull('deleted_at')->first();
		return response()->json(['status' => true, 'data' => $category]);
	}


	function get_question(Request $request, $category_id){
		$output = '';
		$category = Category::find($category_id);
		$question = $category->question;

		$i = 0;
		foreach ($question as $row) {
			$i++;
			$output .='<div class="card rounded-0 mb-2 shadow">';
			$output .= '<div class="card-header bold font-base-lg"><span class="badge badge-primary rounded-circle h3">'.$i.'</span>';
			$output .= '<button class="btn btn-danger ml-2 btn-sm float-right" data-info=\''.$row.'\' onclick="delete_question(this)"><i class="fa fa-trash"></i> Delete</button>';
			$output .= '<button class="btn btn-success btn-sm float-right" onclick="edit_question('.$row->question_id.')"><i class="fa fa-edit"></i> Edit</button>';
			$output .= '</div>';
			$output .='	<div class="card-body questionaire font-base-lg h5">';
			$output .= $row->question;
			$output .='	</div>';
			$output .='	<div class="card-body"><hr>';
			$output .='	<div class="row">';
					$ii = 0;
					$col = ($row->choices->count() > 4)? '-2' : '-3';

					foreach ($row->choices as $data) {
						$output .='<div class="col-sm'.$col.' mb-1">';
							$output .= '<div class="row">';
							$output .= '<div class="col-sm-2">'.Str::ucfirst($this->choice_order()[$ii]).'. </div>';
							$output .= '<div class="col-sm-10 choices_display">'.$data->choice_answer.'</div>';
							$output .= '</div>';
						$output .='</div>';
						$ii++;
					}
			$output .='			</div>';
			$output .='	</div>';
			$output .='</div>';
		}



		return response()->json(['status' => true , 'output' => $output]);
	}


	function get_choices(Request $request, $question_id = ''){
		if (!empty($question_id)) {
			$question = Question::where('question_id', $question_id);
			if ($question->count() > 0) {
				$question_as = $question->first();
				$choices = $question_as->choices;
				return response()->json(['data' => $choices]);
			}

		}

		return response()->json(['data' => []]);

	}

	function delete_choices($choice_id){
		$choice = Choice::find($choice_id);

		if ($choice->delete()) {
			return response()->json(['status' => true, 'message' => 'Choices deleted successfully!']);
		}
	}

	function delete_question($question_id){
		$question = Question::find($question_id);
		if ($question->delete()) {
			return response()->json(['status' => true, 'message' => 'Question deleted successfully!']);
		}
	}


    function index(){
    	$user = Auth::guard('admin')->user();
		$category = $user->category;
    	return view('Admin.category', compact('category'));
    }


	function add(Request $request){
		$user = Auth::guard('admin')->user();
		$user_id = $user->id;

		$category_id = $request->get('category_id');
		$category_name = $request->get('category_name');


		$validator = Validator::make($request->all(), [
			'category_name' => 'required'
		]);

		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
			if (!empty($category_id)) {
				$form = Category::find($category_id);
				$form->category_name = $category_name;
				$form->owner_type = get_class($user);
				$form->owner_id = $user_id;

				if($form->save()){
					return response()->json(['status' => true, 'message' => 'Category saved successfully!', 'id' => $form->category_id ]);
				}
			}else{
				$form = new Category;
				$form->category_name = $category_name;
				$form->owner_type = get_class($user);
				$form->owner_id = $user_id;


				if($form->save()){
					return response()->json(['status' => true, 'message' => 'Category saved successfully!', 'id' => $form->category_id ]);
				}
			}


		}
	}

	function delete_category($category_id){
		$category = Category::find($category_id);
		$category->deleted_at = now();
		if ($category->save()) {
			return response()->json(['status' => true, 'message' => 'Category deleted successfully!']);
		}

	}

	function edit($form_id){
 		$category = Category::find($form_id);

 		return view('Admin.category_edit', compact('category'));
 	}


 	function question_add(Request $request){
 		$category_id = $request->get('category_id');
		$question_id = $request->get('question_id');
    $question = $request->get('question');
		$explanation = $request->get('explanation');

		$checking_option = $request->get('checking_option');


		$category = Category::find($category_id);

		$validator = Validator::make($request->all(), [
			'question' => 'required',
      'explanation' => 'required',
			'checking_option' =>  'required'
		]);

		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
			if (!empty($question_id)) {
				$form = Question::find($question_id);
				$form->question = $question;
        $form->explanation = $explanation;
				$form->owner_id = $category->category_id;
				$form->owner_type = get_class($category);
				$form->is_auto_check = $checking_option;

				if($form->save()){
					return response()->json(['status' => true, 'message' => 'Enter Message', 'id' => $form->question_id]);
				}
			}else{
				$form = new Question;
				$form->question = $question;
        $form->explanation = $explanation;
				$form->owner_id = $category->category_id;
				$form->owner_type = get_class($category);
				$form->is_auto_check = $checking_option;
				


				if($form->save()){
					return response()->json(['status' => true, 'message' => 'Enter Message', 'id' => $form->question_id]);
				}
			}
		}
	}

	function choices_add(Request $request){
		$question_id_cat = $request->get('question_id_cat');
		$question = Question::find($question_id_cat);
		$choice_id = $request->get('choice_id');
		$choice_answer = $request->get('choice_answer');
		$points = $request->get('points');

		$validator = Validator::make($request->all(), [
			'choice_answer' => 'required',
			'points' => 'required',
		]);

		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
			if (!empty($choice_id)) {
				$form = Choice::find($choice_id);
				$form->choice_answer = $choice_answer;
				$form->points = $points;
				$form->owner_id = $question->question_id;
				$form->owner_type = get_class($question);

				if($form->save()){
					return response()->json(['status' => true, 'message' => 'Enter Message']);
				}
			}else{
				$form = new Choice;
				$form->choice_answer = $choice_answer;
				$form->points = $points;
				$form->owner_id = $question->question_id;
				$form->owner_type = get_class($question);

				if($form->save()){
					return response()->json(['status' => true, 'message' => 'Enter Message']);
				}
			}

		}
	}




}
