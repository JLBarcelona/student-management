<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\GroupCategory;
use App\Group;
use App\Admin;
use App\MainForm;

use Validator;
use Auth;
use Str;

class GroupCategoryController extends Controller
{

  function list_group(){
  	$group = GroupCategory::whereNull('deleted_at')->orderBy('group_category_id', 'desc')->get();
  	return response()->json(['status' => true, 'data' => $group]);
  }

  function list_group_find($id){
    $group = [];
    $member = [];
    $member_id = [];
    $group_ = GroupCategory::where('group_category_id', $id)->whereNull('deleted_at');

    if ($group_->count() > 0) {
      $group = $group_->select('group_category_id', 'group_name')->first();
      $group_member = $group->mygroup;
      // $mem = $group_member->groupable()->select('id','name')->get();
      foreach ($group_member as $row) {
        $ndata = $row->groupable()->select('id','name')->first();
        $ndata->group_id = $row->group_id;
        $ndata->group_category_id = $group->group_category_id;
        $member[] = $ndata;
      }

      foreach ($member as $rows) {
        $member_id[] = $rows->id;
      }
    }

    return response()->json(['status' => true, 'group_category' => $group, 'members' => $member, 'members_id' => $member_id]);
  }


  function list_user_group_find(){
    $group = [];
    $user = Auth::guard('admin')->user();
    $groups = $user->UserGroup;

    foreach ($groups as $rr) {
      $group[] = $rr->groupdetail()->select('group_category_id','group_name')->first();
    }

    return response()->json(['status' => true, 'data' => $group]);
  }


  function list_group_member_find($id){
    $user = Auth::guard('admin')->user();
    $group = [];
    $member = [];
    $member_id = [];
    $worksheet = [];
    $group_ = GroupCategory::where('group_category_id', $id)->whereNull('deleted_at');

    if ($group_->count() > 0) {
      $group = $group_->select('group_category_id', 'group_name')->first();
      $group_member = $group->mygroup;
      // $mem = $group_member->groupable()->select('id','name')->get();
      foreach ($group_member as $row) {
        $ndatas = $row->groupable()->select('id','name')->where('id' , '!=', $user->id);
        if ($ndatas->count() > 0) {
          // code...
          $ndata = $ndatas->first();
          $ndata->group_id = $row->group_id;
          $ndata->group_category_id = $group->group_category_id;
          $member[] = $ndata;
        }
      }


      foreach ($member as $rows) {
        $member_id[] = $rows->id;
        $main = MainForm::where('owner_id', $rows->id)->get();

        foreach ($main as $mr) {
          $works = array();
          $works['form_id'] = $mr->mainform_id;
          $works['form_name'] = $mr->name;
          $works['form_timer'] = $mr->form_timer;
          $works['passing_mark'] = $mr->passing_mark;
          $works['name'] = $rows->name;
          $works['created_at'] = $mr->created_at;
          $worksheet[] = $works;
        }

      }
    }

    return response()->json(['status' => true, 'group_category' => $group, 'members' => $member, 'members_id' => $member_id, 'worksheet' => $worksheet]);
    // return response()->json(['status' => true,'worksheet' => $worksheet]);
  }

  // function list_group_member_find($id){
  //   $user = Auth::guard('admin')->user();
  //   $group = [];
  //   $member = [];
  //   $member_id = [];
  //   $group_ = GroupCategory::where('group_category_id', $id)->whereNull('deleted_at');
  //
  //   if ($group_->count() > 0) {
  //     $group = $group_->select('group_category_id', 'group_name')->first();
  //     $group_member = $group->mygroup;
  //     // $mem = $group_member->groupable()->select('id','name')->get();
  //     foreach ($group_member as $row) {
  //       $ndatas = $row->groupable()->select('id','name')->where('id' , '!=', $user->id);
  //       if ($ndatas->count() > 0) {
  //         // code...
  //         $ndata = $ndatas->first();
  //         $ndata->group_id = $row->group_id;
  //         $ndata->group_category_id = $group->group_category_id;
  //         $member[] = $ndata;
  //       }
  //     }
  //
  //     foreach ($member as $rows) {
  //       $member_id[] = $rows->id;
  //     }
  //   }
  //
  //   return response()->json(['status' => true, 'group_category' => $group, 'members' => $member, 'members_id' => $member_id]);
  // }

  function show_my_group($id){
    $group = GroupCategory::where('group_category_id', $id)->whereNull('deleted_at')->firstorFail();
    return view('Teacher.mygroup', compact('group'));

  }

  function add_group(Request $request){
  	$group_category_id= $request->get('group_category_id');
  	$group_name = $request->get('group_name');
    $members = $request->get('members');

  	$validator = Validator::make($request->all(), [
  		'group_name' => 'required',
      'members' => 'required'
  	]);

  	if ($validator->fails()) {
  		return response()->json(['status' => false, 'error' => $validator->errors()]);
  	}else{
  		if (!empty($group_category_id)) {
  			$group = GroupCategory::find($group_category_id);
  			$group->group_name = $group_name;
  			if($group->save()){
          $i = 0;
          $check_exist = Group::where('group_category_id', $group_category_id);
          if ($check_exist->delete()) {
            foreach ($members as $memId) {
              $teacher = Admin::where('id', $memId)->first();
              if ($this->save_to_group($group->group_category_id, $teacher)) {
                $i++;
              }
            }
          }

          return response()->json(['status' => true, 'message' => 'Group with '.$i.' member saved successfully!']);
  			}
  		}else{
  			$group = new GroupCategory;
  			$group->group_name = $group_name;
  			if($group->save()){
          $i = 0;
          $check_exist = Group::where('group_category_id', $group_category_id);
          if ($check_exist->delete()) {
            foreach ($members as $memId) {
              $teacher = Admin::where('id', $memId)->first();
              if ($this->save_to_group($group->group_category_id, $teacher)) {
                $i++;
              }
            }
          }

  				return response()->json(['status' => true, 'message' => 'Group with '.$i.' member saved successfully!']);
  			}
  		}
  	}
  }


  function save_to_group($group_id, $member){

    $check_exist = Group::where('group_category_id', $group_id)->where('groupable_id', $member->id);

    if ($check_exist->count() > 0) {
      return false;
    }else{
      $g_member = new Group;
      $g_member->group_category_id = $group_id;
      $g_member->groupable_id = $member->id;
      $g_member->groupable_type = get_class($member);
      if($g_member->save()){
        return true;
      }
    }
  }


  function delete_group($group_category_id){
  	$group = GroupCategory::find($group_category_id);
  	if($group->delete()){
  		return response()->json(['status' => true, 'message' => 'Group deleted successfully!']);
  	}
  }

}
