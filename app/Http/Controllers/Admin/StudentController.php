<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use App\Mail\AccountVerify;

use App\ForgotPassword;


use Carbon\Carbon;


use App\Admin;
use App\User;

use Str;
use Validator;


class StudentController extends Controller
{
    function index(){
    	return view('Admin.student');
    }

    function list(){
    	$user = User::whereNull('date_deleted')->get();
    	return response()->json(['status' => true, 'data' => $user]);
    }

   function if_url($param){
    	$result = (!empty($param))? 'url' : '';

    	return $result;
    }

    function send_mail($user){
    	  $token = Str::random(10);
		  $forgot = new ForgotPassword;
	      $forgot->forgotable_id = $user->user_id;
	      $forgot->forgotable_type = get_class($user);
	      $forgot->token = $token;
	      $forgot->date_validity = Carbon::now()->add(10, 'minutes');

	      if ($forgot->save()) {
	        $to_name = $user->name;
	        $to_email = $user->email;
	        $url_link = $url_link = url('/new_password/'.$token.'/'.$user->user_id.'');

	        $data = array('name' => $to_name, 'url_link' => $url_link);
	        Mail::to($to_email)->send(new AccountVerify($data));
	        return response()->json(['status' => true, 'message' => 'Please check your email. We will send your link.']);
	      }
    }

    function is_unique($param,$tbl){
    	$result = ($param > 0) ? '|unique:'.$tbl : '';

    	return $result;
    }

    function add(Request $request){
		$user_id = $request->get('user_id');
		$name = $request->get('name');
		$username = $request->get('username');
		$email = $request->get('email');

		$username_check = Admin::where('username', $username)->whereNull('date_deleted')->where('id', '!=', $user_id)->count();
		$email_check = Admin::where('email', $email)->whereNull('date_deleted')->where('id', '!=', $user_id)->count();

		$username_check_user = User::where('username', $username)->whereNull('date_deleted')->where('user_id', '!=', $user_id)->count();
		$email_check_user = User::where('email', $email)->whereNull('date_deleted')->where('user_id', '!=', $user_id)->count();

		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'username' => 'required'. $this->is_unique($username_check, 'sm_admin').$this->is_unique($username_check_user, 'sm_users'),
			'email' => 'required|email'. $this->is_unique($email_check, 'sm_admin').$this->is_unique($email_check_user, 'sm_users'),
		]);

		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
			if (!empty($user_id)) {
				$form = User::find($user_id);
				$form->name = $name;
				$form->username = $username;
				$form->email = $email;

				if($form->save()){
					return response()->json(['status' => true, 'message' => 'We will send your account verification to your email!']);
				}
			}else{
				$form = new User;
				$form->name = $name;
				$form->username = $username;
				$form->email = $email;
				$form->verification_code = Str::random(20);
				$form->password = Str::random(10);
				$form->verified_at = now();
				
				if($form->save()){
					$this->send_mail($form);
					return response()->json(['status' => true, 'message' => 'We will send your account verification to your email!']);
				}
			}
		}
	}

	function student_delete($user_id){
		$user = User::find($user_id);
		$user->date_deleted = now();
		$user->email = '';
		$user->username = '';

		if ($user->save()) {
			return response()->json(['status' => true, 'message' => 'Student deleted successfully!']);
		}

	}

}
