<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Bundle;
use App\BundleItem;

use Auth;
use Str;
use Validator;


class BundleController extends Controller
{


    function index(){
    	$user = Auth::guard('admin')->user();
    	$product = $user->product;

    	return view('Admin.bundle', compact('product'));
    }

    function list(){
		$user = Auth::guard('admin')->user();
		$bundle = $user->bundle;
		return response()->json(['status' => true, 'data' => $bundle]);
	}

	function add(Request $request){
		$user = Auth::guard('admin')->user();
		$bundle_id= $request->get('bundle_id');
		$bundle_name = $request->get('bundle_name');
		$bundle_price = $request->get('bundle_price');
		$product_item = $request->get('product_item');

		$validator = Validator::make($request->all(), [
			'bundle_name' => 'required',
			'bundle_price' => 'required',
		]);

		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
			if (!empty($bundle_id)) {
				$bundle = Bundle::find($bundle_id);
				$bundle->bundle_name = $bundle_name;
				$bundle->bundle_price = $bundle_price;


				$bundle_detail = BundleItem::where('owner_id', $bundle_id);
				$bundle_detail->delete();

				if($bundle->save()){
					// Refresh Bundle 
					foreach ($product_item as $product_ids) {
						$bundle_item = new BundleItem;
						$bundle_item->product_id = $product_ids;
						$bundle_item->owner_id = $bundle->bundle_id;
						$bundle_item->owner_type = get_class($bundle);

						$bundle_item->save();

					}

					return response()->json(['status' => true, 'message' => 'Bundle updated successfully!']);
				}
			}else{
				$bundle = new Bundle;
				$bundle->bundle_name = $bundle_name;
				$bundle->bundle_price = $bundle_price;
				$bundle->owner_id = $user->id;
				$bundle->owner_type = get_class($user);
				if($bundle->save()){
					foreach ($product_item as $product_ids) {
						$bundle_item = new BundleItem;
						$bundle_item->product_id = $product_ids;
						$bundle_item->owner_id = $bundle->bundle_id;
						$bundle_item->owner_type = get_class($bundle);

						$bundle_item->save();
					}

					return response()->json(['status' => true, 'message' => 'Bundle saved successfully!']);
				}
			}
		}
	}


	function delete_bundle($bundle_id){
		$bundle = Bundle::find($bundle_id);
		if($bundle->delete()){
			return response()->json(['status' => true, 'message' => 'Bundle deleted successfully!']);
		}
	}
}
