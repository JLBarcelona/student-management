<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\MainForm;
use App\FormDetail;
use App\Question;
use App\Category;

use App\FormOrder;

use Str;
use Auth;
use Validator;

class FormManualController extends Controller
{

	function choice_order(){
		return 'abcdefghijklmnopqrstuvwxyz';
	}

	function save_form_order(Request $request){
		$main_form_id = $request->get('main_form_id');
		$form_order_select = $request->get('form_order_select');
		$i = 0;

		$formorder_reset = FormOrder::where('mainform_id', $main_form_id);

		if ($formorder_reset->count() > 0) {
			$formorder_reset->delete();
		}

		foreach ($form_order_select as $question_id) {
			if ($this->form_order($main_form_id, $question_id)) {
				$i++;
			}
		}

		$message = $i.' Questions added to this form';
		return response()->json(['status' => true, 'message' => $message]);

	}


	function form_order($mainform_id, $question_id){

		$mainform = MainForm::find($mainform_id);
		$detail = $mainform->formDetail;
		$owner_id = $mainform->mainform_id;
		$owner_type = get_class($mainform);

		$form = new FormOrder;
		$form->question_id = $question_id;
		$form->mainform_id = $mainform_id;
		// $form->sub_category_id = $sub_category_id;
		$form->owner_id = $owner_id;
		$form->owner_type = $owner_type;

		if($form->save()){
			return true;
		}
	}


	function get_category_question($mainform_id){
		$output = '';
		$form = MainForm::find($mainform_id);
		$detail = $form->formDetail;


		foreach ($detail as $data) {
			$output .='<div class="card rounded-0 mb-2 shadow-sm">';
			$output .= '<div class="card-header bold font-base-lg">'.$data->category->category_name;
			$output .= '<button type="button" class="btn btn-info ml-2 btn-sm float-right" onclick="toggle_category(\'card_content_'.$data->category->category_id.'\', this)">Show</button>';
			$output .= '<button type="button" class="btn btn-danger  btn-sm float-right" data-info=\''.$data.'\' onclick="delete_category(this)">Delete</button>';
			$output .= '</div>';
				$output .= '<div class="card-body hide" id="card_content_'.$data->category->category_id.'">';
					$i = 0;
					foreach ($data->category->detail->limit($data->question_load)->get() as $row) {
						$i++;
						$output .='<div class="card rounded-0 mb-2">';
						$output .= '<div class="card-header bold font-base-lg">';
						$output .= '<input class="" type="checkbox" name="form_order_select[]" value="'.$row->question_id.'"> Choose';
						$output .= '</div>';
						$output .='	<div class="card-body questionaire font-base-lg h5">';
						$output .= $row->question;
						$output .='	</div>';
						$output .='	<div class="card-body"><hr>';
						$output .='			<div class="row">';
								$ii = 0;
								$col = ($row->choices->count() > 4)? '-2' : '-3';
								foreach ($row->choices as $datas) {
									$output .='<div class="col-sm'.$col.' product-item-image">';
										$output .= '<div class="row">';
										$output .= '<div class="col-sm-2">'.Str::ucfirst($this->choice_order()[$ii]).'. </div>';
										$output .= '<div class="col-sm-10 product-item-image image-fix-display choices_display">'.$datas->choice_answer.'</div>';
										$output .= '</div>';
									$output .='</div>';
									$ii++;
								}
						$output .='			</div>';
						$output .='	</div>';
						$output .='</div>';
					}
				$output .= '</div>';
			$output .='</div>';
		}

		return response()->json(['status' => true, 'output' => $output ]);
	}

	function form_detail_delete($form_detail_id){
		$form = FormDetail::find($form_detail_id);

		if ($form->delete()) {
			return response()->json(['status' => true, 'message' => 'Form Category deleted successfully!']);
		}
	}


	function list(){
		$user = Auth::guard('admin')->user();

		if ($user->user_type == 1) {
			$form = MainForm::whereNull('deleted_at')->get();
			return response()->json(['data' => $form]);
		}else{
			$form = $user->mainform;
			return response()->json(['data' => $form]);
		}
	}

	function index(){
		$user = Auth::guard('admin')->user();
		$form = $user->mainform;

		return view('Admin.form', compact('form'));
	}

 	function add(Request $request){
	 	$user = Auth::guard('admin')->user();
	 	$user_id = $user->id;

	 	$mainform_id = $request->get('mainform_id');
		$name = $request->get('name');
		$passing_mark = $request->get('passing_mark');
		$form_timer = $request->get('form_timer');
		$is_auto_choose = $request->get('choose_option');

		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'form_timer' => 'required',
			'passing_mark' => 'required',
			'choose_option' => 'required'
		]);

		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
			if (!empty($mainform_id)) {
				$form =	MainForm::find($mainform_id);
				$form->name = $name;
				$form->passing_mark = $passing_mark;
				$form->form_timer = $form_timer;
				$form->owner_type = get_class($user);
				$form->owner_id = $user_id;
				$form->is_auto_choose = $is_auto_choose;

				if($form->save()){
					return response()->json(['status' => true, 'message' => 'Enter Message']);
				}
			}else{
				$form = new MainForm;
				$form->name = $name;
				$form->passing_mark = $passing_mark;
				$form->form_timer = $form_timer;
				$form->owner_type = get_class($user);
				$form->owner_id = $user_id;
				$form->is_auto_choose = $is_auto_choose;

				if($form->save()){
					return response()->json(['status' => true, 'message' => 'Form saved successfully!', 'id' => $form->mainform_id]);
				}
			}

		}
 	}


 	function edit($form_id){
 		$user = Auth::guard('admin')->user();
 		$form = MainForm::find($form_id);
 		$category = $user->category;
 		return view('Admin.form_edit_manual', compact('form', 'category'));
 	}


 	function delete_forms($mainform_id){
 		$form = MainForm::find($mainform_id);
 		$form->deleted_at = now();

 		if ($form->save()) {
 			return response()->json(['status' => true, 'message' => 'Form deleted successfully!']);

 		}

 	}

 	function check_category($category_id, $form_detail_id, $mainform_id){
 		$form = FormDetail::where('owner_id', $mainform_id)->where('category_id', $category_id)->where('form_detail_id', '!=', $form_detail_id);
 		return $form;
 	}


 	function check_questions($category_id){
 		$category = Category::find($category_id);
 		$question = $category->question;


 		return $question;
 	}



 	function add_form_category(Request $request){
 		$mainform_id = $request->get('mainform_id');

		$form_detail_id = $request->get('form_detail_id');
		$category_id = $request->get('category');
		$question_load = $request->get('question_load');

		$mainform = MainForm::find($mainform_id);

		$validator = Validator::make($request->all(), [
			'category' => 'required',
			'question_load' => 'required',
		]);

		$category = Category::find($category_id);

		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{

			if ($this->check_category($category_id, $form_detail_id, $mainform_id)->count() > 0) {
				$validator->errors()->add('category', 'Category is already exist!');
				return response()->json(['status' => false, 'error' => $validator->errors()]);
			}else if ($question_load > $this->check_questions($category_id)->count()) {
				$validator->errors()->add('question_load', 'Category has maximmum of '. $this->check_questions($category_id)->count() . ' questions only!');
				return response()->json(['status' => false, 'error' => $validator->errors()]);
			}else{
				if (!empty($form_detail_id)) {
					$form = FormDetail::find($form_detail_id);

					$form->category_id = $category->category_id;
					$form->category_type = get_class($category);
					$form->question_load = $question_load;
					$form->owner_id = $mainform->mainform_id;
					$form->owner_type = get_class($mainform);

					if($form->save()){
						return response()->json(['status' => true, 'message' => 'Category Updated successfully!']);
					}
				}else{
					$form = new FormDetail;

					$form->category_id = $category->category_id;
					$form->category_type = get_class($category);
					$form->question_load = $question_load;
					$form->owner_id = $mainform->mainform_id;
					$form->owner_type = get_class($mainform);

					if($form->save()){
						return response()->json(['status' => true, 'message' => 'Category added successfully!']);
					}

				}
			}
		}
	}



	function dropdown(){

		$output = '<option selected="" value="" class="is_product_no">Select Form</option>';
		// $user = Auth::guard('admin')->user();
		$form = MainForm::where('deleted_at')->get();


		foreach ($form as $row) {
			if($row->is_product == 0){
				$output .= '<option value="'.$row->mainform_id .'" class="is_product_no">'.$row->name.'</option>';
			}else{
				$output .= '<option value="'.$row->mainform_id .'" disabled="" hidden="" class="is_product_yes" id="is_product_'.$row->mainform_id.'">'.$row->name.'</option>';
			}
		}

		return response()->json(['status' => true, 'output' => $output ]);

	}

}
