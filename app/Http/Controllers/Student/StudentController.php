<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;

class StudentController extends Controller
{

  function get_total_percent($total,$points){
  	$ave = 100 / $total;
    $total = $points * $ave;

    return $total;
   }


   function get_student_progress($user_id){
   		$user = User::find($user_id);
   		$purchased = $user->purchased->whereNotNull('finish_at');


      if ($purchased->count() > 0) {
         $i = 0;
          foreach ($purchased as $row) {
            $i++;
            // $output .=  $row->result;
            $course = $row->products->form->name.'-Purchased('.$i.')';

                // $total = (!empty($row->result->base_score))? $row->result->base_score : 0;
                // $passing_mark = (!empty($row->result->passing_mark)) ? $row->result->passing_mark : 0;
                //
                // $percentage = $this->get_total_percent($total,$row->result->get_score);
                // $percentage_marker = $this->get_total_percent($total,$passing_mark);
                // $myscore = $row->result->get_score;

                $myscore = $row->result->get_score;
                $original_passing_mark = (!empty($row->result->passing_mark)) ? $row->result->passing_mark : 0;

                $total = 0;

                $total_ = (!empty($row->result->base_score))? $row->result->base_score : 0;
                $my_ans_total = ($myscore > $total_)? $myscore : $total_;
                $total = ($my_ans_total > $original_passing_mark) ? $my_ans_total : $original_passing_mark;
                $passing_mark = (!empty($row->result->passing_mark)) ? $row->result->passing_mark : 0;

                $percentage = $this->get_total_percent($total,$row->result->get_score);
                $percentage_marker = $this->get_total_percent($total,$passing_mark);


                $data[] = array('name' => $course, 'y' => round($percentage, 1), 'total_points' => $total, 'score' => $myscore, 'passing_mark' => $passing_mark , 'pass_marker' => round($percentage_marker, 1));

          }

        $data_result = json_encode($data);

        return response()->json(['status' => true, 'data' => $data_result]);
      }else{
        return response()->json(['status' => true, 'data' => json_encode(array())]);
      }



        // $data_table = ($data);
   }


   function get_student_progress_table($user_id){
   		$user = User::find($user_id);
   		$purchased = $user->purchased->whereNotNull('finish_at');

      if ($purchased->count() > 0) {
        $i = 0;
        foreach ($purchased as $row) {
          $i++;
          // $output .=  $row->result;
          $course = $row->products->form->name.'-Purchased('.$i.')';
              $myscore = $row->result->get_score;
              $original_passing_mark = (!empty($row->result->passing_mark)) ? $row->result->passing_mark : 0;

              $total = 0;

              $total_ = (!empty($row->result->base_score))? $row->result->base_score : 0;
              $my_ans_total = ($myscore > $total_)? $myscore : $total_;
              $total = ($my_ans_total > $original_passing_mark) ? $my_ans_total : $original_passing_mark;



              $passing_mark = (!empty($row->result->passing_mark)) ? $row->result->passing_mark : 0;

              $percentage = $this->get_total_percent($total,$myscore);
              $percentage_marker = $this->get_total_percent($total,$original_passing_mark);


              $data[] = array('purchased_id' => $row->purchased_id , 'name' => $course, 'y' => round($percentage, 1).' %' , 'total_points' => $total_, 'score' => $myscore, 'passing_mark' => $original_passing_mark , 'pass_marker' => round($percentage_marker, 1).' %' );
        }

        return response()->json(['status' => true, 'data' => ($data)]);
      }else{
        return response()->json(['status' => true, 'data' => array()]);
      }



   }

}
