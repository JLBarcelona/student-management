<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;

use App\Product;

class ShopController extends Controller
{
   function index(){
   	return view('Student.index');
   }


   function list(){
	$product = Product::whereNull('deleted_at')->get();
	return response()->json(['status' => true, 'data' => $product]);
   }


   function shop(){
   	$product = Product::whereNull('deleted_at')->get();

   	return view('Student.shop', compact('product'));
   }
}
