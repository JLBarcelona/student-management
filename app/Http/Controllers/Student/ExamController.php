<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Purchased;
use App\AnswerDetail;
use App\Choice;
use App\Result;

use Str;
use Auth;


class ExamController extends Controller
{
	function update_time(Request $request, $purchased_id){
		$time = $request->get('time');
		$purchased = Purchased::find($purchased_id);
		$purchased->timer = $time;

		if ($purchased->save()) {
			return response()->json(['status' => true, 'message' => 'Timer Updated']);
		}


	}

	function get_time($purchased_id){
		$purchased = Purchased::find($purchased_id);
		return response()->json(['status' => true, 'time' => $purchased->timer ]);
	}


	public function exam($purchased_id){
		$user = Auth::user();
		$purchased = Purchased::where('purchased_id', $purchased_id)->firstOrFail();

		if ($user->user_id == $purchased->owner_id) {
			if (!empty($purchased->finish_at)) {
				return view('Student.review', compact('purchased'));
			}else{
				return view('Student.exam', compact('purchased'));
			}
		}else{
			abort(404);
		}
		// return response()->json(['status' => true, 'data' => $purchased ]);
	}

	function choice_order(){
		return 'abcdefghijklmnopqrstuvwxyz';
	}


	public function get_exam($purchased_id){
		$output = '';
		$dt = '';
		$user = Auth::user();
		$purchased = $user->purchased_item->where('purchased_id', $purchased_id)->firstOrFail();

		$form = $purchased->product->form->formdetail()->inRandomOrder()->get();
		// $form = $purchased->product->form->formdetail;

		$user_order = $purchased->userorder()->inRandomOrder()->get();

		foreach ($form as $data) {

			$output .='<div class="card rounded-0 mb-2 shadow">';
			$output .= '<div class="card-header bold font-base-lg">'.$data->category->category_name;
			$output .= '</div>';
				$output .= '<div class="card-body">';
					$i = 0;
					foreach ($user_order as $question) {
						$questionaire = $data->category->detail->where('question_id', $question->question_detail->question_id)->get();
						foreach ($questionaire as $row) {
							// $output .= $row->question_id;
							$i++;
							$output .='<div class="card rounded-0 mb-2">';
							$output .= '<div class="card-header bold font-base-lg"><span class="badge badge-dark rounded-circle h3">'.$i.'</span>';
							$output .= '</div>';
							$output .='	<div class="card-body questionaire font-base-lg h5">';
							$output .= $row->question;
							$output .='	</div>';
							$output .='	<div class="card-body"><hr>';
							if ($row->is_auto_check == 0) {
								$output .='<form id="form_custom_question_'.$row->question_id.'">';
								$output .= '<input type="hidden" name="purchased_id" value="'.$purchased_id.'">';
								$output .= '<input type="hidden" name="question_id" value="'.$row->question_id.'">';
								$output .= '<input type="hidden" name="user_id" value="'.$user->user_id.'">';
								$output .= '<input type="hidden" name="choice" value="">';
								$output .='<div class="row">';
								$output .='<div class="col-sm-12">';
									$output .='<label>Write Your Answer</label>';
										$output .='<textarea name="custom_answer_" class="question_'.$row->question_id.' form-control answer_key" id="question_'.$row->question_id.'"></textarea>';
										$output .=' <div class="invalid-feedback" id="err_question_'.$row->question_id.'"></div><br>';
										$output .='<script> let time_'.$row->question_id.' = null; let question_'.$row->question_id.' = CKEDITOR.replace("question_'.$row->question_id.'");';
										$output .=' CKEDITOR.instances.question_'.$row->question_id.'.on("change", function(evt) { clearTimeout(time_'.$row->question_id.'); time_'.$row->question_id.' = setTimeout(submitCustomAnswer(\'form_custom_question_'.$row->question_id.'\', evt), 1000) });</script>';

									$output .='	</div>';
									$output .='</div>';
								$output .='</form>';
							}else{
								$output .='<div class="row">';
										$ii = 0;
										$col = ($row->choices->count() > 4)? '-3 mt-1' : '-3';
										foreach ($row->choices as $datas) {

											$answer_detail = array('purchased' => $purchased_id, 'question' => $row->question_id, 'user' => $user, 'choice' => $datas->choice_id);

											$output .='<div class="col-sm'.$col.' product-item-image">';
												$output .= '<div class="row">';
												$output .= '<div class="col-sm-3"><input type="radio" data-info=\''.json_encode($answer_detail).'\' name="'.$row->question_id.'_answer" onclick="make_answer(this)" class="mr-1"><span class="bold">'.Str::ucfirst($this->choice_order()[$ii]).'</span>. </div>';
												$output .= '<div class="col-sm-9 p-0 product-item-image image-fix-display choices_display">'.$datas->choice_answer.'</div>';
												$output .= '</div>';
											$output .='</div>';
											$ii++;
										}
								$output .='			</div>';
							}


							$output .='	</div>';
							$output .='</div>';
						}
					}
				$output .= '</div>';
			$output .='</div>';
		}

		return response()->json(['status' => true, 'data' => $output ]);
	}


	function get_my_answer($data, $type){
		$output = '';

		$purchased_id = $data['purchased_id'];
		$question_id = $data['question_id'];

		$data = AnswerDetail::where('purchased_id', $purchased_id)->where('question_id', $question_id);

		$output .='<div class="row">';
		if ($data->count() > 0) {
			$row = $data->first();

			if ($type == 0) {
				if (!empty($row->points)) {
					$output .='<div class="col-sm-12 text-success"><span class="fa  fa-check"></span> Score: <b>'.$row->points.'</b></div>';
				}else{
					$output .='<div class="col-sm-12 text-info"><span class="fa  fa-info-circle"></span>The Teacher will check this question.</div>';
				}

				$output .='<div class="col-sm-12">';
				$output .= $row->custom_answer;
				$output .='</div>';
			}else{
				$answer = $row->choice_detail->choice_answer;

				if ($row->points > 0) {
						$output .='<div class="col-sm-4 row">';
						$output .='<div class="col-sm-12 text-success"><span class="fa  fa-check"></span> Correct</div>';
						$output .= '<div class="col-sm-12 product-item-image image-fix-display choices_display">'.$answer.'</div>';

						$output .='</div>';

				}else{
					$output .='<div class="col-sm-4 row">';
					$output .='<div class="col-sm-12 text-danger"><span class=" fa  fa-times"></span> Wrong</div>';
					$output .= '<div class="col-sm-12 product-item-image image-fix-display choices_display">'.$answer.'</div>';

					if (!empty($row->question_explain)) {
						$output .= '<div class="col-sm-12 product-item-image image-fix-display choices_display bold">Explanation:</div>';
						$output .= '<div class="col-sm-12 product-item-image image-fix-display choices_display">';
						$output .= $row->question_explain;
						$output .= '</div>';
						$output .='</div>';
					}

				}
			}

		}else{
			$output .= '<div class="col-sm-4 text-warning"><span class="fa fa-question-circle"></span> No answer found!</div>';
		}
		$output .='</div>';



		return $output;
	}


	function get_review($purchased_id){
		$output = '';
		$dt = '';
		$user = Auth::user();
		$purchased = $user->purchased_item->where('purchased_id', $purchased_id)->firstOrFail();

		$form = $purchased->product->form->formdetail;
		$user_order = $purchased->userorder;

		foreach ($form as $data) {

			$output .='<div class="card rounded-0 mb-2 shadow">';
			$output .= '<div class="card-header bold font-base-lg">'.$data->category->category_name;
			$output .= '</div>';
				$output .= '<div class="card-body">';
					$i = 0;
					foreach ($user_order as $question) {
						$questionaire = $data->category->detail->where('question_id', $question->question_detail->question_id)->get();
						foreach ($questionaire as $row) {
							// $output .= $row->question_id;
							$i++;
							$output .='<div class="card rounded-0 mb-2">';
							$output .= '<div class="card-header bold font-base-lg"><span class="badge badge-dark rounded-circle h3">'.$i.'</span>';
							$output .= '</div>';
							$output .='	<div class="card-body questionaire font-base-lg h5">';
							$output .= $row->question;
							$output .='	</div>';
							$output .='	<div class="card-body"><hr>';
							$details = array('purchased_id' => $purchased_id, 'question_id' => $row->question_id);
								$output .= $this->get_my_answer($details, $row->is_auto_check);
							$output .='	</div>';
							$output .='</div>';
						}
					}
				$output .= '</div>';
			$output .='</div>';
		}

		return response()->json(['status' => true, 'data' => $output ]);
	}


	function make_custom_answer(Request $request){
		$user = Auth::user();

		$purchased_id = $request->get('purchased_id');
		$question_id = $request->get('question_id');
		$custom_answer = $request->get('custom_answer');

		$user_id = $user->user_id;
		$user_type = get_class($user);

		$get_creator = Purchased::where('purchased_id', $purchased_id)->first();

		$mainform = $get_creator->product;

		$checking_by = $mainform->form->owner_id;

		$check = AnswerDetail::where('purchased_id', $purchased_id)->where('question_id', $question_id)->where('owner_id', $user_id);

		if ($check->count() > 0) {
			$ans = $check->first();

			$answer = AnswerDetail::find($ans->answer_id);
			$answer->custom_answer = $custom_answer;
			$answer->purchased_id = $purchased_id;
			$answer->question_id = $question_id;
			$answer->owner_id = $user_id;
			$answer->owner_type = $user_type;
			$answer->check_by = $checking_by;

			if ($answer->save()) {
				return response()->json(['status' => true, 'message' => 'Answer updated successfully!']);
			}
		}else{
			$answer = new AnswerDetail;

			$answer->custom_answer = $custom_answer;
			$answer->purchased_id = $purchased_id;
			$answer->question_id = $question_id;
			$answer->owner_id = $user_id;
			$answer->owner_type = $user_type;
			$answer->check_by = $checking_by;


			if ($answer->save()) {
				return response()->json(['status' => true, 'message' => 'Answer add successfully!']);
			}
		}
	}

	function make_answer(Request $request){
		$user = Auth::user();

		$purchased_id = $request->get('purchased_id');
		$question_id = $request->get('question_id');

		$user_id = $user->user_id;
		$user_type = get_class($user);


		$choice_id = $request->get('choice_id');

		$check = AnswerDetail::where('purchased_id', $purchased_id)->where('question_id', $question_id)->where('owner_id', $user_id);

		if ($check->count() > 0) {
			$ans = $check->first();

			$answer = AnswerDetail::find($ans->answer_id);

			$answer->choice_id = $choice_id;
			$choice = Choice::find($choice_id);
			$answer->points = $choice->points;
			$answer->purchased_id = $purchased_id;
			$answer->question_id = $question_id;
			$answer->owner_id = $user_id;
			$answer->owner_type = $user_type;

			if ($answer->save()) {
				return response()->json(['status' => true, 'message' => 'Answer updated successfully!']);
			}
		}else{
			$answer = new AnswerDetail;

			$answer->choice_id = $choice_id;

			$choice = Choice::find($choice_id);

			$answer->points = $choice->points;
			$answer->purchased_id = $purchased_id;
			$answer->question_id = $question_id;
			$answer->owner_id = $user_id;
			$answer->owner_type = $user_type;

			if ($answer->save()) {
				return response()->json(['status' => true, 'message' => 'Answer add successfully!']);
			}
		}
	}


	function submit_form(Request $request){
		$user =  Auth::user();

		$purchased_id = $request->get('purchased_id');
		$purchased = Purchased::find($purchased_id);

		$purchased->finish_at = now();
		$purchased->check_by = $purchased->product->form->owner_id;
		$purchased->save();

		$base_score = $purchased->product->form->order->count();
		$passing_mark = $purchased->passing_mark;

		$get_score = AnswerDetail::where('purchased_id', $purchased_id)->sum('points');

		$owner_id = $user->user_id;
		$owner_type = get_class($user);

		$result = new Result;
		$result->get_score = $get_score;
		$result->base_score = $base_score;
		$result->passing_mark = $passing_mark;
		$result->purchased_id = $purchased_id;
		$result->owner_id = $owner_id;
		$result->owner_type = $owner_type;
		if($result->save()){
			return response()->json(['status' => true, 'message' => 'Result saved successfully!']);
		}
	}

}
