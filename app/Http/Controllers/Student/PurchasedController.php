<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Purchased;
use App\Product;
use App\UserFormOrder;
use App\Bundle;

use Auth;


class PurchasedController extends Controller
{

	function index(){
		return view('Student.myform');
	}

	function list(){
		$user = Auth::user();
		$purchased =  $user->purchased;
		return response()->json(['status' => true, 'data' => $purchased]);
	}


	function bundle_list(){
		$bundle = Bundle::whereNull('deleted_at')->get();
		return response()->json(['status' => true, 'data' => $bundle]);
	}

    function add(Request $request){
    	$user = Auth::user();

    	$product_id = $request->get('product_id');

    	$product = Product::find($product_id);

			$productable_id = $product_id;
			$productable_type = get_class($product);
			$price = $product->price;

			$owner_id = $user->user_id;
			$owner_type = get_class($user);

			$purchased = new Purchased;
			$purchased->productable_id = $productable_id;
			$purchased->productable_type = $productable_type;

			$purchased->timer = $product->form->form_timer;
			$purchased->total_score = $product->form->order->count();
			$purchased->passing_mark = $product->passing_mark;
			$purchased->price = $price;
			$purchased->owner_id = $owner_id;
			$purchased->owner_type = $owner_type;

			if($purchased->save()){
				$this->user_form_order($purchased->purchased_id);
			}
				return response()->json(['status' => true, 'message' => 'Product Purchased Successfully!']);
	}


	function user_form_order($purchased_id){
		$i = 0;

		$purchased = Purchased::find($purchased_id);
		$owner_id = $purchased->purchased_id;
		$owner_type = get_class($purchased);

		$product = Product::find($purchased->productable_id);
		$form = $product->mainform;

		foreach ($form->formorder->inRandomOrder()->get() as $row) {
			$user_form = new UserFormOrder;
			$user_form->question_id = $row->question_id;
			$user_form->owner_id = $owner_id;
			$user_form->owner_type = $owner_type;

			if ($user_form->save()) {
				$i++;
			}
		}

		return $i;
		// return response()->json(['data' => $form->formorder->inRandomOrder()->get()]);

	}


	function buy_bundle(Request $request){
		$user = Auth::user();
		$bundle_id = $request->get('bundle_id');
		$bundle = Bundle::find($bundle_id);
		$i = 0;

		foreach ($bundle->items as $data) {
	    	$product_id = $data->product_id;
	    	$product = Product::find($product_id);

			$productable_id = $product_id;
			$productable_type = get_class($product);
			$price = $product->price;

			$owner_id = $user->user_id;
			$owner_type = get_class($user);

			$purchased = new Purchased;
			$purchased->productable_id = $productable_id;
			$purchased->productable_type = $productable_type;

			$purchased->timer = $product->form->form_timer;
			$purchased->total_score = $product->form->order->count();
			$purchased->passing_mark = $product->passing_mark;
			$purchased->price = $price;
			$purchased->owner_id = $owner_id;
			$purchased->owner_type = $owner_type;

			if($purchased->save()){
			   $this->user_form_order($purchased->purchased_id);
				$i++;
			}
		}

		if ($i > 0) {
			return response()->json(['status' => true, 'message' => 'Bundle ('.$i.' Forms) purchased Successfully!']);
		}
	}

}
