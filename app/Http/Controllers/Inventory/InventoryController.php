<?php

namespace App\Http\Controllers\Inventory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Product;

use App\MainForm;
use App\FormOrder;

use Auth;
use Validator;


class InventoryController extends Controller
{

	function index(){
		$user = Auth::guard('admin')->user();
  	$form = $user->mainform;

		return view('Product.index', compact('form'));
	}

    function list(){
    	$product = Product::whereNull('deleted_at')->get();
    	return response()->json(['status' => true, 'data' => $product]);
    }


    function add_product(Request $request){
    	$user = Auth::guard('admin')->user();

		$product_id = $request->get('product_id');
		$id = $request->get('form');

		if (!empty($id)) {
			$mainform = MainForm::find($id);
			$formable_id = $mainform->mainform_id;
			$formable_type = get_class($mainform);
			$passing_mark = $mainform->passing_mark;
		}


		$price = $request->get('price');

		$validator = Validator::make($request->all(), [
			'form' => 'required',
			'price' => 'required',
		]);

		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
			if (!empty($product_id)) {
				$form = Product::find($product_id);
				$form->price = $price;
				$form->passing_mark = $passing_mark;
				$form->formable_id = $formable_id;
				$form->formable_type = $formable_type;
				$form->owner_id = $user->id;
				$form->owner_type = get_class($user);

				if($form->save()){
					return response()->json(['status' => true, 'message' => 'Product updated successfully!']);
				}
			}else{
				$form = new Product;
				$form->passing_mark = $passing_mark;
				$form->price = $price;
				$form->formable_id = $formable_id;
				$form->formable_type = $formable_type;
				$form->owner_id = $user->id;
				$form->owner_type = get_class($user);

				if($form->save()){
					// Generate
					if ($form->is_auto_choose == 0) {
						// update selected
						$this->form_order_manual($formable_id, $form->product_id);
					}else{
						// Random Selection
						$this->form_order($formable_id, $form->product_id);
					}
					return response()->json(['status' => true, 'message' => 'Product saved successfully!']);
				}
			}
		}
	}


	function delete_product($product_id){
		$product = Product::find($product_id);
		$product->deleted_at = now();

		$form = MainForm::find($product->formable_id);
		$form->is_product = 0;

		$form->save();
		if ($product->save()) {
			return response()->json(['status' => true, 'message' => 'Product deleted successfully!']);
		}

	}


	// update Selected
	function form_order_manual($mainform_id, $product_id){

		$form = MainForm::find($mainform_id);
		$detail = $form->formDetail;

		$owner_id = $form->mainform_id;
		$owner_type = get_class($form);

		$form->is_product = 1;
		$form->save();

		$form_order = FormOrder::where('mainform_id', $mainform_id)->get();
		foreach ($form_order as $vv) {
			$vv->product_id = $product_id;
			$vv->save();
		}
	}


	// Generator
	function form_order($mainform_id, $product_id){

		$form = MainForm::find($mainform_id);
		$detail = $form->formDetail;

		$owner_id = $form->mainform_id;
		$owner_type = get_class($form);

		$product = Product::find($product_id);


		$form->is_product = 1;

		$form->save();

		$question_count = 0;
		foreach ($detail as $data) {
			foreach ($data->category->detail->limit($data->question_load)->inRandomOrder()->get() as $row) {
				$question_id = $row->question_id;

				$form = new FormOrder;
				$form->question_id = $question_id;
				$form->product_id = $product_id;
				// $form->sub_category_id = $sub_category_id;
				$form->owner_id = $owner_id;
				$form->owner_type = $owner_type;

				if($form->save()){
					$question_count++;
				}
			}
		}
	}

}
