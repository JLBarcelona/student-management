<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;

use App\User;


class AccountController extends Controller
{
    

  function register(){
    return view('Account.signup');
  }

  function verify_account(Request $request, $token, $id){
    $check = User::where('verification_code', $token)->where('user_id', $id)->whereNull('verified_at');
    if ($check->count() > 0) {
      $verify = $check->first();
      $verify->verified_at = now();

      if ($verify->save()) {
        return view('Account.verified', compact('verify'));
      }

    }else{
        return view('Account.login');
    }
  }



   function login_user(Request $request){

    	$result = array();


    	$user_login = array(
    		'username' => $request->get('username'),
    		'password' => $request->get('password')
    	);


    		$validator = Validator::make($request->all(), [
              'username' => 'required',
              'password' => 'required',
              'g-recaptcha-response' => 'required'
        ]);



		if ($validator->fails()) {
			 return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
			if (Auth::guard('admin')->attempt($user_login)) {
	         
	          $user_type = Auth::guard('admin')->user()->user_type;
	          return response()->json(['status' => true, 'message' => 'Accept!', 'user_type' => $user_type]);

	      	}
	      	elseif (Auth::attempt($user_login)) {
	         
	          $user_type = Auth::user()->user_type;
            $verified = Auth::user()->verified_at;

            if (!empty($verified)) {
              return response()->json(['status' => true, 'message' => 'Accept!', 'user_type' => $user_type]);
            }else{
              $validator->errors()->add('username', 'Please verify your account to your email!');
              // $validator->errors()->add('password', 'Please verify your  to your email!');
              return response()->json(['status' => false, 'error' => $validator->errors()]);
            }
	      	
          }else{
	            $validator->errors()->add('username', 'Invalid Account!');
	            $validator->errors()->add('password', 'Invalid Account!');
	            return response()->json(['status' => false, 'error' => $validator->errors()]);
	      }
	}

  }

    function logout_admin(){
        auth()->guard('admin')->logout();
        return redirect('/');
    }

    function logout_user(){
        Auth::logout();
        return redirect('/');
    }

     public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('/');
        }
    }
}
