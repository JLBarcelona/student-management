<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;


use App\User;
use App\Admin;
use App\ForgotPassword;

use App\Mail\ForgotMail;


use Str;
use Validator;
use Carbon\Carbon;

class ForgotPasswordController extends Controller
{

  function create_new_password(Request $request, $token, $id){
    $check = ForgotPassword::where('token', $token)->where('forgotable_id', $id);
    $new_password = $request->get('new_password');
    $confirm_password = $request->get('confirm_password');


    if ($check->count() > 0) {
        $validator = Validator::make($request->all(), [
            'new_password' => 'required|min:8|max:20',
            'confirm_password' => 'required|same:new_password',
            'g-recaptcha-response' => 'required'
          ]);

        if ($validator->fails()) {
      			 return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
            $info = $check->first();
            $info->is_used = 1;
            //change password
            $user = $info->forgotable;
            $user->password = $confirm_password;
            $info->save();
            if ($user->save()) {
              return response()->json(['status' => true, 'message' => 'Password changed successfully!']);
            }
        }
    }else{
      return response()->json(['status' => false, 'message' => 'Token is already expired!']);

    }
  }

  function new_password(Request $request, $token, $id){
    $check = ForgotPassword::where('token', $token)->where('forgotable_id', $id)->where('is_used', 0);
    if ($check->count() > 0) {
      return view('Account.new_password', compact('token', 'id'));
    }else{
      abort(403,'Token not found!');
    }
  }

  function get_token(Request $request, $user_type){
    $email = $request->get('email_address');
    $user_forget = '';

      $validator = Validator::make($request->all(), [
              'email_address' => 'required|email',
              'g-recaptcha-response' => 'required'
        ]);

      if ($validator->fails()) {
    			 return response()->json(['status' => false, 'error' => $validator->errors()]);
      }else{
        if ($user_type == 1) {
            $user = User::where('email', $email);
            $admin = Admin::where('email', $email);
            if ($user->count() > 0) {
              $user_detail = $user->first();

              $myid = $user_detail->user_id;
              $token = Str::random(10);

              $forgot = new ForgotPassword;
              $forgot->forgotable_id = $myid;
              $forgot->forgotable_type = get_class($user_detail);
              $forgot->token = $token;
              $forgot->date_validity = Carbon::now()->add(10, 'minutes');

              if ($forgot->save()) {
                $to_name = $user_detail->name;
                $to_email = $user_detail->email;
                $url_link = $url_link = url('/new_password/'.$token.'/'.$myid.'');

                $data = array('name' => $to_name, 'url_link' => $url_link);
                Mail::to($to_email)->send(new ForgotMail($data));
                return response()->json(['status' => true, 'message' => 'Please check your email. We will send your link.']);
              }


            }elseif ($admin->count() > 0) {

            	  $admin_detail = $admin->first();
	              $myid = $admin_detail->id;
	              $token = Str::random(10);

	              $forgot = new ForgotPassword;
	              $forgot->forgotable_id = $myid;
	              $forgot->forgotable_type = get_class($admin_detail);
	              $forgot->token = $token;
	              $forgot->date_validity = Carbon::now()->add(10, 'minutes');

	              if ($forgot->save()) {
	                $to_name = $admin_detail->name;
	                $to_email = $admin_detail->email;
	                $url_link = $url_link = url('/new_password/'.$token.'/'.$myid.'');

	                $data = array('name' => $to_name, 'url_link' => $url_link);
	                Mail::to($to_email)->send(new ForgotMail($data));
	                return response()->json(['status' => true, 'message' => 'Please check your email. We will send your link.']);
            }else{
              return response()->json(['status' => false, 'error' => $validator->errors()->add('email_address', 'email address not match!')]);

            }
          }
        }
    }

}

}
