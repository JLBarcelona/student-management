<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use Illuminate\Support\Facades\Mail;
use App\Mail\AccountVerify;

use App\ForgotPassword;

use Validator;
use Str;


use App\Admin;
use App\User;

use Carbon\Carbon;
use Hash;

class SignupController extends Controller
{

	 function is_unique($param,$tbl){
      $result = ($param > 0) ? '|unique:'.$tbl : '';

      return $result;
    }


     function send_mail($user){
    	$token = $user->verification_code;
		$to_name = $user->name;
		$to_email = $user->email;
		$url_link = $url_link = url('/verify_account/'.$token.'/'.$user->user_id.'');

		$data = array('name' => $to_name, 'url_link' => $url_link);
		Mail::to($to_email)->send(new AccountVerify($data));
    }



    function add(Request $request){

		$name = $request->get('name');
		$username = $request->get('username');
		$email = $request->get('email');
		$password = $request->get('password');
		$confirm_password = $request->get('confirm_password');
		$token = Str::random(10);


 		$username_check = Admin::where('username', $username)->whereNull('date_deleted')->count();
		$email_check = Admin::where('email', $email)->whereNull('date_deleted')->count();

		$username_check_user = User::where('username', $username)->whereNull('date_deleted')->count();
		$email_check_user = User::where('email', $email)->whereNull('date_deleted')->count();

		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'username' => 'required'. $this->is_unique($username_check, 'sm_admin').$this->is_unique($username_check_user, 'sm_users'),
			'email' => 'required|email'. $this->is_unique($email_check, 'sm_admin').$this->is_unique($email_check_user, 'sm_users'),
			'password' => 'required|min:8|max:20',
			'confirm_password' => 'required|same:password|max:20',
			 'g-recaptcha-response' => 'required',
		]);

		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
				$user = new User;
				$user->name = $name;
				$user->username = $username;
				$user->email = $email;
				$user->password = $confirm_password;
				$user->verification_code = $token;

				if($user->save()){
					$this->send_mail($user);
					return response()->json(['status' => true, 'message' => 'User saved successfully! Please check your email. We will send your confirmation.']);
				}
		}
	}
}
