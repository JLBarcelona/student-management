<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFormOrder extends Model
{
    public $timestamps = false;

    protected $primaryKey = 'order_id';


    protected $table = 'sm_user_order';
    
    protected $appends = ['question_detail'];
    
    protected $fillable = [
       'question_id', 'sub_category_id', 'purchased_id'
    ];

    protected $hidden = [
        'owner_type', 'owner_id',
    ];


    public function getQuestionDetailAttribute(){
        return $this->question()->first();
    }

    public function owner()
    {
        return $this->morphTo();
    }

    public function question(){
        return $this->belongsTo('App\Question', 'question_id');
    }
}
