<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
   protected $primaryKey = 'question_id';


    protected $table = 'sm_question';


    protected $fillable = [
        'question', 'explanation','question_type','is_auto_check'
    ];

    protected $hidden = [
        'owner_type', 'owner_id',
    ];


    public function owner()
    {
        return $this->morphTo();
    }

    public function question()
    {
        return $this->morphTo();
    }

    public function userorder(){
        return $this->hasMany('App\UserFormOrder', 'question_id');
    }

    public function answerDetail(){
        return $this->hasMany('App\AnswerDetail', 'question_id');
    }

    public function choices()
    {
        return $this->morphMany('App\Choice', 'owner');
    }

}
