<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $primaryKey = 'product_id';


    protected $table = 'sm_product';
    
    protected $appends = ['form'];

    protected $fillable = [
       'passing_mark', 'formable_id', 'price', 'price_sale'
    ];

    protected $hidden = [
        'owner_type', 'owner_id', 'formable_type',
    ];


    public function owner()
    {
        return $this->morphTo();
    }

    // public function getPriceAttribute($value){
    //     return  env('APP_CURRENCY').' '.$value;
    // }

    public function mainform()
    {
       return $this->belongsTo('App\MainForm', 'formable_id');
    }

    public function purchased(){
        return $this->hasMany('App\Purchased', 'product_id');
    }

    public function getFormAttribute(){
        return $this->mainform()->first();

        // return $this->belongsTo('App\MainForm', 'mainform_id');
    }


    public function bundle(){
        return $this->hasMany('App\BundleItem', 'product_id');
    }


}
