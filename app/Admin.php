<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Hash;


class Admin extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'sm_admin';


    protected $fillable = [
        'profile_picture','name', 'email', 'username', 'password', 'date_deleted'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */

      public function mainform()
    {
        return $this->morphMany('App\MainForm', 'owner')->whereNull('deleted_at');
    }

      public function category()
    {
        return $this->morphMany('App\Category', 'owner')->whereNull('deleted_at');;
    }


       public function product()
    {
        return $this->morphMany('App\Product', 'owner')->whereNull('deleted_at');
    }

        public function bundle()
    {
        return $this->morphMany('App\Bundle', 'owner')->whereNull('deleted_at');
    }

    public function forgot(){
        return $this->morphMany('App\ForgotPassword', 'forgotable');
    }

    public function UserGroup()
    {
        return $this->morphMany('App\Group', 'groupable');
    }

      public function setPasswordAttribute($value){
        $this->attributes['password'] = Hash::make($value);
    }

    public function getCreatedAtAttribute($value)
    {
        return  \Carbon\Carbon::parse($value)->format('jS F Y');
    }

}
