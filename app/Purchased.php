<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchased extends Model
{

    protected $primaryKey = 'purchased_id';

    protected $table = 'sm_purchased';

    protected $fillable = [
       'timer', 'total_score', 'price', 'discount', 'passing_mark', 'paid_at', 'finish_at'
    ];

    protected $appends = ['products'];

    protected $hidden = [
        'owner_type', 'owner_id','productable_id', 'productable_type',
    ];

    public function owner(){
        return $this->morphTo();
    }

    public function userorder(){
        return $this->morphMany('App\UserFormOrder', 'owner');
    }

    public function getProductsAttribute(){
        return $this->product()->firstorFail();
    }

    public function product(){
        return $this->belongsTo('App\Product', 'productable_id');
    }

      public function result(){
        return $this->hasOne('App\Result', 'purchased_id');
    }

    public function answer_detail(){
        return $this->belongsTo('App\AnswerDetail', 'purchased_id');
    }

     public function getCreatedAtAttribute($value)
    {
        return  \Carbon\Carbon::parse($value)->format('jS F Y');
    }

}
