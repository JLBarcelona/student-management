<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupCategory extends Model
{
   protected $primaryKey = 'group_category_id';


    protected $table = 'sm_group_category';


    protected $fillable = [
        'group_name', 'created_at','updated_at','deleted_at'
    ];

    public function mygroup(){
      return $this->hasMany('App\Group', 'group_category_id');
    }

}
