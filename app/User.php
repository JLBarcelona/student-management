<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Hash;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $primaryKey = 'user_id';


    protected $table = 'sm_users';

    protected $appends = ['purchased_item', 'score'];

    protected $fillable = [
        'profile_picture','name', 'email', 'username', 'date_deleted', 'verification_code', 'verified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */

     public function setPasswordAttribute($value){
        $this->attributes['password'] = Hash::make($value);
    }

    public function getScoreAttribute(){
        return $this->answerdetail();
    }

    public function getPurchasedItemAttribute(){
        return $this->purchased();
    }

    public function purchased()
    {
        return $this->morphMany('App\Purchased', 'owner');
    }

    public function category()
    {
        return $this->morphMany('App\Category', 'owner');
    }

    public function mainform()
    {
        return $this->morphMany('App\MainForm', 'ownerable');
    }

    public function UserGroup()
    {
        return $this->morphMany('App\Group', 'groupable');
    }

    public function result()
    {
        return $this->morphMany('App\Result', 'owner');
    }


     public function answerdetail()
    {
        return $this->morphMany('App\AnswerDetail', 'owner');
    }

    public function forgot(){
        return $this->morphMany('App\ForgotPassword', 'forgotable');
    }


}
