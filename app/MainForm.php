<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainForm extends Model
{
     protected $primaryKey = 'mainform_id';


    protected $table = 'sm_main_form';


    protected $appends  = ['formorder'];

    protected $fillable = [
        'name','form_timer','passing_mark', 'deleted_at','is_product','is_random','is_auto_choose'
    ];

    protected $hidden = [
        'owner_type', 'owner_id',
    ];


    public function owner()
    {
        return $this->morphTo();
    }

    // public function product()
    // {
    //     return $this->morphMany('App\Product', 'formable');
    // }

    public function formdetail()
    {
        return $this->morphMany('App\FormDetail', 'owner');
    }

    public function order()
    {
        return $this->morphMany('App\FormOrder', 'owner');
    }


    public function getFormorderAttribute(){
        return $this->order();
    }

      public function product()
    {
       return $this->hasMany('App\MainForm', 'mainform_id');
    }

      public function getCreatedAtAttribute($value)
    {
        // return ucfirst($value);
        // return  \Carbon\Carbon::parse($value)->format('m-d-y h:i:s A');
        return  \Carbon\Carbon::parse($value)->format('jS F Y');
    }

}
