<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnswerDetail extends Model
{

	protected $primaryKey = 'answer_id';

    protected $table = 'sm_answer_detail';

    protected $fillable = [
       'choice_id', 'points', 'purchased_id' , 'question_id'
    ];

    protected $appends = ['choice_detail', 'question_explain'];

    protected $hidden = [
        'owner_type', 'owner_id', 'question_detail'
    ];

    public function owner()
    {
        return $this->morphTo();
    }

		public function getQuestionExplainAttribute(){
			$data = $this->question()->first();
			return $data->explanation;
		}

    public function getChoiceDetailAttribute(){
        return $this->choice()->first();
    }

    public function choice(){
        return $this->belongsTo('App\Choice', 'choice_id');
    }

		public function question(){
        return $this->belongsTo('App\Question', 'question_id');
    }

		public function purchased(){
			return $this->hasOne('App\Purchased', 'purchased_id', 'purchased_id');
		}

		public function getCreatedAtAttribute($value)
	 {
			 return  \Carbon\Carbon::parse($value)->format('jS F Y');
	 }

	 public function getCheckAtAttribute($value)
	{

			return  (!empty($value))? \Carbon\Carbon::parse($value)->format('jS F Y') : '';
	}
}
