<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
	protected $primaryKey = 'result_id';

    protected $table = 'sm_result';

    protected $fillable = [
       'get_score', 'base_score', 'passing_mark', 'purchased_id'
    ];

    protected $hidden = [
        'owner_type', 'owner_id',
    ];

    public function owner()
    {
        return $this->morphTo();
    }

     public function purchased(){
        return $this->belongsTo('App\Purchased', 'purchased_id');
    }

}
