<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormDetail extends Model
{
    
    public $timestamps = false;

    protected $primaryKey = 'form_detail_id';


    protected $table = 'sm_form_detail';
    
    
    protected $fillable = [
       'category_id', 'question_load'
    ];

    protected $hidden = [
        'owner_type', 'owner_id','category_type',
    ];


    public function owner()
    {
        return $this->morphTo();
    }

     public function category()
    {
         return $this->belongsTo('App\Category', 'category_id');
    }

}
