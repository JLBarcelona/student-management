<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Choice extends Model
{
 	protected $primaryKey = 'choice_id';

    protected $table = 'sm_choices';
    
    protected $fillable = [
       'order','choice_answer','points'
    ];

    protected $hidden = [
        'owner_type', 'owner_id',
    ];


    public function answerdetail(){
        return $this->hasMany('App\AnswerDetail', 'choice_id');
    }

    public function owner()
    {
        return $this->morphTo();
    }

}
