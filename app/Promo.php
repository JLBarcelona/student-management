<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promo extends Model
{
   protected $primaryKey = 'promo_id';


    protected $table = 'sm_promo';
    
    
    protected $fillable = [
       'promo_name', 'promo_code', 'promo_description','price_min','price_discount','is_available','date_deleted'
    ];


}
