<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('Account')->group(function () {

	Route::get('/', function () {
	    return view('Account/login');
	});
	Route::post('/check_user', 'AccountController@login_user');
	Route::get('/logout/admin','AccountController@logout_admin')->name('logout.admin');


	Route::get('/logout/user','AccountController@logout_user')->name('logout.user');

	// logout_user
	Route::get('/home/register', 'AccountController@register')->name('home.register');

	// registration
 	Route::post('/user/add', 'SignupController@add')->name('user.add');


 	Route::get('/forgot_password', function(){
	    return view('Account.forgot_password');
	  });

	 Route::post('get_token/{user_type}', 'ForgotPasswordController@get_token');
	 Route::get('/new_password/{token}/{id}', 'ForgotPasswordController@new_password')->name('account.new_password');
	 Route::get('/verify_account/{token}/{id}', 'AccountController@verify_account')->name('account.verify');

	 Route::post('/create_new_password/{token}/{id}', 'ForgotPasswordController@create_new_password');

});


Route::namespace('Admin')->group(function () {
	Route::middleware(['auth:admin'])->group(function () {

   		Route::get('/Admin/config', function(){
					return view('Admin.account_settings');
			})->middleware('auth:admin');

		Route::get('/admin', 'AdminController@index')->name('admin.index');

		// Account
		 Route::get('/account/list', 'AccountController@list')->name('account.list');
		 Route::post('/account/add', 'AccountController@add')->name('account.add');
		 Route::get('/account/delete/{id}', 'AccountController@delete_account')->name('account.delete');

		 // Account Password
		 Route::post('/password/change_password', 'AccountController@change_password')->name('password.change_password');


		// Student
		Route::get('/admin/student', 'StudentController@index')->name('admin.student.index');
		Route::post('/student_add', 'StudentController@add')->name('admin.student.add');
		Route::get('/student/list', 'StudentController@list')->name('admin.student.list');
		Route::get('/student/delete/{user_id}', 'StudentController@student_delete')->name('admin.student.delete');

		// Teacher
		Route::get('/admin/teacher', 'TeacherController@index')->name('admin.teacher.index');
		Route::get('/teacher/list', 'TeacherController@list')->name('teacher.list');
		Route::post('/teacher/add', 'TeacherController@add')->name('teacher.add');
		Route::get('/teacher/delete/{id}', 'TeacherController@delete_teacher')->name('teacher.delete');

		// Promo Code
  		 Route::get('/admin/promo', 'PromoController@index')->name('admin.promo.index');
		 Route::get('/promo/list', 'PromoController@list')->name('promo.list');
		 Route::post('/promo/add', 'PromoController@add')->name('promo.add');
		 Route::get('/promo/delete/{promo_id}', 'PromoController@delete_promo')->name('promo.delete');

		 // Bundle
		Route::get('/admin/bundle', 'BundleController@index')->name('admin.promo.index');
		Route::get('/bundle/list', 'BundleController@list')->name('bundle.list');
		Route::post('/bundle/add', 'BundleController@add')->name('bundle.add');
		Route::get('/bundle/delete/{bundle_id}', 'BundleController@delete_bundle')->name('bundle.delete');
		// Form
		Route::get('/form/dropdown', 'FormController@dropdown')->name('admin.form.dropdown');

		Route::get('/form/list', 'FormController@list')->name('admin.form.list');
		Route::get('admin/form', 'FormController@index')->name('admin.form.index');
		Route::get('admin/form/edit/{form_id}', 'FormController@edit')->name('admin.form.edit');
		Route::post('admin/form/create', 'FormController@add')->name('admin.form.add');
		Route::post('/form/add/category', 'FormController@add_form_category');
		Route::get('/form/delete/{mainform_id}', 'FormController@delete_forms')->name('mainform.delete');
		Route::get('/form/category/question/{mainform_id}', 'FormController@get_category_question')->name('form.category.question');
		Route::get('/form/category/delete/{form_detail_id}', 'FormController@form_detail_delete')->name('form.detail.action');

		// Manual selection
		Route::get('admin/form/manual/edit/{form_id}', 'FormManualController@edit')->name('admin.form_manual.edit');
		Route::get('/form/category/question/manual/{mainform_id}', 'FormManualController@get_category_question')->name('form.category.question_manual');
		Route::post('/form/save/order', 'FormManualController@save_form_order')->name('save_manual.order');


		// Generator
	  // Route::post('/generate_form_order/{mainform_id}', 'FormController@form_order');

		// Category
		Route::get('/category/list', 'CategoryController@list')->name('category.list');
		Route::get('admin/category', 'CategoryController@index')->name('admin.category.index');
		Route::get('admin/category/edit/{category_id}', 'CategoryController@edit')->name('admin.category.edit');
		Route::post('admin/category/create', 'CategoryController@add')->name('admin.category.add');
		Route::get('category/question/{category_id}', 'CategoryController@get_question')->name('admin.category.question');
		Route::get('/admin/category/delete/{category_id}', 'CategoryController@delete_category')->name('admin.category_id.delete');
		// Question
		Route::get('/question/find/{id}', 'CategoryController@find_question')->name('question.find');
	 	Route::post('/question/add', 'CategoryController@question_add')->name('question.add');
    Route::post('/choices/add', 'CategoryController@choices_add')->name('choices.add');
		Route::get('/delete/question/{question_id}', 'CategoryController@delete_question')->name('question.delete');

 	    //Choices
 	    Route::get('/question/choice/{question_id?}', 'CategoryController@get_choices')->name('choices.list');
 	    Route::get('/choices/delete/{choice_id}', 'CategoryController@delete_choices')->name('choice.delete');


			// Group category
			Route::get('/admin/group/category', function(){
					return view('GroupCategory.index');
			})->name('group.category');

			Route::get('/group/list_group/find/{id}', 'GroupCategoryController@list_group_find')->name('group.find');
			Route::get('/group/list_group_member/find/{id}', 'GroupCategoryController@list_group_member_find')->name('group.find.member');

			Route::get('/group/list_user_group/find', 'GroupCategoryController@list_user_group_find')->name('user.group.find');
			Route::get('/mygroup/show/{id}', 'GroupCategoryController@show_my_group')->name('user.mygroup.show');

			Route::get('/group/list_group', 'GroupCategoryController@list_group')->name('group.list');
			Route::post('/group/add_group', 'GroupCategoryController@add_group')->name('group.add');
			Route::get('/group/delete_group/{group_category_id}', 'GroupCategoryController@delete_group')->name('group.delete');


			// Group
			 Route::get('/g_member/delete_g_member/{group_id}', 'GroupController@delete_g_member')->name('g_member.delete');

			 // Test form check by teacher
			 Route::get('/admin/test/check', function(){
				 return view('Teacher.checkforms');
			 });

			 Route::get('/teacher/check_list', 'CheckController@checking_form_list')->name('check.list');

			 // Open the form Checking modal
			 Route::get('/teacher/check/{id?}', 'CheckController@get_question_from_answer')->name('check.answer.form');


			 Route::get('/testing/check/{id?}', 'CheckController@save_purchase')->name('check.test.form');

			 // Add score
			 Route::post('/teacher/score/question', 'CheckController@add_score')->name('group.add');



	});
});


Route::namespace('Inventory')->group(function () {
	Route::middleware(['auth:admin'])->group(function () {

		Route::get('/admin/inventory', 'InventoryController@index')->name('product.index');
		Route::get('/inventory/list', 'InventoryController@list')->name('product.list');
		Route::post('/product/add', 'InventoryController@add_product')->name('product.add');
		Route::get('/product/delete/{product_id}', 'InventoryController@delete_product')->name('product.delete');


	});



});


Route::namespace('Form')->group(function () {

});


Route::namespace('Student')->group(function () {
	Route::middleware(['auth'])->group(function () {

		Route::get('/student/config', function(){
			return view('Student.account_settings');
		});

		// Account
		Route::post('/student/account/add', 'AccountController@add')->name('student.account.add');
		Route::post('/student/password/change_password', 'AccountController@change_password')->name('student.password.change_password');

		Route::get('/student', 'ShopController@index')->name('student.index');
		Route::get('/student/shop/', 'ShopController@shop')->name('student.shop');

 	 	Route::get('/product/list', 'ShopController@list')->name('product.list');
	 	Route::post('/student/buy/form', 'PurchasedController@add')->name('purchased.add');


		Route::get('/student/form', 'PurchasedController@index')->name('student.form');
	 	// Purchased
	 	Route::get('/purchased/list', 'PurchasedController@list')->name('purchased.list');

	 	// Exam form

	 	Route::get('/student/exam/{purchased_id}', 'ExamController@exam')->name('student.exam');
	 	Route::get('/get/exam/form/{purchased_id}', 'ExamController@get_exam')->name('student.purchased.exam');

		Route::get('/get/exam/review/{purchased_id}', 'ExamController@get_review')->name('student.purchased.review');
	 	Route::post('/save/answer', 'ExamController@make_answer')->name('student.question.answer');
		Route::post('/save/custom_answer', 'ExamController@make_custom_answer')->name('student.question.answer.custom');


	 	// result
	 	 Route::post('/result/add', 'ExamController@submit_form')->name('result.add');

	 	// test route result
 	 	// Route::get('/form/order/{product_id}', 'PurchasedController@user_form_order')->name('form.order');

	 	 // Timer
	 	 Route::post('/exam/timer/update/{purchased_id}', 'ExamController@update_time')->name('exam.timer.update');
	 	 Route::get('/exam/user/timer/{purchased_id}', 'ExamController@get_time')->name('exam.timer.get');

	 	 // Bundle
 	     Route::get('/product/bundle/list', 'PurchasedController@bundle_list')->name('bundle.list');
		 Route::post('/buy/bundle', 'PurchasedController@buy_bundle')->name('bundle.purchased');
	});

	Route::get('/student/progress/{user_id}', 'StudentController@get_student_progress')->name('student.progress');
	Route::get('/student/progress/table/{user_id}', 'StudentController@get_student_progress_table')->name('student.progress.table');
});



Route::namespace('Teacher')->group(function () {
	Route::middleware(['auth:admin'])->group(function () {
		Route::get('/teacher', function(){
			return view('Teacher.index');
		})->name('teacher.index');
	});
});
