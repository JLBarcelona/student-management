var show_choices;


	function edit_choices(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		$("#choice_id").val(data.choice_id);
		// $("#choice_answer").val(data.choice_answer);
		$("#points").val(data.points);
		CKEDITOR.instances['choice_answer'].setData(data.choice_answer);

		// editor1.setData(data.choice_answer);
	}


	function delete_choices(_this){
	  var data = JSON.parse($(_this).attr('data-info'));
	  var url = main_path + '/choices/delete/' + data.choice_id;

	  	// alert(url);
	  	var question_id = $("#question_id_cat").val();

	  swal({
	        title: "Are you sure?",
	        text: "Do you want to delete this choices?",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "Yes",
	        closeOnConfirm: false
	      },
	        function(){
	         $.ajax({
	          type:"GET",
	          url:url,
	          data:{},
	          dataType:'json',
	          beforeSend:function(){
	          },
	          success:function(response){
	            // console.log(response);
	           if (response.status == true) {
	              swal("Success", response.message, "success");
	              show_question();
	              show_choice_answer(question_id);
	           }else{
	            console.log(response);
	           }
	          },
	          error: function(error){
	            console.log(error);
	          }
	        });
	      });
	  }

	function add_question(){
		CKEDITOR.instances['question'].setData('');
		CKEDITOR.instances['explanation'].setData('');
		$("#question_id").val('');
		// $("#question").val('');
		show_choice_answer();
		// $("#show_choices").html('');
		$("#checking_option").val(1);
		$("#add_question_modal").modal('show');
	}

	function show_question(){
		var selector = $("#show_questions");
		var cat_id = selector.attr('data-id');
		var url = main_path + '/category/question/' + cat_id;
		$.ajax({
			type:"GET",
			url:url,
			data:{},
			dataType:'json',
			beforeSend:function(){
				selector.html('<center><img src="'+loader+'" class="img-fluid" width="20"></center>');
				// alert(loader);
			},
			success:function(response){
				// console.log(response.output);
				if (response.status == true) {
					// console.log(response.output);
					selector.html(response.output);
				}else{
					// console.log(response.output);
				}
			},
			error:function(error){
				console.log(error);
			}
		});
	}


	function show_choice_answer(question_id){
		if (show_choices) {
			show_choices.destroy();
		}

		var url = main_path + '/question/choice/' + question_id;
		show_choices = $('#show_choices').DataTable({
        pageLength: 10,
        responsive: true,
        ajax: url,
        deferRender: true,
			language: {
				 "emptyTable": "No data available"
			},
        columns: [
         {
         	 className: 'choices',
            "data": "choice_answer",
            "title": "Choices",
          },{
         	 className: 'width-option-1',
            "data": "points",
            "title": "Points",
          },{
            className: 'width-option-1 text-center',
            "data": "choice_id",
            "orderable": false,
            "title": "Options",
            "render": function(data, type, row, meta){
              var param_data = JSON.stringify(row);
              // console.log();
              // var data_param = JSON.parse(param_data);
              	newdata = '<button class="btn btn-success btn-sm font-base mt-1" data-info=\''+param_data.trim()+'\' onclick="edit_choices(this)" type="button"><i class="fa fa-edit"></i> Edit</button>\
						   <button class="btn btn-danger btn-sm font-base mt-1" data-info=\''+param_data.trim()+'\' type="button" onclick="delete_choices(this)"><i class="fa fa-trash"></i> Delete</button>';

              return newdata;
            }
          }
        ]
        });
	}


		function edit_question(id){
			// var data = JSON.parse($(_this).attr('data-info'));
			// console.log(data.question);
			let url = main_path + '/question/find/'+id;
			$.ajax({
	    type:"GET",
	    url:url,
	    data:{},
	    dataType:'json',
	    beforeSend:function(){
	    },
	    success:function(response){
	      // console.log(response);
	     if (response.status == true) {
	        // swal("Success", response.message, "success");
					show_choice_answer(response.data.question_id);
					$("#question_id").val(response.data.question_id);
					$("#question_id_cat").val(response.data.question_id);
					// $("#question").val(data.question);
					$("#checking_option").val(response.data.is_auto_check);
					CKEDITOR.instances['question'].setData(response.data.question);
					CKEDITOR.instances['explanation'].setData(response.data.explanation);
					// editor.setData(data.question);
					$("#add_question_modal").modal('show');
	     }else{
				 swal("Error", 'Unable to get data please contact your developer.', "error");
	      console.log(response);
	     }
	    },
	    error: function(error){
	      console.log(error);
	    }
	  });


		}

		function delete_question(_this){
		  var data = JSON.parse($(_this).attr('data-info'));
		  var url = main_path + '/delete/question/' + data.question_id;
		  swal({
		        title: "Are you sure?",
		        text: "Do you want to delete this question ?",
		        type: "warning",
		        showCancelButton: true,
		        confirmButtonColor: "#DD6B55",
		        confirmButtonText: "Yes",
		        closeOnConfirm: false
		      },
		        function(){
		         $.ajax({
		          type:"GET",
		          url:url,
		          data:{},
		          dataType:'json',
		          beforeSend:function(){
		          },
		          success:function(response){
		            // console.log(response);
		           if (response.status == true) {
		              swal("Success", response.message, "success");
		              show_question();
		           }else{
		            console.log(response);
		           }
		          },
		          error: function(error){
		            console.log(error);
		          }
		        });
		      });
		  }

	$("#add_question_form").on('submit', function(e){
			var url = $(this).attr('action');

			CKEDITOR.instances['question'].updateElement();
			CKEDITOR.instances['explanation'].updateElement();

			var mydata = $(this).serialize();
			e.stopPropagation();
			e.preventDefault(e);

			// console.log(mydata);
			$.ajax({
				type:"POST",
				url:url,
				data:mydata,
				cache:false,
				beforeSend:function(){
						//<!-- your before success function -->
				},
				success:function(response){
					// console.log(response);
					if(response.status == true){
						console.log(response);
						$("#question_id").val(response.id);
						$("#question_id_cat").val(response.id);
						swal("Success","Question saved successfully! \nYou can now add choices.","success");
						show_question();
						showValidator(response.error,'add_question_form');
					}else{
						//<!-- your error message or action here! -->
						showValidator(response.error,'add_question_form');
					}
				},
				error:function(error){
					console.log(error)
				}
			});
		});


	$("#add_choices_form").on('submit', function(e){
		var url = $(this).attr('action');
		CKEDITOR.instances['choice_answer'].updateElement();

		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		var question_id = $("#question_id_cat").val();

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
				if(response.status == true){
					$("#choice_id").val('');
					$("#points").val(0);
					console.log(response);
					// $("#choice_answer").val('');
					// editor1.setData('');
					showValidator(response.error,'add_choices_form');
					show_question();
					show_choice_answer(question_id);
					CKEDITOR.instances['choice_answer'].setData('');
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'add_choices_form');
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	});
