function show_uploaded_images(typeUser, UploadType, elem){
    var uid = $('input[data-uploader-id="upload_user_id"]').val();
    var url = main_path + '/image/get_image/'+UploadType+'/'+typeUser+'/'+uid;
    // alert(url);
    $.ajax({
      type:"GET",
      url:url,
      data:{},
      dataType:"json",
      beforeSend:function(){},
      success:function(response){
        $("#"+elem).html(response.data);
        // console.log(response);

      },error:function(error){
        console.log(error);
      }
    });
  }


  function delete_upload(id,img){
    var url = main_path + '/image/deleteUpload';

    swal({
        title: "Are you sure?",
        text: "Do you want to delete this image?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false
      },
      function(){
       $.ajax({
        type:"POST",
        url:url,
        data:{image_id : id, image : img},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
          if (response.status == true) {
            
          // GET typeUser = if belongs to comapny = 1 user = 2 or product = 3
          // GET UploadType = if upload is Gallery = 1 or Attachment = 2 
          show_uploaded_images(2, 1, 'show_gallery_images');
          show_uploaded_images(2, 2, 'show_attachment');

          show_uploaded_images(3, 1, 'show_gallery_images_product');
          show_uploaded_images(3, 2, 'show_attachment_product');


            swal("Success","File successfully deleted!","success");
          }else{
            swal("Oops!","There\'s something wrong with the server!","error");
          }
        },
        error: function(error){
          console.log(error);
        }
      });
      });
  }


  